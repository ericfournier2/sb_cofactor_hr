library(GenomicRanges)
library(AnnotationDbi)
library(eulerr)
library(tidyverse)

#####
gencode31_basic_chr_TxDb <- loadDb(file = "input/txdb.gencode31BasicChr.sqlite")

#####
get_today <- function() {
  today() %>% as.character %>% gsub(pattern = "-", replacement = "")
}

#####
keepStdChr <- function(gr) {
  message("With all chromosomes, including contigs : ", length(gr), " regions")
  stdChr <- paste0("chr", c(seq(1:22), "X", "Y"))
  gr_StdChr <- keepSeqlevels(gr, stdChr[stdChr %in% seqlevels(gr)], pruning.mode = "coarse")
  message("Keeping standard chromosomes : ", length(gr_StdChr), " regions")
  message("\t--> ", length(gr) - length(gr_StdChr), " regions removed")
  return(gr_StdChr)
}

#####
load_peaks_MCF7_E2 <- function(targets = c("ERA", "MED1", "BRD4")) {
  peaks_dir <- "output/chip-pipeline-GRCh38/peak_call"
  targets_peaks <- GRangesList()
  name_targets_peaks <- c()
  
  rep_reference <- list("E2_ERA" = "rep1", "CTRL_ERA" = "rep2",
                        "E2_MED1" = "rep2", "CTRL_MED1" = "rep1",
                        "E2_BRD4" = "rep1", "CTRL_BRD4" = "rep2")
  
  for (target in targets) {
    for (condition in c("CTRL", "E2")) {
      message("##### ", target, " | ", condition)
      target_condition <- paste(condition, target, sep = "_")
      rep <- rep_reference[[target_condition]]
      basename <- paste("MCF7", target_condition, rep, sep = "_")
      filename <- paste(basename, "peaks.narrowPeak.bed", sep = "_")
      peaks_path <- file.path(peaks_dir, basename, filename)
      peaks_raw <- rtracklayer::import(peaks_path)
      peaks <- keepStdChr(peaks_raw)
      
      targets_peaks <- append(targets_peaks, GRangesList(peaks))
      name_targets_peaks <- c(name_targets_peaks, paste(target, condition, rep, sep = "_"))
    }
  }
  names(targets_peaks) <- name_targets_peaks
  message("#####################################")
  message("Available set of regions in MCF7+HS: ")
  print(sapply(targets_peaks, length))
  return(targets_peaks)
}

#####
load_peaks_MCF7_HS <- function(targets = c("HSF1", "MED1", "BRD4")){
  peaks_dir <- "output/chip-pipeline-GRCh38-HS/peak_call"
  timepoint <- c(0, 10, 20, 30)
  targets_peaks <- GRangesList()
  name_targets_peaks <- c()
  for (target in targets) {
    for (time in timepoint) {
      message("##### ", target, " | ", time, "m")
      basename <- paste(target, time, 1, sep = "_")
      filename <- paste(basename, "peaks.narrowPeak.bed", sep = "_")
      peaks_path <- file.path(peaks_dir, basename, filename)
      peaks_raw <- rtracklayer::import(peaks_path)
      peaks <- keepStdChr(peaks_raw)
      
      targets_peaks <- append(targets_peaks, GRangesList(peaks))
      name_targets_peaks <- c(name_targets_peaks, paste(target, paste0(time, "m"), sep = "_"))
    }
  }
  names(targets_peaks) <- name_targets_peaks
  message("#####################################")
  message("Available set of regions in MCF7+E2: ")
  print(sapply(targets_peaks, length))
  return(targets_peaks)
}

#####
load_diffbind_peaks_MCF7_E2 <- function(targets = c("ERA", "MED1", "BRD4")) {
  peaks_dir <- "output/chip-pipeline-GRCh38/binding_diff"
  diffbind_targets_peaks <- GRangesList()
  name_diffbind_targets_peaks <- c()
  
  vs_list <- list("ERA" = "MCF7_E2_ERA_rep1_peaks.narrowPeak_vs_MCF7_CTRL_ERA_rep2_peaks.narrowPeak",
                  "MED1" = "MCF7_E2_MED1_rep2_peaks.narrowPeak_vs_MCF7_CTRL_MED1_rep1_peaks.narrowPeak",
                  "BRD4" = "MCF7_E2_BRD4_rep1_peaks.narrowPeak_vs_MCF7_CTRL_BRD4_rep2_peaks.narrowPeak")
  
  for (target in targets) {
    vs <- vs_list[[target]]
    message("####\t", target)
    target_folder <- file.path(paste("MCF7_E2", target, sep = "_"), "output_filters")
    
    # up
    peaks_up_path <- file.path(peaks_dir, target_folder, paste(vs, "M_above_0.5_biased_peaks.bed", sep = "_"))
    message(" > ", peaks_up_path)
    peaks_up_raw <- rtracklayer::import(peaks_up_path)
    message("   > Number of regions : ", length(peaks_up_raw))
    peaks_up <- keepStdChr(peaks_up_raw)

    # down
    peaks_down_path <- file.path(peaks_dir, target_folder, paste(vs, "M_below_-0.5_biased_peaks.bed", sep = "_"))
    message(" > ", peaks_down_path)
    peaks_down_raw <- rtracklayer::import(peaks_down_path)
    message("   > Number of regions : ", length(peaks_down_raw))
    peaks_down <- keepStdChr(peaks_down_raw)

    # unbiased
    peaks_unbiased_path <- file.path(peaks_dir, target_folder, paste(vs, "unbiased_peaks.bed", sep = "_"))
    message(" > ", peaks_unbiased_path)
    peaks_unbiased_raw <- rtracklayer::import(peaks_unbiased_path)
    message("   > Number of regions : ", length(peaks_unbiased_raw))
    peaks_unbiased <- keepStdChr(peaks_unbiased_raw)

    # gather everything
    diffbind_targets_peaks <- append(diffbind_targets_peaks, GRangesList(peaks_up, peaks_down, peaks_unbiased))
    name_diffbind_targets_peaks <- c(name_diffbind_targets_peaks, c(paste(target, c("UP", "DOWN", "UNBIASED"), sep = "_")))
  }
  names(diffbind_targets_peaks) <- name_diffbind_targets_peaks
  message("#####################################")
  message("Available set of regions in MCF7+E2: ")
  print(names(diffbind_targets_peaks))
  return(diffbind_targets_peaks)
}

#####
load_diffbind_peaks_MCF7_HS <- function(targets = c("HSF1", "MED1", "BRD4")) {
  peaks_dir <- "output/chip-pipeline-GRCh38-HS/binding_diff"
  diffbind_targets_peaks <- GRangesList()
  name_diffbind_targets_peaks <- c()
  
  vs_list <- c("10vs0", "20vs0", "30vs0", "20vs10", "30vs10", "30vs20")
  
  for (target in targets) {
    for (vs in vs_list) {
      message("####\t", target, " | ", vs)
      target_folder <- file.path(paste("MCF7_HS", target, vs, sep = "_"), "output_filters")
      
      tps <- str_split(vs, pattern = "vs") %>% unlist
      tp1 <- tps[1]
      tp2 <- tps[2]
      
      vs_name <- paste(target, tp1, "1", "peaks.narrowPeak", "vs", target, tp2, "1", "peaks.narrowPeak", sep = "_")
      
      # up
      peaks_up_path <- file.path(peaks_dir, target_folder, paste(vs_name, "M_above_0.5_biased_peaks.bed", sep = "_"))
      message(" > ", peaks_up_path)
      peaks_up_raw <- rtracklayer::import(peaks_up_path)
      message("   > Number of regions : ", length(peaks_up_raw))
      peaks_up <- keepStdChr(peaks_up_raw)
      
      # down
      peaks_down_path <- file.path(peaks_dir, target_folder, paste(vs_name, "M_below_-0.5_biased_peaks.bed", sep = "_"))
      message(" > ", peaks_down_path)
      peaks_down_raw <- rtracklayer::import(peaks_down_path)
      message("   > Number of regions : ", length(peaks_down_raw))
      peaks_down <- keepStdChr(peaks_down_raw)
      
      # unbiased
      peaks_unbiased_path <- file.path(peaks_dir, target_folder, paste(vs_name, "unbiased_peaks.bed", sep = "_"))
      message(" > ", peaks_unbiased_path)
      peaks_unbiased_raw <- rtracklayer::import(peaks_unbiased_path)
      message("   > Number of regions : ", length(peaks_unbiased_raw))
      peaks_unbiased <- keepStdChr(peaks_unbiased_raw)
      
      # gather everything
      diffbind_targets_peaks <- append(diffbind_targets_peaks, GRangesList(peaks_up, peaks_down, peaks_unbiased))
      name_diffbind_targets_peaks <- c(name_diffbind_targets_peaks, c(paste(target, vs, c("UP", "DOWN", "UNBIASED"), sep = "_")))
    }
  }
  names(diffbind_targets_peaks) <- name_diffbind_targets_peaks
  message("#####################################")
  message("Available set of regions in MCF7+HS: ")
  print(names(diffbind_targets_peaks))
  message("#####################################")
  message("Number of peaks in MCF7+HS: ")
  print(sapply(diffbind_targets_peaks, length))
  return(diffbind_targets_peaks)
}

#####
annotatePeaks <- function(gr, output = "df", tss = 3000, TxDb_db, verbose_TF = FALSE) {
  gr_anno <- ChIPseeker::annotatePeak(gr, overlap = "all", 
                                      tssRegion = c(-tss, tss), TxDb = TxDb_db, annoDb = "org.Hs.eg.db",
                                      verbose = verbose_TF)
  if (output == "anno") {
    message("Return a csAnno object")
    return(gr_anno)
  } else if (output == "df") {
    gr_anno_df <- as.data.frame(gr_anno)
    gr_anno_df$Annot <- gr_anno_df$annotation
    gr_anno_df$Annot <- gsub(" \\(.*\\)", "", gr_anno_df$Annot)
    gr_anno_df$Annot <- as.factor(gr_anno_df$Annot)
    gr_anno_df$ENSG <- str_replace(gr_anno_df$geneId , "\\.[0-9]+$", "")
    gr_anno_df$Symbol <- mapIds(org.Hs.eg.db, gr_anno_df$ENSG, "SYMBOL", "ENSEMBL")
    gr_anno_df$Coordinates <- paste0(gr_anno_df$seqnames, ":", gr_anno_df$start, "-", gr_anno_df$end)
    message("Return a data.frame object")
    return(gr_anno_df)
  }
}

##### make UpSet plot
# input: matrix of overlaps (can be made with build_intersect function)
displayUpSet <- function(combMat, threshold = 1, customSetOrder = NULL) {
  combMat <- combMat[comb_size(combMat) >= threshold]
  annot_top <- HeatmapAnnotation("Intersection\nsize" = anno_barplot(comb_size(combMat), 
                                                                     border = FALSE,
                                                                     gp = gpar(fill = "black"),
                                                                     height = unit(3, "cm")), 
                                 "Size" = anno_text(comb_size(combMat),
                                                    rot = 0,
                                                    just = "center",
                                                    location = 0.25),
                                 annotation_name_side = "left", annotation_name_rot = 0)
  annot_right <- rowAnnotation("Set size" = anno_barplot(set_size(combMat), 
                                                         border = FALSE, 
                                                         gp = gpar(fill = "black"), 
                                                         width = unit(2, "cm")),
                               "Size" = anno_text(set_size(combMat)))
  
  if (!is.null(customSetOrder)) {
    UpSet(combMat, top_annotation = annot_top, right_annotation = annot_right,
          set_order = customSetOrder)
  } else {
    UpSet(combMat, top_annotation = annot_top, right_annotation = annot_right)
  }
}

#####
plotVenn <- function(gr_list, labels = TRUE) {
  overlaps <- GenomicOperations::GenomicOverlaps(gr_list)
  mat <- overlaps@matrix > 0
  fit <- euler(mat, shape = "ellipse")
  p <- plot(fit, labels = labels,
            quantities = list(type = c("counts", "percent")),
            fills = list(fill = c("#2B70AB", "#CD3301", "#FFB027", "#449B2B")),
            edges = list(alpha = 0))
  return(p)
}

#####
annotatePeaks <- function(gr, output = "df", tss = 3000, TxDb_db, verbose_TF = FALSE) {
  # difference between txdb and most expressed txdb???
  # output can be: "df" or "anno"
  # TxDb can be: most_expressed_TxDb or txdb.hg38
  gr_anno <- ChIPseeker::annotatePeak(gr, overlap = "all", 
                                      tssRegion = c(-tss, tss), TxDb = TxDb_db, annoDb = "org.Hs.eg.db",
                                      verbose = verbose_TF)
  if (output == "anno") {
    message("Return a csAnno object")
    return(gr_anno)
  } else if (output == "df") {
    gr_anno_df <- as.data.frame(gr_anno)
    gr_anno_df$Annot <- gr_anno_df$annotation
    gr_anno_df$Annot <- gsub(" \\(.*\\)", "", gr_anno_df$Annot)
    gr_anno_df$Annot <- as.factor(gr_anno_df$Annot)
    gr_anno_df$ENSG <- str_replace(gr_anno_df$geneId , "\\.[0-9]+$", "")
    gr_anno_df$Symbol <- mapIds(org.Hs.eg.db, gr_anno_df$ENSG, "SYMBOL", "ENSEMBL")
    gr_anno_df$Coordinates <- paste0(gr_anno_df$seqnames, ":", gr_anno_df$start, "-", gr_anno_df$end)
    message("Return a data.frame object")
    return(gr_anno_df)
  }
}