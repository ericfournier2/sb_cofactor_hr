import subprocess
import os
import time

alignment_path = "/Users/chris/Desktop/sb_cofactor_hr/MCF7/output/chip-pipeline-GRCh38-HS/alignment"

targets = ["HSF1", "MED1", "BRD4"]
timepoints = ["0", "10", "20", "30"]

i = 1
for target in targets:
    for tm in timepoints:
        start = time.time()
        basename = target + "_" + tm + "_1"

        bamfile = basename + ".sorted.dup.bam"
        bedfile = basename + ".sorted.dup.bed"

        input_bam = os.path.join(alignment_path, basename, bamfile)
        output_bed = os.path.join(alignment_path, basename, bedfile)

        sub = ["bedtools", "bamtobed",
               "-i", input_bam,
               ">", output_bed]

        sub2 = " ".join(sub)
        print("##### " + str(i) + " / " + str(len(targets)*4))
        print(sub2)

        # subprocess.call(sub2, shell=True)

        end = time.time()
        timer = end - start
        print("Conversion from BAM to BED of " + basename + " :\nDONE in " + str(timer) + " seconds")
        print("\n#################################################\n")

        i += 1
