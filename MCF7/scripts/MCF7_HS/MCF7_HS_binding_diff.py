import subprocess
import os
import time

read_path = "/Users/chris/Desktop/sb_cofactor_hr/MCF7/output/chip-pipeline-GRCh38-HS/alignment"
peak_path = "/Users/chris/Desktop/sb_cofactor_hr/MCF7/output/chip-pipeline-GRCh38-HS/peak_call"
output_path = "/Users/chris/Desktop/sb_cofactor_hr/MCF7/output/chip-pipeline-GRCh38-HS/binding_diff"

# targets = ["HSF1", "MED1", "BRD4"]
targets = ["HSF1"]
timepoints = ["0", "10", "20", "30"]
# contrast = [("10", "0"), ("20", "0"), ("30", "0"),
#             ("20", "10"), ("30", "10"),
#             ("30", "20")]
contrast = [("30", "0")]

i = 1
for target in targets:
    for tm1, tm2 in contrast:
        print("##### " + target + " | " + tm1 + "vs" + tm2)
        start = time.time()
        basename_tm1 = target + "_" + tm1 + "_1"
        basename_tm2 = target + "_" + tm2 + "_1"
        extension_peak = "_peaks.narrowPeak.bed"
        extension_read = ".sorted.dup.bed"

        p1 = os.path.join(peak_path, basename_tm1, basename_tm1 + extension_peak)
        p2 = os.path.join(peak_path, basename_tm2, basename_tm2 + extension_peak)
        r1 = os.path.join(read_path, basename_tm1, basename_tm1 + extension_read)
        r2 = os.path.join(read_path, basename_tm2, basename_tm2 + extension_read)
        output = os.path.join(output_path, "MCF7_HS_" + target + "_" + tm1 + "vs" + tm2)

        sub = ["manorm",
               "--p1", p1,
               "--p2", p2,
               "--r1", r1,
               "--r2", r2,
               "-m", "0.5",
               "-o", output]
        sub2 = " ".join(sub)

        print("##### " + str(i) + " / " + str(len(targets)*len(contrast)))
        print(sub2)

        # subprocess.call(sub2, shell=True)

        end = time.time()
        timer = end - start
        print("\nDifferential binding on " + output + " samples has been done")
        print("Processed time : " + str(timer) + " seconds")
        print("\n#################################################\n")

        i += 1


#

#
    #
#
    #
