import subprocess
import os
import time

samples_e2 = ["MCF7_E2_ERA_rep1", "MCF7_E2_MED1_rep2", "MCF7_E2_BRD4_rep1"]
samples_ctrl = ["MCF7_CTRL_ERA_rep2", "MCF7_CTRL_MED1_rep1", "MCF7_CTRL_BRD4_rep2"]
targets = ["ERA", "MED1", "BRD4"]

read_path = "/Users/chris/Desktop/sb_cofactor_hr/MCF7/output/chip-pipeline-GRCh38/alignment"
peak_path = "/Users/chris/Desktop/sb_cofactor_hr/MCF7/output/chip-pipeline-GRCh38/peak_call"
output_path = "/Users/chris/Desktop/sb_cofactor_hr/MCF7/output/chip-pipeline-GRCh38/binding_diff"

for cpt in range(3):
    i = cpt + 1

    start = time.time()
    basename_e2 = samples_e2[cpt]
    basename_ctrl = samples_ctrl[cpt]
    target = targets[cpt]
    extension_peak = "_peaks.narrowPeak.bed"
    extension_read = ".sorted.dup.bed"

    p1 = os.path.join(peak_path, basename_e2, basename_e2 + extension_peak)
    p2 = os.path.join(peak_path, basename_ctrl, basename_ctrl + extension_peak)
    r1 = os.path.join(read_path, basename_e2, basename_e2 + extension_read)
    r2 = os.path.join(read_path, basename_ctrl, basename_ctrl + extension_read)
    output = os.path.join(output_path, "MCF7_E2_" + target)

    sub = ["manorm",
           "--p1", p1,
           "--p2", p2,
           "--r1", r1,
           "--r2", r2,
	       "-m", "0.5",
           "-o", output]

    sub2 = " ".join(sub)
    print("##### " + str(i) + " / " + str(len(targets)))
    print(sub2)

    subprocess.call(sub2, shell=True)

    end = time.time()
    timer = end - start
    print("\nDifferential binding on " + output + " samples has been done")
    print("Processed time : " + str(timer) + " seconds")
    print("\n#################################################\n")
