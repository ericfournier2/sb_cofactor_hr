# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

library(ComplexHeatmap)
library(circlize)
source("scripts/chris/load_chrStructure.R")
source("scripts/ckn_utils.R")

##### Load diffbind
diffbind <- load_diffbind_cofactors_peaks()

##### Build peaks_set
MED1_UP <- diffbind[["MED1_UP"]] # 5956
MED1_DOWN <- diffbind[["MED1_DOWN"]] # 3048
BRD4_UP <- diffbind[["BRD4_UP"]] # 7173
BRD4_DOWN <- diffbind[["BRD4_DOWN"]] # 5913
CDK9_UP <- diffbind[["CDK9_UP"]] # 1578
CDK9_DOWN <- diffbind[["CDK9_DOWN"]] # 1083
MBC_UP <- GenomicRanges::reduce(c(diffbind[["MED1_UP"]], diffbind[["BRD4_UP"]], diffbind[["CDK9_UP"]])) # 8253
MBC_DOWN <- GenomicRanges::reduce(c(diffbind[["MED1_DOWN"]], diffbind[["BRD4_DOWN"]], diffbind[["CDK9_DOWN"]])) # 7492
MB_UP <- GenomicRanges::reduce(c(diffbind[["MED1_UP"]], diffbind[["BRD4_UP"]])) # 8195
MB_DOWN <- GenomicRanges::reduce(c(diffbind[["MED1_DOWN"]], diffbind[["BRD4_DOWN"]])) # 7221

cofactors_list <- GRangesList("MED1_UP" = MED1_UP, "MED1_DOWN" = MED1_DOWN,
                              "BRD4_UP" = BRD4_UP, "BRD4_DOWN" = BRD4_DOWN,
                              "CDK9_UP" = CDK9_UP, "CDK9_DOWN" = CDK9_DOWN,
                              "MBC_UP" = MBC_UP, "MBC_DOWN" = MBC_DOWN,
                              "MB_UP" = MB_UP, "MB_DOWN" = MB_DOWN)
sapply(cofactors_list, length)

#####
names_set <- c("MED1_UP", "BRD4_UP", "CDK9_UP",
               "MBC_UP", "MBC_DOWN",
               "MED1_DOWN", "BRD4_DOWN", "CDK9_DOWN")
cofactors <- cofactors_list[names_set]

#####
inter_cofactors <- GenomicOperations::GenomicOverlaps(cofactors)
matrix_cofactors <- inter_cofactors@matrix
sum(matrix_cofactors > 1)
matrix_cofactors[matrix_cofactors > 1] <- 1
sum(matrix_cofactors > 1)
colnames(matrix_cofactors)

# cofactors_names <- c("MED1", "BRD4", "CDK9", "NIPBL", "SMC1A")
# cofactors_set_order <- c(paste0(cofactors_names, "_DOWN"))
m4 <- make_comb_mat(matrix_cofactors, remove_empty_comb_set = TRUE)
upset <- displayUpSet(combMat = m4, threshold = 0, customSetOrder = names_set)
upset
# 


