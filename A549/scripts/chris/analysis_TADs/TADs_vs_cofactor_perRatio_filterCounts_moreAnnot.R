# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

library(ComplexHeatmap)
library(circlize)
source("scripts/chris/load_chrStructure.R")
source("scripts/ckn_utils.R")
source("scripts/load_hic_data.R")

#####
countCDBSInTad <- function(tads, diffbind) {
  tad_id <- paste(seqnames(tads), start(tads), end(tads), sep = "_")
  df <- data.frame("tad_id" = tad_id)
  
  for (set in names(diffbind)) {
    regions <- diffbind[[set]]
    countInTad <- countOverlaps(tads, regions)
    df <- cbind(df, countInTad)
  }
  colnames(df) <- c("tad_id", names(diffbind))
  return(df)
}

######
computeRatio <- function(vec_up, vec_down) {
  res <- rep("TMP", length(vec_up))
  for (i in 1:length(vec_up)) {
    val_up <- vec_up[i]
    val_down <- vec_down[i]
    if (val_up == 0 & val_down == 0) {
      res[i] = -1
    } else {
      res[i] = val_up/(val_up+val_down)
    }
  }
  return(as.numeric(res))
}

######
saveHeatmap <- function(heatmap, output_dir, output_file, width_val = 25, height_val = 22, format = "pdf") {
  output_filepath <- file.path(output_dir, paste0(output_file, ".", format))
  pdf(file = output_filepath, width = width_val, height = height_val)
  print(heatmap)
  dev.off()
  message(" > Heatmap saved in ", output_filepath)
}

######
filterN <- function(countTad_cof, n, cof) {
  res <- list()
  df_res <- countTad_cof
  tot_tad <- nrow(countTad_cof)
  nb_tad <- tot_tad - sum(countTad_cof[, 2] == -1)
  for (i in c(1:4, seq(5, n, 5))) {
    n_column <- ifelse(countTad_cof[, 3] > i, countTad_cof[, 2], -1)
    nb_tad_n <- tot_tad - sum(n_column == -1)
    df_res <- cbind(df_res, n_column)
    nb_tad <- c(nb_tad, nb_tad_n)
  }
  cnames_filterN <- paste0("n > ", c(1:4, seq(5, n, 5)))
  colnames(df_res) <- c(colnames(countTad_cof), cnames_filterN)
  
  res$df <- df_res
  res$nb_tad <- nb_tad
  
  return(res)
}

##### Function to parse Hits object obtain from findOverlaps
get_genes_perTad <- function(tads, feature) {
  ov <- findOverlaps(tads, feature)
  query <- queryHits(ov)
  subject <- subjectHits(ov)
  res <- vector("list", length(tads))
  for (i in 1:length(ov)) {
    res[[query[i]]] <- c(res[[query[i]]], names(feature)[subject[i]])
  }
  
  res_unique <- lapply(res, unique)
  res_length <- lapply(res_unique, length)
  
  output <- list()
  output$genesPerTad <- res_unique
  output$nbGenesPerTad <- unlist(res_length)
  
  return(output)
}

###### Color TADs according to CDBS ratio for visualisation in Juicer
colorTADs <- function(CDBSratio) {
  colors <- vector(mode = "character", length = length(CDBSratio))
  categories <- vector(mode = "character", length = length(CDBSratio))
  # only up | R = 1
  colors[CDBSratio == 1] <- "139,0,0" # darkred
  categories[CDBSratio == 1] <- "only_up"
  # up > down | 0.75 <= R < 1 
  colors[CDBSratio >= 0.75 & CDBSratio < 1] <- "205,92,92" # indianred
  categories[CDBSratio >= 0.75 & CDBSratio < 1] <- "up>down+"
  # up > down | 0.5 < R < 0.75
  colors[CDBSratio > 0.5 & CDBSratio < 0.75] <- "250,128,114" # salmon
  categories[CDBSratio > 0.5 & CDBSratio < 0.75] <- "up>down-"
  # up == down | R = 0.5
  colors[CDBSratio == 0.5] <- "255,215,0" # gold
  categories[CDBSratio == 0.5] <- "up=down"
  # down > up | 0.25 <= R < 0.5 
  colors[CDBSratio >= 0.25 & CDBSratio < 0.5] <- "144,238,144" # lightgreen
  categories[CDBSratio >= 0.25 & CDBSratio < 0.5] <- "down>up+"
  # down > up | 0 < R < 0.25
  colors[CDBSratio > 0 & CDBSratio < 0.25] <- "50,205,50" # limegreen
  categories[CDBSratio > 0 & CDBSratio < 0.25] <- "down>up-"
  # only down | R = 0
  colors[CDBSratio == 0] <- "0,100,0" # darkgreen
  categories[CDBSratio == 0] <- "only_down"
  # no binding | R = -1
  colors[CDBSratio == -1] <- "190,190,190" # X11gray
  categories[CDBSratio == -1] <- "no_binding"
  return(list("colors" = colors, "categories" = categories))
}

# today
today <- get_today()

# Output dir
outputdir <- "output/analyses/ecosystem/heatmap_cofactor_perTad_filterCounts_moreAnnot"

# Load reference genome
raw_promoters_A549 <- promoters(gencode31_basic_chr_TxDb, upstream = 1000, downstream = 1000, columns = "gene_id")
stdchr_promoters_A549 <- keepStdChr(raw_promoters_A549)
ensg_without_version <- str_replace(unlist(stdchr_promoters_A549$gene_id), "\\.[0-9]+$", "")
stdchr_promoters_A549$gene_id <- ensg_without_version
names(stdchr_promoters_A549) <- stdchr_promoters_A549$gene_id

##### Load categories
deg <- readRDS(file = "output/analyses/deg0_6_fdr10.rds")
upreg <- deg$gene_list$FC0p5$upreg # 1061
downreg <- deg$gene_list$FC0p5$downreg # 764
# Retrieve coordinates of all the transcripts of up and downreg
upreg_ranges <- stdchr_promoters_A549[names(stdchr_promoters_A549) %in% upreg] # 3177
downreg_ranges <- stdchr_promoters_A549[names(stdchr_promoters_A549) %in% downreg] # 2508

# Load diffbind
diffbind <- load_diffbind_cofactors_peaks()

# Build MED1_BRD4_CDK9 and MED1_BRD4
MBC_UP <- GenomicRanges::reduce(c(diffbind[["MED1_UP"]], diffbind[["BRD4_UP"]], diffbind[["CDK9_UP"]])) # 8253
MBC_DOWN <- GenomicRanges::reduce(c(diffbind[["MED1_DOWN"]], diffbind[["BRD4_DOWN"]], diffbind[["CDK9_DOWN"]])) # 7492
MBC_UNBIASED <- GenomicRanges::reduce(c(diffbind[["MED1_UNBIASED"]], diffbind[["BRD4_UNBIASED"]], diffbind[["CDK9_UNBIASED"]])) # 22749
MB_UP <- GenomicRanges::reduce(c(diffbind[["MED1_UP"]], diffbind[["BRD4_UP"]])) # 8195
MB_DOWN <- GenomicRanges::reduce(c(diffbind[["MED1_DOWN"]], diffbind[["BRD4_DOWN"]])) # 7221
MB_UNBIASED <- GenomicRanges::reduce(c(diffbind[["MED1_UNBIASED"]], diffbind[["BRD4_UNBIASED"]])) # 20605

# Load GR binding
all_gr_regions <- load_reddy_gr_binding_consensus()
gr_regions <- GRangesList(c(all_gr_regions[grep("minutes", names(all_gr_regions))], "1 hour" = all_gr_regions[["1 hour"]]))
gr_regions <- unlist(gr_regions)

# Set of peaks to compare with
diffbind_bis <- append(diffbind,
                       GRangesList("MBC_UP" = MBC_UP, "MBC_DOWN" = MBC_DOWN, "MBC_UNBIASED" = MBC_UNBIASED,
                                   "MB_UP" = MB_UP, "MB_DOWN" = MB_DOWN, "MB_UNBIASED" = MB_UNBIASED,
                                   "GR" = gr_regions))

# Parameters
# resolution <- c("5000", "10000", "25000", "50000", "Reddy_0h", "Reddy_1h")
resolution <- c("Reddy_1h")
with_clustering <- FALSE
# cofactors <- c("MED1", "BRD4", "CDK9", "NIPBL", "SMC1A", "MBC", "MB")
cofactors <- c("MBC")
customColors = colorRamp2(c(-1, -0.1, 0, 0.5, 1), c("black", "black", "#1E8449", "white", "#800020"))
makeBEDPE = FALSE

allTADs_ratio <- list()

for (res in resolution) {
  # Loading TADs and count CDBS per TADs
  if (res == "Reddy_0h") {
    TADs <- load_reddy_hic()[["Ctrl"]]$TAD
  } else if (res == "Reddy_1h") {
    TADs <- load_reddy_hic()[["1 hour"]]$TAD
  } else {
    TADs <- load_TADs(timepoint = "1h", resolution = res, method_norm = "VC_SQRT")
  }
  n_tad <- length(TADs)
  countTad <- countCDBSInTad(TADs, diffbind_bis)
  
  countTad_genes <- tibble("UPREG" = get_genes_perTad(TADs, upreg_ranges)$nbGenesPerTad,
                               "DOWNREG" = get_genes_perTad(TADs, downreg_ranges)$nbGenesPerTad,
                               "UPREG_names" = get_genes_perTad(TADs, upreg_ranges)$genesPerTad,
                               "DOWNREG_names" = get_genes_perTad(TADs, downreg_ranges)$genesPerTad)
  
  countTad_ratio <- countTad %>% mutate(MED1_R = computeRatio(MED1_UP, MED1_DOWN),
                                        BRD4_R = computeRatio(BRD4_UP, BRD4_DOWN),
                                        CDK9_R = computeRatio(CDK9_UP, CDK9_DOWN),
                                        NIPBL_R = computeRatio(NIPBL_UP, NIPBL_DOWN),
                                        SMC1A_R = computeRatio(SMC1A_UP, SMC1A_DOWN),
                                        MBC_R = computeRatio(MBC_UP, MBC_DOWN),
                                        MB_R = computeRatio(MB_UP, MB_DOWN),
                                        MED1_S = MED1_UP + MED1_DOWN,
                                        BRD4_S = BRD4_UP + BRD4_DOWN,
                                        CDK9_S = CDK9_UP + CDK9_DOWN,
                                        NIPBL_S = NIPBL_UP + NIPBL_DOWN,
                                        SMC1A_S = SMC1A_UP + SMC1A_DOWN,
                                        MBC_S = MBC_UP + MBC_DOWN,
                                        MB_S = MB_UP + MB_DOWN,
                                        width_TADs = width(TADs))
  
  allTADs_ratio[[res]] <- countTad_ratio
  
  countTad_ratio <- cbind(countTad_ratio, countTad_genes)
  
  for (cof in cofactors) {
    countTad_cof <- countTad_ratio %>% dplyr::select(tad_id, paste0(cof, "_R"), paste0(cof, "_S")) %>%
      arrange(desc(.[[2]]), desc(.[[3]]))
    n_no_binding <- sum(countTad_cof[, 2] == -1)
    
    countTad_cof_annot <- countTad_ratio %>%
      dplyr::select(tad_id, paste0(cof, "_R"), paste0(cof, "_S"), paste0(cof, "_UP"), paste0(cof, "_DOWN"), paste0(cof, "_UNBIASED"),
                    width_TADs, GR, UPREG, DOWNREG, UPREG_names, DOWNREG_names) %>%
      arrange(desc(.[[2]]), desc(.[[3]])) %>%
      mutate("UP" = .[[4]], "DOWN" = .[[5]], "UNBIASED" = .[[6]]) %>% 
      dplyr::select(UP, DOWN, UNBIASED, width_TADs, GR, UPREG, DOWNREG) %>% as.matrix
    
    filtered_list <- filterN(countTad_cof, n = 50, cof)
    countTad_cof_filterN <- filtered_list$df
    
    countTad_mat <- countTad_cof_filterN %>% dplyr::select(-tad_id, c(-3)) %>% as.matrix
    
    # Ditribution of cofactor ratio
    cof_ratio <- countTad_cof %>% filter(.[[2]] != -1) %>% dplyr::select(paste0(cof, "_R"))
    distrib_title <- paste("TADs", "(Resolution", "=", paste0(res, ")\nDistribution of"), cof, "ratio")
    distribR <- cof_ratio %>% 
      ggplot(aes(x = .[[1]])) +
      geom_histogram(binwidth=0.1, color="#e9ecef", fill = "#69b3a2") +
      ggtitle(distrib_title) +
      scale_x_continuous(breaks = seq(0, 1, 0.1), labels = seq(0, 1, 0.1),
                         name = paste(cof, "ratio")) +
      theme_minimal()
    
    # Are GR always in those TADs?
    TADvsGR <- countTad_ratio %>%
      dplyr::select(tad_id, paste0(cof, "_R"), paste0(cof, "_S"), paste0(cof, "_UP"), paste0(cof, "_DOWN"), GR) %>% 
      dplyr::filter(.[[2]] != -1)
    
    withGR <- TADvsGR %>% dplyr::filter(GR != 0)
   #  hist(withGR$GR, breaks = 300, xlab = "Number of GR", ylab = "Number of TADs")
    # hist(withGR$MBC_R)
    # hist(withGR$MBC_UP)
   #  hist(withGR$MBC_DOWN)
    table(withGR$MBC_R)
    table(withGR$MBC_UP)
    table(withGR$MBC_DOWN)
    
    noGR <- TADvsGR %>% dplyr::filter(GR == 0)
    # hist(noGR$MBC_R)
    # hist(noGR$MBC_UP)
    # hist(noGR$MBC_DOWN, breaks = 10, xlab = "Number of MBC_DOWN", ylab = "Number of TADs with no GR")
    table(noGR$MBC_R)
    table(noGR$MBC_UP)
    table(noGR$MBC_DOWN)
    
    # Annotation top
    annot_top <- HeatmapAnnotation("TADs" = anno_text(filtered_list$nb_tad,
                                                    rot = 0,
                                                    just = "center",
                                                    location = 0.5,
                                                    gp = gpar(fontsize = 8),
                                                    height = unit(0.4, "cm")),
                                   "percentTAD" = anno_text(paste(round(filtered_list$nb_tad/n_tad*100, 1), "%", sep = " "),
                                                            rot = 0,
                                                            just = "center",
                                                            location = 0.5,
                                                            gp = gpar(fontsize = 5),
                                                            height = unit(0.2, "cm")),
                                   "# of TADs\nwith binding" = anno_barplot(filtered_list$nb_tad,
                                                                       border = FALSE,
                                                                       gp = gpar(fill = "black"),
                                                                       height = unit(2, "cm")),
                                   empty = anno_empty(border = FALSE, height = unit(1, "mm")),
                                   show_annotation_name = c(TRUE, TRUE),
                                   annotation_name_offset = unit(10, "mm"),
                                   annotation_name_side = "left",
                                   annotation_name_gp = gpar(fontsize = 10))
    
    # annotation left
    annot_left <- rowAnnotation("# of DOWNREG" = anno_barplot(countTad_cof_annot[, "DOWNREG"],
                                                              gp = gpar(col = c("#3BC14A")),
                                                              width = unit(3, "cm"),
                                                              axis_param = list(
                                                                direction = "reverse",
                                                                side = "bottom",
                                                                labels_rot = 0),
                                                              ylim = c(0, 10)),
                                "# of UPREG" = anno_barplot(countTad_cof_annot[, "UPREG"],
                                                           gp = gpar(col = c("#FA8072")),
                                                           width = unit(3, "cm"),
                                                           axis_param = list(
                                                             side = "bottom",
                                                             labels_rot = 0),
                                                           ylim = c(0, 10)),
                                "# of UNBIASED" = anno_barplot(countTad_cof_annot[, "UNBIASED"],
                                                           gp = gpar(col = c("#2F4F4F")),
                                                           width = unit(6, "cm"),
                                                           axis_param = list(
                                                             side = "bottom",
                                                             at = c(0, 25, 50, 75, 100, 125),
                                                             labels_rot = 0),
                                                           ylim = c(0, 125)),
                                "# of DOWN" = anno_barplot(countTad_cof_annot[, "DOWN"],
                                                         gp = gpar(col = c("#ffd700")),
                                                         width = unit(3, "cm"),
                                                         axis_param = list(
                                                           direction = "reverse",
                                                           side = "bottom",
                                                           at = c(0, 25, 50),
                                                           labels_rot = 0),
                                                         ylim = c(0, 50)),
                                "# of UP" = anno_barplot(countTad_cof_annot[, "UP"],
                                                      gp = gpar(col = c("#4682B4")),
                                                      width = unit(3, "cm"),
                                                      axis_param = list(
                                                        side = "bottom",
                                                        at = c(0, 25, 50),
                                                        labels_rot = 0),
                                                      ylim = c(0, 50)),
                                gap = unit(3, "mm"))
    
    # annotation right
    annot_right <- rowAnnotation("TAD widths" = anno_barplot(countTad_cof_annot[, "width_TADs"],
                                                            gp = gpar(col = c("#9370DB")),
                                                            width = unit(3, "cm"),
                                                            axis_param = list(
                                                              side = "bottom",
                                                              labels_rot = 0)),
                                "# of GR" = anno_barplot(countTad_cof_annot[, "GR"],
                                                         gp = gpar(col = c("#DF6D14")),
                                                         width = unit(3, "cm"),
                                                         axis_param = list(
                                                           side = "bottom",
                                                           labels_rot = 0)),
                                gap = unit(3, "mm"))

    hm <- Heatmap(countTad_mat, name = "ratio",
                  top_annotation = annot_top, left_annotation = annot_left, right_annotation = annot_right,
                  cluster_rows = with_clustering,
                  show_row_names = FALSE,
                  cluster_columns = FALSE,
                  column_dend_side = "bottom",
                  col = customColors,
                  column_names_side = "top", column_names_rot = 45,
                  column_names_gp = gpar(fontsize = 10),
                  na_col = "black",
                  row_title = paste0(n_tad, " TADs"),
                  # row_title_rot = 0,
                  column_title = paste("TADs", "(Resolution", "=", res, "bp)\n",
                                       cof, "ratio"),
                  column_title_gp = gpar(fontsize = 15, fontface = "bold"))
    outputfile <- paste(today, "heatmap", "CDBS", "perTAD", "ratio", paste0("res", res), cof, "filterN", "moreAnnot", sep = "_")
    # saveHeatmap(hm, output_dir = outputdir, output_file = outputfile, width_val = 18, height_val = 11)
    
    # readline(prompt="Press [enter] to continue")
    
    ###### Make BEDPE or visualisation in Juicer
    ###### Color TADs according to CDBS ratio
    if (makeBEDPE) {
      bedpe_output_dir <- "output/analyses/ecosystem/colorTADs_perCDBSRatio"
      bedpe_output_filename <- paste("coloredTADsPerCDBSRatio", paste0("res", res), paste0(cof, ".bedpe"), sep = "_")
      bedpe_output_filepath <- file.path(bedpe_output_dir, bedpe_output_filename)
      
      coord <- countTad_cof$tad_id
      splitCoord <- str_split(coord, pattern = "_")
      chrs <- splitCoord %>% purrr::map(1) %>% unlist
      start <- splitCoord %>% purrr::map(2) %>% unlist
      end <- splitCoord %>% purrr::map(3) %>% unlist
      ratioCDBS <- countTad_cof[[paste(cof, "R", sep = "_")]]
      sumCDBS <- countTad_cof[[paste(cof, "S", sep = "_")]]
      color <- colorTADs(ratioCDBS)$colors
      name <- "."
      score <- "."
      strand1 <- "."
      strand2 <- "."
      bedpe <- data.frame(chrs, start, end, chrs, start, end, name, score, strand1, strand2, color)
      colnames(bedpe) <- c("#chr1", "x1", "x2", "chr2", "y1", "y2", "name", "score", "strand1", "strand2", "color")
      
      write.table(bedpe, file = bedpe_output_filepath, quote = FALSE, row.names = FALSE, sep = "\t")
      message(" > BEDPE saved in ", bedpe_output_filepath)
      
      ### Stats on chr19 (to remove later)
      tmpdf <- data.frame(chrs, category = colorTADs(ratioCDBS)$categories) %>% dplyr::filter(chrs == "chr19") %>% 
        dplyr::pull(category)
      table(tmpdf)
      round(table(tmpdf)/277*100, 2)
      
    }
  }
  # readline(prompt="Press [enter] to continue")
}

# saveRDS(allTADs_ratio, file = "output/hic/allTADs_ratio.rds")
