# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

library(ComplexHeatmap)
library(circlize)
source("scripts/chris/load_chrStructure.R")
source("scripts/ckn_utils.R")

#####
countCBDSInTad <- function(tads, diffbind) {
  tad_id <- paste(seqnames(tads), start(tads), end(tads), sep = "_")
  df <- data.frame("tad_id" = tad_id)
  
  for (set in names(diffbind)) {
    regions <- diffbind[[set]]
    countInTad <- countOverlaps(tads, regions)
    df <- cbind(df, countInTad)
  }
  colnames(df) <- c("tad_id", names(diffbind))
  return(df)
}

######
computeRatio <- function(vec_up, vec_down) {
  res <- rep("TMP", length(vec_up))
  for (i in 1:length(vec_up)) {
    val_up <- vec_up[i]
    val_down <- vec_down[i]
    if (val_up == 0 & val_down == 0) {
      res[i] = -1
    } else {
      res[i] = val_up/(val_up+val_down)
    }
  }
  return(as.numeric(res))
}

######
saveHeatmap <- function(heatmap, output_dir, output_file, width_val = 25, height_val = 22, format = "pdf") {
  output_filepath <- file.path(output_dir, paste0(output_file, ".", format))
  pdf(file = output_filepath, width = width_val, height = height_val)
  print(heatmap)
  dev.off()
  message(" > Heatmap saved in ", output_filepath)
}

######
filterN <- function(countTad_cof, n, cof) {
  res <- list()
  df_res <- countTad_cof
  tot_tad <- nrow(countTad_cof)
  nb_tad <- tot_tad - sum(countTad_cof[, 2] == -1)
  for (i in c(1:4, seq(5, n, 5))) {
    n_column <- ifelse(countTad_cof[, 3] > i, countTad_cof[, 2], -1)
    nb_tad_n <- tot_tad - sum(n_column == -1)
    df_res <- cbind(df_res, n_column)
    nb_tad <- c(nb_tad, nb_tad_n)
  }
  cnames_filterN <- paste0("n > ", c(1:4, seq(5, n, 5)))
  colnames(df_res) <- c(colnames(countTad_cof), cnames_filterN)
  
  res$df <- df_res
  res$nb_tad <- nb_tad
  
  return(res)
}

# today
today <- get_today()

# Output dir
outputdir <- "output/analyses/ecosystem/heatmap_cofactor_perTad_filterCounts"

# Load diffbind
diffbind <- load_diffbind_cofactors_peaks()

# Build MED1_BRD4_CDK9 and MED1_BRD4
MBC_UP <- GenomicRanges::reduce(c(diffbind[["MED1_UP"]], diffbind[["BRD4_UP"]], diffbind[["CDK9_UP"]])) # 8253
MBC_DOWN <- GenomicRanges::reduce(c(diffbind[["MED1_DOWN"]], diffbind[["BRD4_DOWN"]], diffbind[["CDK9_DOWN"]])) # 7492
MB_UP <- GenomicRanges::reduce(c(diffbind[["MED1_UP"]], diffbind[["BRD4_UP"]])) # 8195
MB_DOWN <- GenomicRanges::reduce(c(diffbind[["MED1_DOWN"]], diffbind[["BRD4_DOWN"]])) # 7221
diffbind_bis <- append(diffbind,
                       GRangesList("MBC_UP" = MBC_UP, "MBC_DOWN" = MBC_DOWN,
                                   "MB_UP" = MB_UP, "MB_DOWN" = MB_DOWN))

# Parameters
resolution <- c("5000", "10000", "25000", "50000")
with_clustering <- FALSE
cofactors <- c("MED1", "BRD4", "CDK9", "NIPBL", "SMC1A", "MBC", "MB")
customColors = colorRamp2(c(-1, -0.1, 0, 0.5, 1), c("black", "black", "#1E8449", "white", "#800020"))

for (res in resolution) {
  # Loading TADs and count CBDS per TADs
  TADs <- load_TADs(timepoint = "1h", resolution = res, method_norm = "VC_SQRT")
  n_tad <- length(TADs)
  countTad <- countCBDSInTad(TADs, diffbind_bis)
  
  countTad_ratio <- countTad %>% mutate(MED1_R = computeRatio(MED1_UP, MED1_DOWN),
                                        BRD4_R = computeRatio(BRD4_UP, BRD4_DOWN),
                                        CDK9_R = computeRatio(CDK9_UP, CDK9_DOWN),
                                        NIPBL_R = computeRatio(NIPBL_UP, NIPBL_DOWN),
                                        SMC1A_R = computeRatio(SMC1A_UP, SMC1A_DOWN),
                                        MBC_R = computeRatio(MBC_UP, MBC_DOWN),
                                        MB_R = computeRatio(MB_UP, MB_DOWN),
                                        MED1_S = MED1_UP + MED1_DOWN,
                                        BRD4_S = BRD4_UP + BRD4_DOWN,
                                        CDK9_S = CDK9_UP + CDK9_DOWN,
                                        NIPBL_S = NIPBL_UP + NIPBL_DOWN,
                                        SMC1A_S = SMC1A_UP + SMC1A_DOWN,
                                        MBC_S = MBC_UP + MBC_DOWN,
                                        MB_S = MB_UP + MB_DOWN)
  for (cof in cofactors) {
    countTad_cof <- countTad_ratio %>% dplyr::select(tad_id, paste0(cof, "_R"), paste0(cof, "_S")) %>%
      arrange(desc(.[[2]]), desc(.[[3]]))
    n_no_binding <- sum(countTad_cof[, 2] == -1)
    
    countTad_cof_annot <- countTad_ratio %>% dplyr::select(tad_id, paste0(cof, "_R"), paste0(cof, "_S"), paste0(cof, "_UP"), paste0(cof, "_DOWN")) %>%
      arrange(desc(.[[2]]), desc(.[[3]])) %>% mutate("UP" = .[[4]], "DOWN" = .[[5]]) %>% 
      dplyr::select(UP, DOWN) %>% as.matrix
    
    filtered_list <- filterN(countTad_cof, n = 50, cof)
    countTad_cof_filterN <- filtered_list$df
    
    countTad_mat <- countTad_cof_filterN %>% dplyr::select(-tad_id, c(-3)) %>% as.matrix
    
    # Annotation top
    annot_top <- HeatmapAnnotation("TADs" = anno_text(filtered_list$nb_tad,
                                                    rot = 0,
                                                    just = "center",
                                                    location = 0.5,
                                                    gp = gpar(fontsize = 8),
                                                    height = unit(0.4, "cm")),
                                   "percentTAD" = anno_text(paste(round(filtered_list$nb_tad/n_tad*100, 1), "%", sep = " "),
                                                            rot = 0,
                                                            just = "center",
                                                            location = 0.5,
                                                            gp = gpar(fontsize = 5),
                                                            height = unit(0.2, "cm")),
                                   "# of TADs\nwith binding" = anno_barplot(filtered_list$nb_tad,
                                                                       border = FALSE,
                                                                       gp = gpar(fill = "black"),
                                                                       height = unit(2, "cm")),
                                   empty = anno_empty(border = FALSE, height = unit(1, "mm")),
                                   show_annotation_name = c(TRUE, TRUE),
                                   annotation_name_offset = unit(10, "mm"),
                                   annotation_name_side = "left",
                                   annotation_name_gp = gpar(fontsize = 10))
    
    # annotation left
    annot_left <- rowAnnotation("# of DOWN" = anno_barplot(countTad_cof_annot[, "DOWN"],
                                                         gp = gpar(col = c("#ffd700")),
                                                         width = unit(5, "cm"),
                                                         axis_param = list(
                                                          direction = "reverse",
                                                          side = "bottom",
                                                          at = c(0, 10, 20, 30, 40, 50),
                                                           labels_rot = 0),
                                                         ylim = c(0, 50)),
                                "# of UP" = anno_barplot(countTad_cof_annot[, "UP"],
                                                      gp = gpar(col = c("#4682B4")),
                                                      width = unit(5, "cm"),
                                                      axis_param = list(
                                                        side = "bottom",
                                                        at = c(0, 10, 20, 30, 40, 50),
                                                        labels_rot = 0),
                                                      ylim = c(0, 50)),
                                gap = unit(3, "mm"))

    hm <- Heatmap(countTad_mat, name = "ratio",
                  top_annotation = annot_top, left_annotation = annot_left,
                  cluster_rows = with_clustering,
                  show_row_names = FALSE,
                  cluster_columns = FALSE,
                  column_dend_side = "bottom",
                  col = customColors,
                  column_names_side = "top", column_names_rot = 45,
                  column_names_gp = gpar(fontsize = 10),
                  na_col = "black",
                  row_title = paste0(n_tad, " TADs"),
                  # row_title_rot = 0,
                  column_title = paste("TADs", "(Resolution", "=", res, "bp)\n",
                                       cof, "ratio"),
                  column_title_gp = gpar(fontsize = 15, fontface = "bold"))
    outputfile <- paste(today, "heatmap", "CBDS", "perTAD", "ratio", paste0("res", res), cof, "filterN", sep = "_")
    saveHeatmap(hm, output_dir = outputdir, output_file = outputfile, width_val = 11, height_val = 11)
    
    # readline(prompt="Press [enter] to continue")
  }
  readline(prompt="Press [enter] to continue")
}
