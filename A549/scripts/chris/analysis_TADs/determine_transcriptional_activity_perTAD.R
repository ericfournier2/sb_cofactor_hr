# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

library(tidyverse)
library(GenomicRanges)
library(viridis)
source("scripts/ckn_utils.R")

# set direction
set_TAD_direction <- function(ratio, sumcof) {
  res <- rep("TMP", length(ratio))
  res[ratio == 1 & sumcof > 9] <- "TAD_onlyUP_10AndMoreCof"
  res[ratio == 1 & sumcof == 9] <- "TAD_onlyUP_9Cof"
  res[ratio == 1 & sumcof == 8] <- "TAD_onlyUP_8Cof"
  res[ratio == 1 & sumcof == 7] <- "TAD_onlyUP_7Cof"
  res[ratio == 1 & sumcof == 6] <- "TAD_onlyUP_6Cof"
  res[ratio == 1 & sumcof == 5] <- "TAD_onlyUP_5Cof"
  res[ratio == 1 & sumcof == 4] <- "TAD_onlyUP_4Cof"
  res[ratio == 1 & sumcof == 3] <- "TAD_onlyUP_3Cof"
  res[ratio == 1 & sumcof == 2] <- "TAD_onlyUP_2Cof"
  res[ratio == 1 & sumcof == 1] <- "TAD_onlyUP_1Cof"
  res[ratio >= 0.6 & ratio < 1] <- "TAD_UP"
  res[ratio <= 0.4 & ratio > 0] <- "TAD_DOWN"
  res[ratio == 0 & sumcof == 1] <- "TAD_onlyDOWN_1Cof"
  res[ratio == 0 & sumcof == 2] <- "TAD_onlyDOWN_2Cof"
  res[ratio == 0 & sumcof == 3] <- "TAD_onlyDOWN_3Cof"
  res[ratio == 0 & sumcof == 4] <- "TAD_onlyDOWN_4Cof"
  res[ratio == 0 & sumcof == 5] <- "TAD_onlyDOWN_5Cof"
  res[ratio == 0 & sumcof == 6] <- "TAD_onlyDOWN_6Cof"
  res[ratio == 0 & sumcof == 7] <- "TAD_onlyDOWN_7Cof"
  res[ratio == 0 & sumcof == 8] <- "TAD_onlyDOWN_8Cof"
  res[ratio == 0 & sumcof == 9] <- "TAD_onlyDOWN_9Cof"
  res[ratio == 0 & sumcof > 9] <- "TAD_onlyDOWN_10AndMoreCof"
  res[ratio == -1] <- "TAD_NB"
  res[ratio > 0.4 & ratio < 0.6] <- "TAD_UNBIASED"
  return(res)
}

# makeGRanges
makeGRanges <- function(tad_df) {
  seqnames <- tad_df$tad_id %>% str_split("_") %>% purrr::map(1) %>% unlist
  start <- tad_df$tad_id %>% str_split("_") %>% purrr::map(2) %>% unlist %>% as.numeric
  end <- tad_df$tad_id %>% str_split("_") %>% purrr::map(3) %>% unlist %>% as.numeric
  TADs <- GRanges(seqnames = seqnames, IRanges(start, end))
  mcols(TADs) <- tad_df %>% dplyr::select(-tad_id)
  return(TADs)
}

# splitTADPerRatio
splitTADPerRatio <- function(tad_df) {
  tad_up <- tad_df %>% dplyr::filter(direction == "TAD_UP") %>% makeGRanges
  tad_down <- tad_df %>% dplyr::filter(direction == "TAD_DOWN") %>% makeGRanges
  tad_unbiased <- tad_df %>% dplyr::filter(direction == "TAD_UNBIASED") %>% makeGRanges
  tad_nb <- tad_df %>% dplyr::filter(direction == "TAD_NB") %>% makeGRanges
  return(GRangesList("TAD_UP" = tad_up, "TAD_DOWN" = tad_down, "TAD_UNBIASED" = tad_unbiased, "TAD_NB" = tad_nb))
}

#####
get_expression_perTAD <- function(genesPerTad, gene_expr) {
  tmp <- gene_expr[gene_expr$gene_id %in% genesPerTad, ]
  expr_sum <- colSums(as.matrix(tmp$expr))
  # message(expr_sum)
  return(expr_sum)
}

#####
get_expression <- function(genesPerTad_list, gene_expr) {
  nbTad <- length(genesPerTad_list)
  exprsTad <- vector("numeric", nbTad)
  for (i in 1:nbTad) {
    genesPerTad <- genesPerTad_list[[i]]
    expr_sum <- get_expression_perTAD(genesPerTad, gene_expr)
    exprsTad[i] <- expr_sum
  }
  return(exprsTad)
}

get_overlapping_features <- function(anchor, feature, prefix) {
  anchorGR <- makeGRanges(anchor)
  if (prefix != FALSE) {
    names(feature) <- paste(prefix, 1:length(feature), sep = "_")
  }
  ov <- findOverlaps(anchorGR, feature)
  query <- queryHits(ov)
  subject <- subjectHits(ov)
  res <- vector("list", length(anchorGR))
  
  for (i in 1:length(ov)) {
    res[[query[i]]] <- c(res[[query[i]]], names(feature)[subject[i]])
  }
  return(res)
}
########################################################################################################

# Aim determine the variation of transcriptional activity between 0 and 1 hour in each TAD
# Then, compute a ratio of transcriptional activity (TA): TA_1h / TA_0h > log2
# Assess correlation with cofactor_Ratio

# Load annotation database
raw_promoters_A549 <- promoters(gencode31_basic_chr_TxDb, upstream = 3000, downstream = 3000, columns = "gene_id")
stdchr_promoters_A549 <- keepStdChr(raw_promoters_A549)
ensg_without_version <- str_replace(unlist(stdchr_promoters_A549$gene_id), "\\.[0-9]+$", "")
stdchr_promoters_A549$gene_id <- ensg_without_version
names(stdchr_promoters_A549) <- stdchr_promoters_A549$gene_id

#####
asRPM <- function(raw_counts) {
  mat_counts <- raw_counts[, -1] %>% as.matrix
  library_size <- colSums((mat_counts))
  rpm <- (mat_counts/library_size)*1000000
  df <- data.frame("gene_id" = raw_counts$gene_id, rpm)
  return(df)
}

#raw_counts <- rnaseq1h_raw

# Load gene expression at 0 and 1 hour
rnaseq_raw <- read_tsv("results/a549_dex_time_points/raw_counts_with_colnames.txt")
rnaseq1h_rpm <- rnaseq_raw %>% dplyr::select(gene_id, contains("1h")) %>% asRPM
rnaseq0h_rpm <- rnaseq_raw %>% dplyr::select(gene_id, starts_with("0h")) %>% asRPM
expression_1h <- rowMeans(as.matrix(rnaseq1h_rpm[, -1]))
expression_0h <- rowMeans(as.matrix(rnaseq0h_rpm[, -1]))
rnaseq1h <- data.frame(gene_id = rnaseq1h_rpm$gene_id, expr = expression_1h)
rnaseq0h <- data.frame(gene_id = rnaseq0h_rpm$gene_id, expr = expression_0h)

#### Load TADs_ratio and split TADs into 3 categories:
# * TAD_UNBIASED : 0,4 < R < 0,6
# * TAD_UP: R > 0,6
# * TAD_DOWN : R < 0,4
allTADs_ratio <- readRDS(file = "output/hic/allTADs_ratio.rds")
TADs_Reddy_1h <- allTADs_ratio[["Reddy_1h"]]
TADs_Reddy_1h <- TADs_Reddy_1h %>% mutate(direction = set_TAD_direction(MBC_R, MBC_S))
TADs_Reddy_1h$direction <- factor(TADs_Reddy_1h$direction,
                                  levels = c("TAD_onlyDOWN_10AndMoreCof", "TAD_onlyDOWN_9Cof",
                                             "TAD_onlyDOWN_8Cof", "TAD_onlyDOWN_7Cof",
                                             "TAD_onlyDOWN_6Cof", "TAD_onlyDOWN_5Cof",
                                             "TAD_onlyDOWN_4Cof", "TAD_onlyDOWN_3Cof",
                                             "TAD_onlyDOWN_2Cof", "TAD_onlyDOWN_1Cof",
                                             "TAD_DOWN",
                                             "TAD_UNBIASED",
                                             "TAD_UP",
                                             "TAD_onlyUP_1Cof", "TAD_onlyUP_2Cof",
                                             "TAD_onlyUP_3Cof", "TAD_onlyUP_4Cof",
                                             "TAD_onlyUP_5Cof", "TAD_onlyUP_6Cof",
                                             "TAD_onlyUP_7Cof", "TAD_onlyUP_8Cof",
                                             "TAD_onlyUP_9Cof", "TAD_onlyUP_10AndMoreCof",
                                             "TAD_NB"))
table(TADs_Reddy_1h$direction)
# TADs_list <- splitTADPerRatio(TADs_Reddy_1h)
# sapply(TADs_list, length)

### Distribution of MBC_ratio
TADs_Reddy_1h %>% dplyr::filter(MBC_R != -1) %>% dplyr::select(MBC_R) %>% ggplot(aes(x=MBC_R)) +
  geom_histogram(binwidth=0.1, color="#e9ecef", alpha=0.8, position = 'dodge') +
  scale_color_manual(values=c("#69b3a2", "#404080", "red", "yellow")) +
  theme_minimal() +
  labs(fill="") +
  scale_x_continuous(breaks = seq(0, 1, 0.1), labels = seq(0, 1, 0.1))



distribR <- data_R_val %>%
  ggplot( aes(x=TAD_R, fill=Type)) +
  geom_histogram(binwidth=0.1, color="#e9ecef", alpha=0.8, position = 'dodge') +
  scale_fill_manual(values=c("#69b3a2", "#404080", "red", "yellow")) +
  theme_minimal() +
  labs(fill="") +
  ggtitle(distrib_title) +
  scale_x_continuous(breaks = seq(0, 1, 0.1), labels = seq(0, 1, 0.1))


#### Get gene expression per TAD at 1 hour and 0 hour
genesPerTad <- get_overlapping_features(TADs_Reddy_1h, stdchr_promoters_A549, prefix = FALSE)
sumExprPerTad_1h <- get_expression(genesPerTad, rnaseq1h)
sumExprPerTad_0h <- get_expression(genesPerTad, rnaseq0h)
# calculate Ratio
ratioExprPerTad <- log2((sumExprPerTad_1h+1)/(sumExprPerTad_0h+1))
hist(ratioExprPerTad)
summary(ratioExprPerTad)
boxplot(ratioExprPerTad)

### Correlating with TAD status
TADs_Reddy_1h_withTA <- cbind(TADs_Reddy_1h, ratioExprPerTad)

# Ditribution of ExprRatio according to TAD status
R_val <- TADs_Reddy_1h_withTA %>% dplyr::select(direction, ratioExprPerTad) %>%
  arrange(desc(ratioExprPerTad)) %>% mutate(rank = 1:nrow(R_val))

R_val %>% ggplot(aes(x = direction, y = ratioExprPerTad, fill = direction)) +
  geom_boxplot(outlier.shape = NA) +
  theme_minimal() +
  geom_jitter(color = "black", size = 0.4, width = 0.25, alpha = 0.5) +
  scale_color_viridis(discrete=TRUE) +
  ylab("TranscriptionIndex") +
  theme(axis.text.x=element_blank())

# If keep top and bottom 100
R_val2 <- R_val %>% mutate(topbottom100 = ifelse(rank %in% c(1:100, nrow(R_val2):(nrow(R_val2)-99)), "topbottom", "none"))

R_val2 %>% ggplot(aes(x = direction, y = ratioExprPerTad, fill = direction)) +
  geom_boxplot(outlier.shape = NA) +
  theme_minimal() +
  geom_jitter(aes(color = topbottom100), size = 0.4, width = 0.25, alpha = 0.5) +
  scale_color_viridis(discrete=TRUE) +
  ylab("TranscriptionIndex") +
  theme(axis.text.x=element_blank()) +
  scale_color_manual(values = c("grey", "red"))

topR <- R_val2 %>% dplyr::filter(rank %in% c(1:100))
table(topR$direction) %>% as.data.frame %>%
  dplyr::filter(Freq != 0) %>% 
  plot_ly(labels = ~Var1, values = ~Freq,
          textinfo = "label+percent",
          insidetextfont = list(color = "#FFFFFF")) %>%
    add_pie(hole = 0.4)
topR$direction <- gsub("TAD_onlyUP.*", "TAD_onlyUP", topR$direction)

bottomR <- R_val2 %>% dplyr::filter(rank %in% nrow(R_val2):(nrow(R_val2)-99))
table(bottomR$direction) %>% as.data.frame %>%
  dplyr::filter(Freq != 0) %>% 
  plot_ly(labels = ~Var1, values = ~Freq,
          textinfo = "label+percent",
          insidetextfont = list(color = "#FFFFFF")) %>%
  add_pie(hole = 0.4)
bottomR$direction <- gsub("TAD_onlyDOWN.*", "TAD_onlyDOWN", bottomR$direction)
bottomR$direction <- gsub("TAD_onlyUP.*", "TAD_onlyUP", bottomR$direction)

####
# group_onlyUP <- R_val %>% dplyr::filter(direction == "TAD_onlyUP") %>% dplyr::pull(ratioExprPerTad) 
# group_UP <- R_val %>% dplyr::filter(direction == "TAD_UP") %>% dplyr::pull(ratioExprPerTad)
# group_UNBIASED <- R_val %>% dplyr::filter(direction == "TAD_UNBIASED") %>% dplyr::pull(ratioExprPerTad)
# group_DOWN <- R_val %>% dplyr::filter(direction == "TAD_DOWN") %>% dplyr::pull(ratioExprPerTad)
# group_onlyDOWN <- R_val %>% dplyr::filter(direction == "TAD_onlyDOWN") %>% dplyr::pull(ratioExprPerTad)
# group_NB <- R_val %>% dplyr::filter(direction == "TAD_NB") %>% dplyr::pull(ratioExprPerTad)

# Test de normalité
# shapiro.test(group_onlyUP)
# shapiro.test(group_UP)
# shapiro.test(group_UNBIASED)
# shapiro.test(group_DOWN)
# shapiro.test(group_onlyDOWN)
# shapiro.test(group_NB)
# 
# var.test(group_onlyUP, group_UP)
# var.test(group_onlyUP, group_UNBIASED)
# var.test(group_onlyUP, group_DOWN)
# var.test(group_onlyUP, group_onlyDOWN)
# var.test(group_onlyUP, group_NB)
# 
# var.test(group_UP, group_UNBIASED)
# var.test(group_UP, group_DOWN)
# var.test(group_UP, group_onlyDOWN)
# var.test(group_UP, group_NB)
# 
# var.test(group_UNBIASED, group_DOWN)
# var.test(group_UNBIASED, group_onlyDOWN)
# var.test(group_UNBIASED, group_NB)
# 
# var.test(group_DOWN, group_onlyDOWN)
# var.test(group_DOWN, group_NB)
# 
# var.test(group_onlyDOWN, group_NB)
# 
# t.test(group_onlyUP, group_UP)
# t.test(group_onlyUP, group_UNBIASED)
# t.test(group_onlyUP, group_DOWN)
# t.test(group_onlyUP, group_onlyDOWN)
# t.test(group_onlyUP, group_NB)
# 
# t.test(group_UP, group_UNBIASED)
# t.test(group_UP, group_DOWN)
# t.test(group_UP, group_onlyDOWN)
# t.test(group_UP, group_NB)
# 
# t.test(group_UNBIASED, group_DOWN)
# t.test(group_UNBIASED, group_onlyDOWN)
# t.test(group_UNBIASED, group_NB)
# 
# t.test(group_DOWN, group_onlyDOWN)
# t.test(group_DOWN, group_NB)
# 
# t.test(group_onlyDOWN, group_NB)
# 
# wilcox.test(group_onlyUP, group_UP)
# wilcox.test(group_onlyUP, group_UNBIASED)
# wilcox.test(group_onlyUP, group_DOWN)
# wilcox.test(group_onlyUP, group_onlyDOWN)
# wilcox.test(group_onlyUP, group_NB)
# 
# wilcox.test(group_UP, group_UNBIASED)
# wilcox.test(group_UP, group_DOWN)
# wilcox.test(group_UP, group_onlyDOWN)
# wilcox.test(group_UP, group_NB)
# 
# wilcox.test(group_UNBIASED, group_DOWN)
# wilcox.test(group_UNBIASED, group_onlyDOWN)
# wilcox.test(group_UNBIASED, group_NB)
# 
# wilcox.test(group_DOWN, group_onlyDOWN)
# wilcox.test(group_DOWN, group_NB)
# 
# wilcox.test(group_onlyDOWN, group_NB)

# R_val_TAD_UP <- R_val %>% dplyr::filter(direction == "TAD_UP")
# R_val_TAD_DOWN <- R_val %>% dplyr::filter(direction == "TAD_DOWN")
# R_val_TAD_UNBIASED <- R_val %>% dplyr::filter(direction == "TAD_UNBIASED")
# R_val_TAD_NB <- R_val %>% dplyr::filter(direction == "TAD_NB")
# 
# hist(R_val_TAD_UP$ratioExprPerTad)
# boxplot(R_val_TAD_UP$ratioExprPerTad)
# summary(R_val_TAD_UP$ratioExprPerTad)
# 
# hist(R_val_TAD_DOWN$ratioExprPerTad)
# boxplot(R_val_TAD_DOWN$ratioExprPerTad)
# summary(R_val_TAD_DOWN$ratioExprPerTad)
# 
# hist(R_val_TAD_UNBIASED$ratioExprPerTad)
# boxplot(R_val_TAD_UNBIASED$ratioExprPerTad)
# summary(R_val_TAD_UNBIASED$ratioExprPerTad)
# 
# hist(R_val_TAD_NB$ratioExprPerTad)
# boxplot(R_val_TAD_NB$ratioExprPerTad)
# summary(R_val_TAD_NB$ratioExprPerTad)

# data_R_val <- rbind(R_val, R_val_compA, R_val_compB, R_val_compNA)
# data_R_val$Type <- factor(data_R_val$Type, levels = c("allCOMPs", "A", "B", "NA"))
# 
# distrib_title <- paste("Compartments", "(Resolution", "=", paste0(res, ")\nDistribution of TAD", "ratio"))
# distribR <- data_R_val %>%
#   ggplot( aes(x=TAD_R, fill=Type)) +
#   geom_histogram(binwidth=0.1, color="#e9ecef", alpha=0.8, position = 'dodge') +
#   scale_fill_manual(values=c("#69b3a2", "#404080", "red", "yellow")) +
#   theme_minimal() +
#   labs(fill="") +
#   ggtitle(distrib_title) +
#   scale_x_continuous(breaks = seq(0, 1, 0.1), labels = seq(0, 1, 0.1))
# 
# print(distribR)






##### On my laptop
library(knitr)

source("scripts/load_reddy.R")

#####
makeGRangesFromCompartments <- function(filepath, res, chr) {
  # Make df
  values <- read_csv(filepath, col_names = FALSE, col_types = cols()) %>% pull(X1)
  nb_values <- length(values)
  seqnames <- rep(chr, nb_values)
  start <- seq(from = 1, by = res, length.out = nb_values)
  end <- seq(from = res, by = res, length.out = nb_values)
  df <- data.frame(seqnames, start, end, eigenvalue = values) %>%
    mutate(compartment = ifelse(eigenvalue > 0, "X", "Y"))
  df$compartment[is.na(df$compartment)] <- "NA"
  
  # Merge consecutive compartments
  compt_rle <- rle(df$compartment)
  df_GR <- list()
  init <- 0
  for (cpt in 1:length(compt_rle$lengths)) {
    len <- compt_rle$lengths[cpt]
    init <- init + 1
    end <- init + len - 1
    
    tmp_GR <- df[init:end, ]
    df_GR[[cpt]] <- tmp_GR
    
    init <- init + len - 1
  }
  
  # Convert to GRanges
  GR <- lapply(df_GR, GRanges)
  GR_reduced <- lapply(GR, GenomicRanges::reduce)
  GR_comp <- unlist(GRangesList(GR_reduced))
  GR_comp$compartment <- compt_rle$values
  
  return(GR_comp)
}

#####
load_compartmentsXY <- function(timepoint, resolution, method_norm) {
  COMP_dir <- "output/hic/compartmentsAB"
  chroms <- paste0("chr", c(1:22, "X", "Y"))
  
  comps <- GRanges()
  for (chr in chroms) {
    COMP_filename <- paste("compartmentsAB", timepoint, "allReps", resolution, method_norm, chr, sep = "_")
    filepath <- file.path(COMP_dir, COMP_filename)
    if (file.exists(filepath)) {
      comp_tmp <- makeGRangesFromCompartments(filepath, as.numeric(resolution), chr = chr)
      message(" > Loading ", chr, "... ", length(comp_tmp), " compartments")
      # compXY_tmp <- ovHist(comp_tmp, chr, dhs = dhss_1h, ths = thss_1h, h3k27ac = h3k27ac_1h, h3k9me3 = h3k9me3_1h)
      comps <- c(comps, comp_tmp)
    }
  }
  missing_chr <- chroms[!(chroms %in% as.character(unique(seqnames(comps))))]
  message("Compartments have been loaded :")
  message("\t- resolution : ", resolution, " bp")
  message("\t- timepoint : ", timepoint)
  message("\t- normalisation method : ", method_norm)
  message("\t\t> Total : ", length(comps), " compartments")
  message("\t\t> Missing chromosome : ", ifelse(length(missing_chr) != 0, missing_chr, "none"))
  return(comps)
}

# Load compartments
comp500k <- load_compartmentsXY("1h", resolution = "500000", method_norm = "VC_SQRT")
summary(width(comp500k))
comp500k[comp500k$compartment == "NA"]
counts_comp500k <- comp500k %>% as.data.frame %>% group_by(seqnames, compartment) %>% tally
kable(counts_comp500k)

comp1000k <- load_compartmentsXY("1h", resolution = "1000000", method_norm = "VC_SQRT")
summary(width(comp1000k))
comp1000k[comp1000k$compartment == "NA"]
counts_comp1000k <- comp1000k %>% as.data.frame %>% group_by(seqnames, compartment) %>% tally
kable(counts_comp1000k)

comp2500k <- load_compartmentsXY("1h", resolution = "2500000", method_norm = "VC_SQRT")
summary(width(comp2500k))
comp2500k[comp2500k$compartment == "NA"]
counts_comp2500k <- comp2500k %>% as.data.frame %>% group_by(seqnames, compartment) %>% tally
kable(counts_comp2500k)

COMPs_list <- GRangesList(comp500k, comp1000k, comp2500k)

# Load annotation database
raw_promoters_A549 <- promoters(gencode31_basic_chr_TxDb, upstream = 3000, downstream = 3000, columns = "gene_id")
stdchr_promoters_A549 <- keepStdChr(raw_promoters_A549)
ensg_without_version <- str_replace(unlist(stdchr_promoters_A549$gene_id), "\\.[0-9]+$", "")
stdchr_promoters_A549$gene_id <- ensg_without_version
names(stdchr_promoters_A549) <- stdchr_promoters_A549$gene_id

# Overlaps betwenn compartments and genes promoters
get_overlapping_features <- function(anchor, feature, prefix) {
  if (prefix != FALSE) {
    names(feature) <- paste(prefix, 1:length(feature), sep = "_")
  }
  ov <- findOverlaps(anchor, feature)
  query <- queryHits(ov)
  subject <- subjectHits(ov)
  res <- vector("list", length(anchor))
  
  for (i in 1:length(ov)) {
    res[[query[i]]] <- c(res[[query[i]]], names(feature)[subject[i]])
  }
  return(res)
}

#####
get_expression_perCOMP <- function(genesPerComp, gene_expr) {
  tmp <- gene_expr[gene_expr$gene_id %in% genesPerComp, ]
  expr_sum <- colSums(as.matrix(tmp$expr))
  # message(expr_sum)
  return(expr_sum)
}

#####
get_expression <- function(genesPerComp_list, gene_expr) {
  nbComp <- length(genesPerComp_list)
  exprsComp <- vector("numeric", nbComp)
  for (i in 1:nbComp) {
    genesPerComp <- genesPerComp_list[[i]]
    expr_sum <- get_expression_perCOMP(genesPerComp, gene_expr)
    exprsComp[i] <- expr_sum
  }
  return(exprsComp)
}

# Load gene expression at 1 hour
rnaseq_raw <- read_tsv("results/a549_dex_time_points/raw_counts_with_colnames.txt") %>% 
  dplyr::select(gene_id, contains("1h"))
expression_1h <- rowMeans(as.matrix(rnaseq_raw[, -1]))
rnaseq1h <- data.frame(gene_id = rnaseq_raw$gene_id, expr = expression_1h)

# make Heatmap and boxplot
library(ComplexHeatmap)
library(circlize)
library(viridis)
library(gridExtra)

today <- get_today()

output_dir <- "output/analyses/ecosystem/assign_compartmentsAB"
res_list <- c("500 kb", "1000 kb", "2500 kb")
chroms <- paste0("chr", c(1:22, "X", "Y"))

allCOMPs <- list()

for (i in 1:length(COMPs_list)) {
  COMPs <- COMPs_list[[i]]
  res <- res_list[i]
  
  genesPerComp <- get_overlapping_features(COMPs, stdchr_promoters_A549, prefix = FALSE)
  sumExprPerComp <- get_expression(genesPerComp, rnaseq1h)
  
  CompWithGenesExpr <- tibble(comps = as_tibble(COMPs), genes = genesPerComp, sum_expr = sumExprPerComp, widthComp = width(COMPs))
  CompWithGenesExpr$comps$compartment <- factor(CompWithGenesExpr$comps$compartment, levels = c("X", "Y", "NA"))
  
  COMPs_AB <- GRanges()
  
  for (chr in chroms) {
    if (res == "2500 kb" & chr == "chrY") {next}
    message("##### ", chr)
    CompPerChr <- CompWithGenesExpr %>% dplyr::filter(comps$seqnames == chr) %>%
      mutate(matNExpr = sum_expr/widthComp*10000) %>% 
      arrange(comps$compartment, desc(matNExpr))
    # CompPerChr$matNExpr[CompPerChr$matNExpr == 0] <- NA
    matSumExpr <- CompPerChr$sum_expr %>% as.matrix
    matNExpr <- CompPerChr$matNExpr %>% as.matrix
    compType <- CompPerChr$comps$compartment
    
    nbX <- sum(compType == "X")
    nbY <- sum(compType == "Y")
    nbNA <- sum(compType == "NA")
    
    # annotation left
    annot_left <- rowAnnotation("Type" = compType,
                                col = list(Type = c("X" = "#2B70AB", "Y" = "#CD3301", "NA" = "#FFB027")))
    
    # color scale
    max_val <- max(matNExpr, na.rm = TRUE)
    mid_val <- median(matNExpr, na.rm = TRUE)
    customColors = colorRamp2(c(0, mid_val, max_val), c("#1E8449", "white", "#800020"))
    # customColors = colorRamp2(c(0, max_val), c("white", "#800020"))
    
    hm_comp <- Heatmap(matNExpr, name = "ExprIndex1h", col = customColors,
                       left_annotation = annot_left,
                       cluster_rows = FALSE,
                       column_title = paste("Resolution", "=", res,
                                            "\n", chr, "|", nrow(CompPerChr), "compartments",
                                            "\n", "(", "X :", nbX, "|", "Y :", nbY, "|", "NA :", nbNA, ")"),
                       column_title_gp = gpar(fontsize = 10),
                       rect_gp = gpar(col = "black", lwd = 0.1))
    
    # cell_fun = function(j, i, x, y, width, height, fill) {
    #   grid.text(sprintf("%.2f", matNExpr[i, j]), x, y, gp = gpar(fontsize = 10))
    # }
    
    ## make boxplot
    boxplot_df <- data.frame(type = compType, value = matNExpr)
    
    boxplot_comp <- ggplot(boxplot_df, aes(x=type, y=value, fill=type)) +
      geom_boxplot() +
      scale_fill_manual(values = c("#2B70AB", "#CD3301", "#FFB027")) +
      theme_minimal() +
      theme(legend.position="none") +
      xlab("Type") + ylab("ExprIndex1h") +
      geom_jitter(color = "black", size = 0.4, width = 0.25, alpha = 0.5)
    
    # T-test
    groupX <- boxplot_df %>% dplyr::filter(type == "X") %>% pull(value)
    groupY <- boxplot_df %>% dplyr::filter(type == "Y") %>% pull(value)
    ttest <- t.test(groupX, groupY)
    pval <- ttest$p.value
    
    COMPs_chr <- COMPs[seqnames(COMPs) == chr]
    compXY <- COMPs_chr$compartment
      
    meanX <- ttest$estimate[1]
    meanY <- ttest$estimate[2]
    if (meanX > meanY) {
      ccl <- "X = A | Y = B"
      compAB <- ifelse(compXY == "X",
                       "A",
                       ifelse(compXY == "Y",
                              "B",
                              "NA"))
      } else {
        ccl <- "X = B | Y = A"
        compAB <- ifelse(compXY == "X",
                         "B",
                         ifelse(compXY == "Y",
                                "A",
                                "NA"))
      }
    
    COMPs_chr$compAB <- compAB
    COMPs_AB <- c(COMPs_AB, COMPs_chr)
    
    if (pval > 0.05) {
      ns <- "n.s"
    } else {
      ns <- "*"
    }
    
    message("     # p-value = ", pval, " (", ns, ")")
    message("     > ", ccl)
    
    hm_comp_gb <- grid.grabExpr(draw(hm_comp))
    fig <- grid.arrange(hm_comp_gb, boxplot_comp,
                        ncol = 2, nrow = 1,
                        bottom = paste0("T-test > p-value = ", pval, " (", ns, ")\n", ccl))

    output_filename <- paste(today, "compXY_withExpr", gsub(" ", "", res), paste0(chr, ".pdf"), sep = "_")
    output_path <- file.path(output_dir, output_filename)
    ggsave(output_path, fig)
    
    # readline(prompt="Press [enter] to continue")
  }
  allCOMPs[[res]] <- COMPs_AB
}

saveRDS(allCOMPs, file = "output/hic/compartmentsAB/allCOMPs.rds")