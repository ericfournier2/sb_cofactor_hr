# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")
# setwd("/home/tavchr01/sb_cofactor_hr/A549")

# To run on genome.ulaval's servers

library(tidyverse)
source("scripts/ckn_utils.R")

# Identification of TADs at 1 hour
pwd <- "/Users/chris/Desktop/sb_cofactor_hr/A549"
hic_dir <- "input/ENCODE/A549/GRCh38/hic"
hic_file <- c("A549_1h_rep1_ENCFF525EFN.hic") #, "A549_1h_rep2_ENCFF690QRC.hic", "A549_1h_rep3_ENCFF452FWS.hic", "A549_1h_rep4_ENCFF977OQV.hic")
resolution <- c("5000") #, "10000", "25000", "50000")
method_norm <- "VC_SQRT"
juicer_tools <- "java -jar /Users/chris/Documents/software/juicer_tools_1.13.02.jar"
output_dir <- "output/hic/TADsByArrowhead"
# chroms <- paste0("chr", c(1:22, "X", "Y"))

for (res in resolution) {
  # call TADs
  hic_files <- file.path(pwd, hic_dir, hic_file)
  hic_path <- paste(hic_files, collapse = "+")
  output_file <- paste("TAD", "1h", res, method_norm, sep = "_")
  output_path <- file.path(pwd, output_dir, output_file)
  call <- paste(juicer_tools, "arrowhead",
                "-r", res,
                #"-c", "chr19",
                "--threads", 3,
                "-k", method_norm,
                hic_path,
                output_path)
  message(call)
  system(call)
}




for (chr in chroms) {
  for (res in resolution) {
    # Call TADs
    hic_path <- file.path(pwd, hic_dir, hic_file)
    output_file <- paste("TAD", "1h", "rep1", res, method_norm, chr, sep = "_")
    output_path <- file.path(pwd, output_dir, output_file)
    call <- paste(juicer_tools, "arrowhead",
                  "-c", chr,
                  "-r", res,
                  "--threads", 3,
                  "-k", method_norm,
                  hic_path,
                  output_path)
    message(call)
    system(call)
    
    # Rename files
    mv1 <- file.path(output_path, paste0(res, "_blocks.bedpe"))
    mv2 <- file.path(file.path(pwd, output_dir, paste(output_file, "bedpe", sep = ".")))
    call_mv <- paste("mv", mv1, mv2)
    message(call_mv)
    # system(call_mv)
    
    # Delete directory
    call_rm <- paste("rm", "-r", file.path(pwd, output_dir, output_file))
    message(call_rm)
    # system(call_rm)
  }
}

# 
# # open TAD
# openTAD <- function(filepath) {
#   raw <- read_tsv(filepath)[-1, ]
#   colnames(raw) <- c("chr1", "x1", "x2", "chr2", "y1", "y2", "name", "score", "strand1","strand2", 
#                      "color", "score_1", "uVarScore", "lVarScore", "upSign", "loSign")
#   tad_gr_raw <- raw[, 1:3]
#   colnames(tad_gr_raw) <- c("seqnames", "start", "end")
#   tad_gr <- GRanges(tad_gr_raw)
#   return(tad_gr)
# }
# 
# #
# tach_chr19_5k_path <- file.path(output_dir, "TAD_1h_rep1_5000_KR_chr19.bedpe") 
# tach_chr19_5k <- openTAD(tach_chr19_5k_path)
# length(tach_chr19_5k)
# summary(width(tach_chr19_5k))
# hist(width(tach_chr19_5k))
# 
# tach_chr19_10k_path <- file.path(output_dir, "TAD_1h_rep1_10000_KR_chr19.bedpe") 
# tach_chr19_10k <- openTAD(tach_chr19_10k_path)
# length(tach_chr19_10k)
# summary(width(tach_chr19_10k))
# hist(width(tach_chr19_10k))
# 
# tach_chr19_25k_path <- file.path(output_dir, "TAD_1h_rep1_25000_KR_chr19.bedpe") 
# tach_chr19_25k <- openTAD(tach_chr19_25k_path)
# length(tach_chr19_25k)
# summary(width(tach_chr19_25k))
# hist(width(tach_chr19_25k))
# 
# tach_chr19_50k_path <- file.path(output_dir, "TAD_1h_rep1_50000_KR_chr19.bedpe") 
# tach_chr19_50k <- openTAD(tach_chr19_50k_path)
# length(tach_chr19_50k)
# summary(width(tach_chr19_50k))
# hist(width(tach_chr19_50k))