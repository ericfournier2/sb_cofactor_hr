# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

library(ComplexHeatmap)
library(circlize)
library(viridis)
source("scripts/ckn_utils.R")
source("scripts/load_hic_data.R")

#####
countCDBSInTad <- function(tads, diffbind) {
  tad_id <- paste0(seqnames(tads), ":", start(tads), "-", end(tads))
  df <- data.frame("tad_id" = tad_id)
  
  for (set in names(diffbind)) {
    regions <- diffbind[[set]]
    countInTad <- countOverlaps(tads, regions)
    df <- cbind(df, countInTad)
  }
  colnames(df) <- c("tad_id", names(diffbind))
  return(df)
}

######
computeRatio <- function(vec_up, vec_down) {
  res <- rep("TMP", length(vec_up))
  for (i in 1:length(vec_up)) {
    val_up <- vec_up[i]
    val_down <- vec_down[i]
    if (val_up == 0 & val_down == 0) {
      res[i] = -1
    } else {
      res[i] = val_up/(val_up+val_down)
    }
  }
  return(as.numeric(res))
}

##### set direction
set_TAD_direction <- function(ratio, sumcof) {
  res <- rep("TMP", length(ratio))
  res[ratio == 1 & sumcof > 9] <- "TAD_onlyUP_10AndMoreCof"
  res[ratio == 1 & sumcof == 9] <- "TAD_onlyUP_9Cof"
  res[ratio == 1 & sumcof == 8] <- "TAD_onlyUP_8Cof"
  res[ratio == 1 & sumcof == 7] <- "TAD_onlyUP_7Cof"
  res[ratio == 1 & sumcof == 6] <- "TAD_onlyUP_6Cof"
  res[ratio == 1 & sumcof == 5] <- "TAD_onlyUP_5Cof"
  res[ratio == 1 & sumcof == 4] <- "TAD_onlyUP_4Cof"
  res[ratio == 1 & sumcof == 3] <- "TAD_onlyUP_3Cof"
  res[ratio == 1 & sumcof == 2] <- "TAD_onlyUP_2Cof"
  res[ratio == 1 & sumcof == 1] <- "TAD_onlyUP_1Cof"
  res[ratio >= 0.6 & ratio < 1] <- "TAD_UP"
  res[ratio <= 0.4 & ratio > 0] <- "TAD_DOWN"
  res[ratio == 0 & sumcof == 1] <- "TAD_onlyDOWN_1Cof"
  res[ratio == 0 & sumcof == 2] <- "TAD_onlyDOWN_2Cof"
  res[ratio == 0 & sumcof == 3] <- "TAD_onlyDOWN_3Cof"
  res[ratio == 0 & sumcof == 4] <- "TAD_onlyDOWN_4Cof"
  res[ratio == 0 & sumcof == 5] <- "TAD_onlyDOWN_5Cof"
  res[ratio == 0 & sumcof == 6] <- "TAD_onlyDOWN_6Cof"
  res[ratio == 0 & sumcof == 7] <- "TAD_onlyDOWN_7Cof"
  res[ratio == 0 & sumcof == 8] <- "TAD_onlyDOWN_8Cof"
  res[ratio == 0 & sumcof == 9] <- "TAD_onlyDOWN_9Cof"
  res[ratio == 0 & sumcof > 9] <- "TAD_onlyDOWN_10AndMoreCof"
  res[ratio == -1] <- "TAD_NB"
  res[ratio > 0.4 & ratio < 0.6] <- "TAD_UNBIASED"
  return(res)
}

set_TAD_direction2 <- function(ratio) {
  res <- rep("TMP", length(ratio))
  res[ratio == 1] <- "TAD_onlyUP"
  res[ratio >= 0.6 & ratio < 1] <- "TAD_UP"
  res[ratio <= 0.4 & ratio > 0] <- "TAD_DOWN"
  res[ratio == 0] <- "TAD_onlyDOWN"
  res[ratio == -1] <- "TAD_NB"
  res[ratio > 0.4 & ratio < 0.6] <- "TAD_UNBIASED"
  return(res)
}

##### Load TADs
TADs <- load_reddy_hic()[["1 hour"]]$TAD

##### Load diffbind
diffbind <- load_diffbind_cofactors_peaks()

##### Build MED1_BRD4_CDK9 and MED1_BRD4
MBC_UP <- GenomicRanges::reduce(c(diffbind[["MED1_UP"]], diffbind[["BRD4_UP"]], diffbind[["CDK9_UP"]])) # 8253
MBC_DOWN <- GenomicRanges::reduce(c(diffbind[["MED1_DOWN"]], diffbind[["BRD4_DOWN"]], diffbind[["CDK9_DOWN"]])) # 7492
MBC_UNBIASED <- GenomicRanges::reduce(c(diffbind[["MED1_UNBIASED"]], diffbind[["BRD4_UNBIASED"]], diffbind[["CDK9_UNBIASED"]])) # 22749

# Set of peaks to compare with
MBC_bis <- GRangesList("MBC_UP" = MBC_UP, "MBC_DOWN" = MBC_DOWN, "MBC_UNBIASED" = MBC_UNBIASED)

##### Analysis
n_tad <- length(TADs)
countTad <- countCDBSInTad(TADs, MBC_bis)

countTad_ratio <- countTad %>% mutate(MBC_R = computeRatio(MBC_UP, MBC_DOWN),
                                      MBC_S = MBC_UP + MBC_DOWN,
                                      width_TADs = width(TADs),
                                      direction = set_TAD_direction2(MBC_R))

# countTad_ratio$direction <- factor(countTad_ratio$direction,
#                                   levels = c("TAD_onlyDOWN_10AndMoreCof", "TAD_onlyDOWN_9Cof",
#                                              "TAD_onlyDOWN_8Cof", "TAD_onlyDOWN_7Cof",
#                                              "TAD_onlyDOWN_6Cof", "TAD_onlyDOWN_5Cof",
#                                              "TAD_onlyDOWN_4Cof", "TAD_onlyDOWN_3Cof",
#                                              "TAD_onlyDOWN_2Cof", "TAD_onlyDOWN_1Cof",
#                                              "TAD_DOWN",
#                                              "TAD_UNBIASED",
#                                              "TAD_UP",
#                                              "TAD_onlyUP_1Cof", "TAD_onlyUP_2Cof",
#                                              "TAD_onlyUP_3Cof", "TAD_onlyUP_4Cof",
#                                              "TAD_onlyUP_5Cof", "TAD_onlyUP_6Cof",
#                                              "TAD_onlyUP_7Cof", "TAD_onlyUP_8Cof",
#                                              "TAD_onlyUP_9Cof", "TAD_onlyUP_10AndMoreCof",
#                                              "TAD_NB"))

countTad_ratio$direction <- factor(countTad_ratio$direction,
                                   levels = c("TAD_onlyDOWN",
                                              "TAD_DOWN",
                                              "TAD_UNBIASED",
                                              "TAD_UP",
                                              "TAD_onlyUP",
                                              "TAD_NB"))

table(countTad_ratio$direction)

##### Load DETads dataframe
deTADs <- read_tsv("output/analyses/ecosystem/countReads_inTADs/DETADs.txt")
colnames(deTADs) <- c("tad_id", "baseMean", "log2FoldChange", "lfcSE", "stat", "pvalue", "padj")

##### Merge countTad_ratio and deTADs
TADs_infos <- left_join(countTad_ratio, deTADs, by = "tad_id")


# Ditribution of log2FoldChange according to TAD status
R_val <- TADs_infos %>% dplyr::select(direction, log2FoldChange, padj) %>% 
  dplyr::mutate(significant = ifelse(padj < 0.1, "significant", "none")) %>% dplyr::select(-padj)

R_val %>% dplyr::filter(significant == "significant") %>% dplyr::select(direction) %>% table

R_val %>% ggplot(aes(x = direction, y = log2FoldChange, fill = direction)) +
  geom_boxplot(outlier.shape = NA) +
  theme_minimal() +
  geom_jitter(color = "black", size = 0.4, width = 0.25, alpha = 0.5) +
  scale_color_viridis(discrete=TRUE)
  # ylab("TranscriptionIndex") +
  # theme(axis.text.x=element_blank())

R_val %>% ggplot(aes(x = direction, y = log2FoldChange, fill = direction)) +
  geom_boxplot(outlier.shape = NA) +
  theme_minimal() +
  geom_jitter(aes(color = significant), size = 0.4, width = 0.25, alpha = 0.5) +
  scale_color_viridis(discrete=TRUE) +
  ylab("TranscriptionIndex") +
  # theme(axis.text.x=element_blank()) +
  scale_color_manual(values = c("grey", "red"))