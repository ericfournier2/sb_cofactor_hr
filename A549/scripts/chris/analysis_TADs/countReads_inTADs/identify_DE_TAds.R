# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

library(tidyverse)
library(DESeq2)

# Load raw counts in TADs
raw_counts <- read_tsv("output/analyses/ecosystem/countReads_inTADs/nbReads_inTADs.txt")

# Make design
sample_names <- colnames(raw_counts)[-1]
time_point <- str_split(sample_names, pattern = "_") %>% purrr::map(1) %>% unlist
design <- data.frame(sample_names, time_point)

# Make matrix
m <- dplyr::select(raw_counts, -Coordinates)  %>% as.matrix
rownames(m) <- raw_counts$Coordinates

# DE_TADs
dds <- DESeqDataSetFromMatrix(m, design, ~ time_point) # 6778 TADs
dds <- dds[ rowSums(counts(dds)) > 1, ] # 6772 TADs
dds <- DESeq(dds)
res <- results(dds)
DESeq2::plotMA(res)

res_df <- as.data.frame(res) %>% mutate(Coordinates = rownames(res)) %>% dplyr::select(Coordinates, everything())

# output_dir <- "output/analyses/ecosystem/countReads_inTADs"
# filename <- paste("DETADs.txt", sep = "_")
# write.table(res_df, file = file.path(output_dir, filename),
#             quote = FALSE, row.names = FALSE, sep = "\t")

#### Analysis
significantTAD <- res_df %>% dplyr::filter(padj <= 0.05)
# > 171 significant TADs
upTADs <- significantTAD %>% dplyr::filter(log2FoldChange >= 0.5) %>% arrange(desc(log2FoldChange))
# FC = 1 : 17 upTADs | FC = 0.5 : 47 upTADs
downTADs <- significantTAD %>% dplyr::filter(log2FoldChange <= -0.5) %>% arrange(log2FoldChange)
# # FC = 1 : 38 downTADs | FC = 0.5 : 95 downTADs