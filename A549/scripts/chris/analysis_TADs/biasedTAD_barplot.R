# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

library(ComplexHeatmap)
library(circlize)
source("scripts/ckn_utils.R")
source("scripts/load_hic_data.R")

#####
countCDBSInTad <- function(tads, diffbind) {
  tad_id <- paste(seqnames(tads), start(tads), end(tads), sep = "_")
  df <- data.frame("tad_id" = tad_id)
  
  for (set in names(diffbind)) {
    regions <- diffbind[[set]]
    countInTad <- countOverlaps(tads, regions)
    df <- cbind(df, countInTad)
  }
  colnames(df) <- c("tad_id", names(diffbind))
  return(df)
}

######
computeRatio <- function(vec_up, vec_down) {
  res <- rep("TMP", length(vec_up))
  for (i in 1:length(vec_up)) {
    val_up <- vec_up[i]
    val_down <- vec_down[i]
    if (val_up == 0 & val_down == 0) {
      res[i] = -1
    } else {
      res[i] = val_up/(val_up+val_down)
    }
  }
  return(as.numeric(res))
}

##### Load TADs
TADs <- load_reddy_hic()[["1 hour"]]$TAD

##### Load diffbind
diffbind <- load_diffbind_cofactors_peaks()

##### Build MED1_BRD4_CDK9 and MED1_BRD4
MBC_UP <- GenomicRanges::reduce(c(diffbind[["MED1_UP"]], diffbind[["BRD4_UP"]], diffbind[["CDK9_UP"]])) # 8253
MBC_DOWN <- GenomicRanges::reduce(c(diffbind[["MED1_DOWN"]], diffbind[["BRD4_DOWN"]], diffbind[["CDK9_DOWN"]])) # 7492
MBC_UNBIASED <- GenomicRanges::reduce(c(diffbind[["MED1_UNBIASED"]], diffbind[["BRD4_UNBIASED"]], diffbind[["CDK9_UNBIASED"]])) # 22749

# Set of peaks to compare with
MBC_bis <- GRangesList("MBC_UP" = MBC_UP, "MBC_DOWN" = MBC_DOWN, "MBC_UNBIASED" = MBC_UNBIASED)

##### Analysis
n_tad <- length(TADs)
countTad <- countCDBSInTad(TADs, MBC_bis)

countTad_ratio <- countTad %>% mutate(MBC_R = computeRatio(MBC_UP, MBC_DOWN),
                                      MBC_S = MBC_UP + MBC_DOWN,
                                      width_TADs = width(TADs))

nbTAD_withMBC <- countTad_ratio %>% dplyr::filter(MBC_R != -1) %>% nrow
nbTAD_nobinding <- countTad_ratio %>% dplyr::filter(MBC_R == -1) %>% nrow
nb_TADonlyup <- countTad_ratio  %>% dplyr::filter(MBC_R == 1) %>% nrow
nb_TADonlydown <- countTad_ratio  %>% dplyr::filter(MBC_R == 0) %>% nrow
nb_TADmix <- countTad_ratio  %>% dplyr::filter(MBC_R > 0 & MBC_R < 1) %>% nrow
## category:
# TAD with only gain or loss of cofactors
# TAD with gain and loss of cofactors
## subcategory
# only gain
# only loss
# gainANDloss
category <- c("biased TADs",
              "biased TADs",
              "TADs with gain and loss of cofactors")
subcategory <- c("only gain", "only loss", "gain and loss")
value <- c(nb_TADonlyup, nb_TADonlydown, nb_TADmix)

df <- data.frame(category, subcategory, value)
df$category <- factor(df$category, levels = c("biased TADs", "TADs with gain and loss of cofactors"))
barplotTAD <- ggplot(df, aes(fill = subcategory, y = value, x = category)) +
  geom_bar(position = "stack", stat = "identity", width = 0.75) +
  scale_fill_viridis(discrete = T) +
  theme_minimal() +
  ylab("Number of TADs") +
  theme(axis.title.x = element_blank(),
        axis.title.y = element_text(size=14, face="bold"),
        axis.text = element_text(size = 12))
  
barplotTAD