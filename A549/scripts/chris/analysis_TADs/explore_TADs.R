# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

library(plyr)
source("scripts/chris/detect_TADs/load_TADs.R")

TADs_5kb <- load_TADs(timepoint = "1h", resolution = "5000", method_norm = "VC_SQRT")
length(TADs_5kb)
summary(width(TADs_5kb))
perchr_5kb <- as.data.frame(TADs_5kb) %>% group_by(seqnames) %>% tally

TADs_10kb <- load_TADs(timepoint = "1h", resolution = "10000", method_norm = "VC_SQRT")
length(TADs_10kb)
summary(width(TADs_10kb))
perchr_10kb <- as.data.frame(TADs_10kb) %>% group_by(seqnames) %>% tally

TADs_25kb <- load_TADs(timepoint = "1h", resolution = "25000", method_norm = "VC_SQRT")
length(TADs_25kb)
summary(width(TADs_25kb))
perchr_25kb <- as.data.frame(TADs_25kb) %>% group_by(seqnames) %>% tally

TADs_50kb <- load_TADs(timepoint = "1h", resolution = "50000", method_norm = "VC_SQRT")
length(TADs_50kb)
summary(width(TADs_50kb))
perchr_50kb <- as.data.frame(TADs_50kb) %>% group_by(seqnames) %>% tally

nb_tads_per_chr <- join_all(list(perchr_50kb, perchr_25kb, perchr_10kb, perchr_5kb),
                            by = "seqnames", match = "all", type = "left")
colnames(nb_tads_per_chr) <- c("chromosome", "50kb", "25kb", "10kb", "5kb")
knitr::kable(nb_tads_per_chr)
