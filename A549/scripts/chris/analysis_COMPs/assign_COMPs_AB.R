# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

##### On my laptop
library(tidyverse)
library(knitr)
source("scripts/ckn_utils.R")
source("scripts/load_reddy.R")

#####
makeGRangesFromCompartments <- function(filepath, res, chr) {
  # Make df
  values <- read_csv(filepath, col_names = FALSE, col_types = cols()) %>% pull(X1)
  nb_values <- length(values)
  seqnames <- rep(chr, nb_values)
  start <- seq(from = 1, by = res, length.out = nb_values)
  end <- seq(from = res, by = res, length.out = nb_values)
  df <- data.frame(seqnames, start, end, eigenvalue = values) %>%
    mutate(compartment = ifelse(eigenvalue > 0, "X", "Y"))
  df$compartment[is.na(df$compartment)] <- "NA"
  
  # Merge consecutive compartments
  compt_rle <- rle(df$compartment)
  df_GR <- list()
  init <- 0
  for (cpt in 1:length(compt_rle$lengths)) {
    len <- compt_rle$lengths[cpt]
    init <- init + 1
    end <- init + len - 1
    
    tmp_GR <- df[init:end, ]
    df_GR[[cpt]] <- tmp_GR
    
    init <- init + len - 1
  }
  
  # Convert to GRanges
  GR <- lapply(df_GR, GRanges)
  GR_reduced <- lapply(GR, GenomicRanges::reduce)
  GR_comp <- unlist(GRangesList(GR_reduced))
  GR_comp$compartment <- compt_rle$values
  
  return(GR_comp)
}

#####
load_compartmentsXY <- function(timepoint, resolution, method_norm) {
  COMP_dir <- "output/hic/compartmentsAB"
  chroms <- paste0("chr", c(1:22, "X", "Y"))
  
  comps <- GRanges()
  for (chr in chroms) {
    COMP_filename <- paste("compartmentsAB", timepoint, "allReps", resolution, method_norm, chr, sep = "_")
    filepath <- file.path(COMP_dir, COMP_filename)
    if (file.exists(filepath)) {
      comp_tmp <- makeGRangesFromCompartments(filepath, as.numeric(resolution), chr = chr)
      message(" > Loading ", chr, "... ", length(comp_tmp), " compartments")
      # compXY_tmp <- ovHist(comp_tmp, chr, dhs = dhss_1h, ths = thss_1h, h3k27ac = h3k27ac_1h, h3k9me3 = h3k9me3_1h)
      comps <- c(comps, comp_tmp)
    }
  }
  missing_chr <- chroms[!(chroms %in% as.character(unique(seqnames(comps))))]
  message("Compartments have been loaded :")
  message("\t- resolution : ", resolution, " bp")
  message("\t- timepoint : ", timepoint)
  message("\t- normalisation method : ", method_norm)
  message("\t\t> Total : ", length(comps), " compartments")
  message("\t\t> Missing chromosome : ", ifelse(length(missing_chr) != 0, missing_chr, "none"))
  return(comps)
}

# Load compartments
comp500k <- load_compartmentsXY("1h", resolution = "500000", method_norm = "VC_SQRT")
summary(width(comp500k))
comp500k[comp500k$compartment == "NA"]
counts_comp500k <- comp500k %>% as.data.frame %>% group_by(seqnames, compartment) %>% tally
kable(counts_comp500k)

comp1000k <- load_compartmentsXY("1h", resolution = "1000000", method_norm = "VC_SQRT")
summary(width(comp1000k))
comp1000k[comp1000k$compartment == "NA"]
counts_comp1000k <- comp1000k %>% as.data.frame %>% group_by(seqnames, compartment) %>% tally
kable(counts_comp1000k)

comp2500k <- load_compartmentsXY("1h", resolution = "2500000", method_norm = "VC_SQRT")
summary(width(comp2500k))
comp2500k[comp2500k$compartment == "NA"]
counts_comp2500k <- comp2500k %>% as.data.frame %>% group_by(seqnames, compartment) %>% tally
kable(counts_comp2500k)

COMPs_list <- GRangesList(comp500k, comp1000k, comp2500k)

# Load annotation database
raw_promoters_A549 <- promoters(gencode31_basic_chr_TxDb, upstream = 3000, downstream = 3000, columns = "gene_id")
stdchr_promoters_A549 <- keepStdChr(raw_promoters_A549)
ensg_without_version <- str_replace(unlist(stdchr_promoters_A549$gene_id), "\\.[0-9]+$", "")
stdchr_promoters_A549$gene_id <- ensg_without_version
names(stdchr_promoters_A549) <- stdchr_promoters_A549$gene_id

# Overlaps betwenn compartments and genes promoters
get_overlapping_features <- function(anchor, feature, prefix) {
  if (prefix != FALSE) {
    names(feature) <- paste(prefix, 1:length(feature), sep = "_")
  }
  ov <- findOverlaps(anchor, feature)
  query <- queryHits(ov)
  subject <- subjectHits(ov)
  res <- vector("list", length(anchor))
  
  for (i in 1:length(ov)) {
    res[[query[i]]] <- c(res[[query[i]]], names(feature)[subject[i]])
  }
  return(res)
}

#####
get_expression_perCOMP <- function(genesPerComp, gene_expr) {
  tmp <- gene_expr[gene_expr$gene_id %in% genesPerComp, ]
  expr_sum <- colSums(as.matrix(tmp$expr))
  # message(expr_sum)
  return(expr_sum)
}

#####
get_expression <- function(genesPerComp_list, gene_expr) {
  nbComp <- length(genesPerComp_list)
  exprsComp <- vector("numeric", nbComp)
  for (i in 1:nbComp) {
    genesPerComp <- genesPerComp_list[[i]]
    expr_sum <- get_expression_perCOMP(genesPerComp, gene_expr)
    exprsComp[i] <- expr_sum
  }
  return(exprsComp)
}

# Load gene expression at 1 hour
rnaseq_raw <- read_tsv("results/a549_dex_time_points/raw_counts_with_colnames.txt") %>% 
  dplyr::select(gene_id, contains("1h"))
expression_1h <- rowMeans(as.matrix(rnaseq_raw[, -1]))
rnaseq1h <- data.frame(gene_id = rnaseq_raw$gene_id, expr = expression_1h)

# make Heatmap and boxplot
library(ComplexHeatmap)
library(circlize)
library(viridis)
library(gridExtra)

#####
today <- get_today()

output_dir <- "output/analyses/ecosystem/assign_compartmentsAB"
res_list <- c("500 kb", "1000 kb", "2500 kb")
chroms <- paste0("chr", c(1:22, "X", "Y"))

allCOMPs <- list()

for (i in 1:length(COMPs_list)) {
  COMPs <- COMPs_list[[i]]
  res <- res_list[i]
  
  genesPerComp <- get_overlapping_features(COMPs, stdchr_promoters_A549, prefix = FALSE)
  sumExprPerComp <- get_expression(genesPerComp, rnaseq1h)
  
  CompWithGenesExpr <- tibble(comps = as_tibble(COMPs), genes = genesPerComp, sum_expr = sumExprPerComp, widthComp = width(COMPs))
  CompWithGenesExpr$comps$compartment <- factor(CompWithGenesExpr$comps$compartment, levels = c("X", "Y", "NA"))
  
  COMPs_AB <- GRanges()
  
  for (chr in chroms) {
    if (res == "2500 kb" & chr == "chrY") {next}
    message("##### ", chr)
    CompPerChr <- CompWithGenesExpr %>% dplyr::filter(comps$seqnames == chr) %>%
      mutate(matNExpr = sum_expr/widthComp*10000) %>% 
      arrange(comps$compartment, desc(matNExpr))
    # CompPerChr$matNExpr[CompPerChr$matNExpr == 0] <- NA
    matSumExpr <- CompPerChr$sum_expr %>% as.matrix
    matNExpr <- CompPerChr$matNExpr %>% as.matrix
    compType <- CompPerChr$comps$compartment
    
    nbX <- sum(compType == "X")
    nbY <- sum(compType == "Y")
    nbNA <- sum(compType == "NA")
    
    # annotation left
    annot_left <- rowAnnotation("Type" = compType,
                                col = list(Type = c("X" = "#2B70AB", "Y" = "#CD3301", "NA" = "#FFB027")))
    
    # color scale
    max_val <- max(matNExpr, na.rm = TRUE)
    mid_val <- median(matNExpr, na.rm = TRUE)
    customColors = colorRamp2(c(0, mid_val, max_val), c("#1E8449", "white", "#800020"))
    # customColors = colorRamp2(c(0, max_val), c("white", "#800020"))
    
    hm_comp <- Heatmap(matNExpr, name = "ExprIndex1h", col = customColors,
                       left_annotation = annot_left,
                       cluster_rows = FALSE,
                       column_title = paste("Resolution", "=", res,
                                            "\n", chr, "|", nrow(CompPerChr), "compartments",
                                            "\n", "(", "X :", nbX, "|", "Y :", nbY, "|", "NA :", nbNA, ")"),
                       column_title_gp = gpar(fontsize = 10),
                       rect_gp = gpar(col = "black", lwd = 0.1))
    
    # cell_fun = function(j, i, x, y, width, height, fill) {
    #   grid.text(sprintf("%.2f", matNExpr[i, j]), x, y, gp = gpar(fontsize = 10))
    # }
    
    ## make boxplot
    boxplot_df <- data.frame(type = compType, value = matNExpr)
    
    boxplot_comp <- ggplot(boxplot_df, aes(x=type, y=value, fill=type)) +
      geom_boxplot() +
      scale_fill_manual(values = c("#2B70AB", "#CD3301", "#FFB027")) +
      theme_minimal() +
      theme(legend.position="none") +
      xlab("Type") + ylab("ExprIndex1h") +
      geom_jitter(color = "black", size = 0.4, width = 0.25, alpha = 0.5)
    
    # T-test
    groupX <- boxplot_df %>% dplyr::filter(type == "X") %>% pull(value)
    groupY <- boxplot_df %>% dplyr::filter(type == "Y") %>% pull(value)
    ttest <- t.test(groupX, groupY)
    pval <- ttest$p.value
    
    COMPs_chr <- COMPs[seqnames(COMPs) == chr]
    compXY <- COMPs_chr$compartment
      
    meanX <- ttest$estimate[1]
    meanY <- ttest$estimate[2]
    if (meanX > meanY) {
      ccl <- "X = A | Y = B"
      compAB <- ifelse(compXY == "X",
                       "A",
                       ifelse(compXY == "Y",
                              "B",
                              "NA"))
      } else {
        ccl <- "X = B | Y = A"
        compAB <- ifelse(compXY == "X",
                         "B",
                         ifelse(compXY == "Y",
                                "A",
                                "NA"))
      }
    
    COMPs_chr$compAB <- compAB
    COMPs_AB <- c(COMPs_AB, COMPs_chr)
    
    if (pval > 0.05) {
      ns <- "n.s"
    } else {
      ns <- "*"
    }
    
    message("     # p-value = ", pval, " (", ns, ")")
    message("     > ", ccl)
    
    hm_comp_gb <- grid.grabExpr(draw(hm_comp))
    fig <- grid.arrange(hm_comp_gb, boxplot_comp,
                        ncol = 2, nrow = 1,
                        bottom = paste0("T-test > p-value = ", pval, " (", ns, ")\n", ccl))

    output_filename <- paste(today, "compXY_withExpr", gsub(" ", "", res), paste0(chr, ".pdf"), sep = "_")
    output_path <- file.path(output_dir, output_filename)
    # ggsave(output_path, fig)
    
    # readline(prompt="Press [enter] to continue")
  }
  allCOMPs[[res]] <- COMPs_AB
}

# saveRDS(allCOMPs, file = "output/hic/compartmentsAB/allCOMPs.rds")

# Generate BED file
output_dir <- "output/hic/compartmentsAB"
norm <- "VC_SQRT"
for (res in names(allCOMPs)) {
  COMPs <- allCOMPs[[res]]
  bed_filename <- paste("compartmentsAB", "1h", "allReps", norm, paste0(gsub(" ", "", res), ".bed"), sep = "_")
  output_path <- file.path(output_dir, bed_filename)
  COMPs_df <- as.data.frame(COMPs)
  colnames(COMPs_df) <- c("seqnames", "start", "end", "width", "strand", "compXY", "compAB")
  write.table(COMPs_df, file = output_path, quote = FALSE, row.names = FALSE, sep = "\t", col.names = FALSE)
}
