# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")
# setwd("/home/tavchr01/sb_cofactor_hr/A549")

#### On CHUL server
# Calling compartments A/B at 1 hour
pwd <- "/home/tavchr01/sb_cofactor_hr/A549"
resolution <- c("500000", "1000000", "2500000")
hic_dir <- "input/ENCODE/A549/GRCh38/hic"
hic_file <- c("A549_1h_rep1_ENCFF525EFN.hic", "A549_1h_rep2_ENCFF690QRC.hic", "A549_1h_rep3_ENCFF452FWS.hic", "A549_1h_rep4_ENCFF977OQV.hic")
method_norm <- "VC_SQRT"
juicer_tools <- "java -jar /home/tavchr01/software/juicertools/juicer_tools_1.13.02.jar"
output_dir <- "output/hic/compartmentsAB"
chroms <- paste0("chr", c(1:22, "X", "Y"))

for (chr in chroms) {
  for (res in resolution) {
    hic_files <- file.path(pwd, hic_dir, hic_file)
    hic_path <- paste(hic_files, collapse = "+")
    output_file <- paste("compartmentsAB", "1h", "allReps", res, method_norm, chr, sep = "_")
    output_path <- file.path(pwd, output_dir, output_file)
    call <- paste(juicer_tools, "eigenvector", method_norm, hic_path, chr, "BP", res, output_path)
    message(call)
    # system(call)
  }
}

##### On my laptop
library(tidyverse)
library(knitr)
source("scripts/ckn_utils.R")
source("scripts/load_reddy.R")

#####
makeGRangesFromCompartments <- function(filepath, res, chr) {
  # Make df
  values <- read_csv(filepath, col_names = FALSE, col_types = cols()) %>% pull(X1)
  nb_values <- length(values)
  seqnames <- rep(chr, nb_values)
  start <- seq(from = 1, by = res, length.out = nb_values)
  end <- seq(from = res, by = res, length.out = nb_values)
  df <- data.frame(seqnames, start, end, eigenvalue = values) %>%
    mutate(compartment = ifelse(eigenvalue > 0, "X", "Y"))
  df$compartment[is.na(df$compartment)] <- "NA"
  
  # Merge consecutive compartments
  compt_rle <- rle(df$compartment)
  df_GR <- list()
  init <- 0
  for (cpt in 1:length(compt_rle$lengths)) {
    len <- compt_rle$lengths[cpt]
    init <- init + 1
    end <- init + len - 1
    
    tmp_GR <- df[init:end, ]
    df_GR[[cpt]] <- tmp_GR
    
    init <- init + len - 1
  }
  
  # Convert to GRanges
  GR <- lapply(df_GR, GRanges)
  GR_reduced <- lapply(GR, GenomicRanges::reduce)
  GR_comp <- unlist(GRangesList(GR_reduced))
  GR_comp$compartment <- compt_rle$values
  
  return(GR_comp)
}

ovHist <- function(compXY, chr, dhs = dhss_1h, ths = thss_1h, h3k27ac = h3k27ac_1h, h3k9me3 = h3k9me3_1h,
                     h3k4me1 = h3k4me1_1h, h3k4me2 = h3k4me2_1h, h3k4me3 = h3k4me3_1h) {
  compXYlist <- list()
  compXYlist$compX <- compXY[compXY$compartment == "X"]
  compXYlist$compY <- compXY[compXY$compartment == "Y"]
  compXYlist$compNA <- compXY[compXY$compartment == "NA"]
  
  dhs_chr <- dhs[seqnames(dhs) == chr]
  ths_chr <- ths[seqnames(ths) == chr]
  h3k27ac_chr <- h3k27ac[seqnames(h3k27ac) == chr]
  h3k9me3_chr <- h3k9me3[seqnames(h3k9me3) == chr]
  h3k4me1_chr <- h3k4me1[seqnames(h3k4me1) == chr]
  h3k4me2_chr <- h3k4me2[seqnames(h3k4me2) == chr]
  h3k4me3_chr <- h3k4me3[seqnames(h3k4me3) == chr]
  
  df_ov <- data.frame("peaks" = c("DHS", "THS", "H3K27ac", "H3K9me3", "H3K4me1", "H3K4me2", "H3K4me3"))
  for (name in names(compXYlist)) {
    regions <- compXYlist[[name]]
    l_regions <- length(regions)
    
    ov_dhs <- sum(countOverlaps(regions, dhs_chr))
    ov_ths <- sum(countOverlaps(regions, ths_chr))
    ov_h3k27ac <- sum(countOverlaps(regions, h3k27ac_chr))
    ov_h3k9me3 <- sum(countOverlaps(regions, h3k9me3_chr))
    ov_h3k4me1 <- sum(countOverlaps(regions, h3k4me1_chr))
    ov_h3k4me2 <- sum(countOverlaps(regions, h3k4me2_chr))
    ov_h3k4me3 <- sum(countOverlaps(regions, h3k4me3_chr))
    
    c_comp <- c(ov_dhs, ov_ths, ov_h3k27ac, ov_h3k9me3, ov_h3k4me1, ov_h3k4me2, ov_h3k4me3)
    p_comp <- round(c_comp/c(length(dhs_chr), length(ths_chr), length(h3k27ac_chr), length(h3k9me3_chr),
                             length(h3k4me1_chr), length(h3k4me2_chr), length(h3k4me3_chr))*100, 2)
      
    df_ov <- cbind(df_ov, c_comp, p_comp)
  }
  colnames(df_ov) <- c("peaks", "countCompX", "percentCompX", "countCompY", "percentCompY", "countCompNA", "percentCompNA")
  print(kable(df_ov))
}

#####
load_compartmentsXY <- function(timepoint, resolution, method_norm) {
  COMP_dir <- "output/hic/compartmentsAB"
  chroms <- paste0("chr", c(1:22, "X", "Y"))
  
  comps <- GRanges()
  for (chr in chroms) {
    COMP_filename <- paste("compartmentsAB", timepoint, "allReps", resolution, method_norm, chr, sep = "_")
    filepath <- file.path(COMP_dir, COMP_filename)
    if (file.exists(filepath)) {
      comp_tmp <- makeGRangesFromCompartments(filepath, as.numeric(resolution), chr = chr)
      message(" > Loading ", chr, "... ", length(comp_tmp), " compartments")
      # compXY_tmp <- ovHist(comp_tmp, chr, dhs = dhss_1h, ths = thss_1h, h3k27ac = h3k27ac_1h, h3k9me3 = h3k9me3_1h)
      comps <- c(comps, comp_tmp)
    }
  }
  missing_chr <- chroms[!(chroms %in% as.character(unique(seqnames(comps))))]
  message("Compartments have been loaded :")
  message("\t- resolution : ", resolution, " bp")
  message("\t- timepoint : ", timepoint)
  message("\t- normalisation method : ", method_norm)
  message("\t\t> Total : ", length(comps), " compartments")
  message("\t\t> Missing chromosome : ", ifelse(length(missing_chr) != 0, missing_chr, "none"))
  return(comps)
}

###### Overlaps with DHSs, H3K27ac and H3K9me3
## load DHSs 
dhss <- load_reddy_dhss_consensus()
sapply(dhss, length)
dhss_1h <- dhss[["1 hour"]]
## load THSs
thss <- load_reddy_thss_consensus()
sapply(thss, length)
thss_1h <- thss[["1 hour"]]
## load H3K9me3 
h3k9me3 <- load_reddy_binding_consensus_broadPeak("H3K9me3")
sapply(h3k9me3, length)
h3k9me3_1h <- h3k9me3[["1 hour"]]
## load H3K27ac 
h3k27ac <- load_reddy_binding_consensus("H3K27ac")
sapply(h3k27ac, length)
h3k27ac_1h <- h3k27ac[["1 hour"]]
## load H3K4me1
h3k4me1 <- load_reddy_binding_consensus("H3K4me1")
sapply(h3k4me1, length)
h3k4me1_1h <- h3k4me1[["1 hour"]]
## load H3K4me2
h3k4me2 <- load_reddy_binding_consensus("H3K4me2")
sapply(h3k4me2, length)
h3k4me2_1h <- h3k4me2[["1 hour"]]
## load H3K4me3
h3k4me3 <- load_reddy_binding_consensus("H3K4me3")
sapply(h3k4me3, length)
h3k4me3_1h <- h3k4me3[["1 hour"]]

comp500k <- load_compartmentsXY("1h", resolution = "500000", method_norm = "VC_SQRT")
summary(width(comp500k))
comp500k[comp500k$compartment == "NA"]
counts_comp500k <- comp500k %>% as.data.frame %>% group_by(seqnames, compartment) %>% tally
kable(counts_comp500k)

comp1000k <- load_compartmentsXY("1h", resolution = "1000000", method_norm = "VC_SQRT")
summary(width(comp1000k))
comp1000k[comp1000k$compartment == "NA"]
counts_comp1000k <- comp1000k %>% as.data.frame %>% group_by(seqnames, compartment) %>% tally
kable(counts_comp1000k)

comp2500k <- load_compartmentsXY("1h", resolution = "2500000", method_norm = "VC_SQRT")
summary(width(comp2500k))
comp2500k[comp2500k$compartment == "NA"]
counts_comp2500k <- comp2500k %>% as.data.frame %>% group_by(seqnames, compartment) %>% tally
kable(counts_comp2500k)

# # makeBEDPE from GRanges
# makeBEDPEfromGRanges <- function(gr, output_dir, output_file) {
#   bedpe <- data.frame(as.data.frame(gr)[, 1:3], as.data.frame(gr)[, c(1:3, 6)])
#   colnames(bedpe) <- c("chr1", "x1", "x2", "chr2", "y1", "y2", "compartment")
#   output_path <- file.path(output_dir, output_file)
#   write.table(bedpe, file = output_path, quote = FALSE, row.names = FALSE, sep = "\t")
#   message("Saved in ", output_path)
# }
# 
# # split GRanges according to compartment A, compartiment B, compartiment NA
# makeCompartmentBEDPE <- function(gr, output_dir, res, method_norm, chr) {
#   grlist <- list()
#   grlist$compartmentX <- gr[gr$compartment == "X"]
#   grlist$compartmentY <- gr[gr$compartment == "Y"]
#   grlist$compartmentNA <- gr[gr$compartment == "NA"]
#   
#   for (name in names(grlist)) {
#     gr <- grlist[[name]]
#     output_file <- paste("compartments", "1h", "rep1", as.character(res), method_norm, chr, paste0(name, ".bedpe"), sep = "_")
#     makeBEDPEfromGRanges(gr, output_dir, output_file)
#   }
# }
# 

# chr19_list <- list("chr19_500k" = chr19_500k,
#                    "chr19_1000k" = chr19_1000k,
#                    "chr19_2500k" = chr19_2500k)

# for (set in names(chr19_list)) {
#   message("### ", set)
#   all_comp <- chr19_list[[set]]
#   print(kable(table(all_comp$compartment)))
#   
#   compX <- all_comp[all_comp$compartment == "X"]
#   compY <- all_comp[all_comp$compartment == "Y"]
#   compNA <- all_comp[all_comp$compartment == "NA"]
#   
#   message("\tCompartment X")
#   XvsDHS <- countOverlaps(compX, dhss_1h_chr19)
#   XvsK27 <- countOverlaps(compX, h3k27ac_1h_chr19)
#   XvsH3 <- countOverlaps(compX, h3k9me3_1h_chr19)
#   message("\t- vs DHSs : ", sum(XvsDHS), " / ", l_dhs, " = ", round(sum(XvsDHS)/l_dhs*100, 2), " %")
#   message("\t- vs H3K27ac : ", sum(XvsK27), " / ", l_k27, " = ", round(sum(XvsK27)/l_k27*100, 2), " %")
#   message("\t- vs H3K9me3 : ", sum(XvsH3), " / ", l_hist, " = ", round(sum(XvsH3)/l_hist*100, 2), " %")
#   message("")
#   
#   message("\tCompartment Y")
#   YvsDHS <- countOverlaps(compY, dhss_1h_chr19)
#   YvsK27 <- countOverlaps(compY, h3k27ac_1h_chr19)
#   YvsH3 <- countOverlaps(compY, h3k9me3_1h_chr19)
#   message("\t- vs DHSs : ", sum(YvsDHS), " / ", l_dhs, " = ", round(sum(YvsDHS)/l_dhs*100, 2), " %")
#   message("\t- vs H3K27ac : ", sum(YvsK27), " / ", l_k27, " = ", round(sum(YvsK27)/l_k27*100, 2), " %")
#   message("\t- vs H3K9me3 : ", sum(YvsH3), " / ", l_hist, " = ", round(sum(YvsH3)/l_hist*100, 2), " %")
#   message("")
#   
#   message("\tCompartment NA")
#   NAvsDHS <- countOverlaps(compNA, dhss_1h_chr19)
#   NAvsK27 <- countOverlaps(compNA, h3k27ac_1h_chr19)
#   NAvsH3 <- countOverlaps(compNA, h3k9me3_1h_chr19)
#   message("\t- vs DHSs : ", sum(NAvsDHS), " / ", l_dhs, " = ", round(sum(NAvsDHS)/l_dhs*100, 2), " %")
#   message("\t- vs H3K27ac : ", sum(NAvsK27), " / ", l_k27, " = ", round(sum(NAvsK27)/l_k27*100, 2), " %")
#   message("\t- vs H3K9me3 : ", sum(NAvsH3), " / ", l_hist, " = ", round(sum(NAvsH3)/l_hist*100, 2), " %")
#   message("")
# }

# ###### Overlaps with diffbind
# raw_diffbind <- load_diffbind_cofactors_peaks()
# diffbind <- as.list(raw_diffbind)
# sapply(diffbind, length)
# diffbind_chr19 <- lapply(diffbind, function(gr) {gr[seqnames(gr) == "chr19"]})
# sapply(diffbind_chr19, length)
# all_ov <- c("dhss_1h_chr19" = dhss_1h_chr19,
#             "thss_1h_chr19" = thss_1h_chr19,
#             "h3k27ac_1h_chr19" = h3k27ac_1h_chr19,
#             "h3k9me3_1h_chr19" = h3k9me3_1h_chr19,
#             diffbind_chr19)
# 
# for (set_comp in names(chr19_list)) {
#   message("###################")
#   message("### ", set_comp)
#   message("###################")
#   all_comp <- chr19_list[[set_comp]]
#   print(kable(table(all_comp$compartment)))
#   
#   compX <- all_comp[all_comp$compartment == "X"]
#   compY <- all_comp[all_comp$compartment == "Y"]
#   compNA <- all_comp[all_comp$compartment == "NA"]
#   
#   for (set_cdbs in names(all_ov)) {
#     message("   ### ", set_cdbs)
#     diffbind <- all_ov[[set_cdbs]]
#     l_diffbind <- length(diffbind)
#     
#     message("\tCompartment X")
#     XvsDiffbind <- countOverlaps(compX, diffbind)
#     message("\t- vs ", set_cdbs, " : ", sum(XvsDiffbind), " / ", l_diffbind, " = ", round(sum(XvsDiffbind)/l_diffbind*100, 2), " %")
#     message("")
#     
#     message("\tCompartment Y")
#     YvsDiffbind <- countOverlaps(compY, diffbind)
#     message("\t- vs ", set_cdbs, " : ", sum(YvsDiffbind), " / ", l_diffbind, " = ", round(sum(YvsDiffbind)/l_diffbind*100, 2), " %")
#     message("")
#     
#     message("\tCompartment NA")
#     NAvsDiffbind <- countOverlaps(compNA, diffbind)
#     message("\t- vs ", set_cdbs, " : ", sum(NAvsDiffbind), " / ", l_diffbind, " = ", round(sum(NAvsDiffbind)/l_diffbind*100, 2), " %")
#     message("")
#   }
# }
# 
# 
# 
# 
# 
