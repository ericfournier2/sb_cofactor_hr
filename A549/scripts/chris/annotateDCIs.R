# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

source("scripts/ckn_utils.R")
library(GenomicInteractions)
library(tidyverse)

#
makeGenomicInteractionsFromDF <- function(raw_hic) {
  anchor1 <- GRanges(raw_hic[, 1:3] %>% set_names("seqnames", "start", "end"))
  anchor2 <- GRanges(raw_hic[, 4:6] %>% set_names("seqnames", "start", "end"))
  observed <- raw_hic$logCPM
  metadata <- raw_hic %>% dplyr::select(D, logFC, logCPM, p.value, p.adj)
  
  hic_interactions <- GenomicInteractions(anchor1, anchor2)
  mcols(hic_interactions) <- cbind(counts = observed, metadata)
  
  return(hic_interactions)
}

#
annotatePeaks3D <- function(promoter_regions, regulatory_regions, GIObject) {
  annotation.features <- list(promoter = promoter_regions, enhancer = regulatory_regions)
  annotateInteractions(GIObject, annotation.features)
  return(GIObject)
}

# Load promoters
raw_promoters_A549 <- promoters(gencode31_basic_chr_TxDb, upstream = 3000, downstream = 3000, columns = "gene_id")
stdchr_promoters_A549 <- keepStdChr(raw_promoters_A549)
ensg_without_version <- str_replace(unlist(stdchr_promoters_A549$gene_id), "\\.[0-9]+$", "")
stdchr_promoters_A549$gene_id <- ensg_without_version
names(stdchr_promoters_A549) <- stdchr_promoters_A549$gene_id

# Load DCIs
hic_folder <- "input/ENCODE/A549/GRCh38/hic"
dcis_raw <- read_tsv(file.path(hic_folder, "DCIs_1h_0p05.txt"))
dcis_up <- dcis_raw %>% filter(logFC >= 1)
l_dci_up <- nrow(dcis_up)
dcis_down <- dcis_raw %>% filter(logFC <= -1)
l_dci_down <- nrow(dcis_down)

dcis_up_GI <- makeGenomicInteractionsFromDF(dcis_up)
dcis_down_GI <- makeGenomicInteractionsFromDF(dcis_down)

# Load CBDS
diffbind <- load_diffbind_cofactors_peaks()

for (set in names(diffbind)[1]) {
  regions <- diffbind[[set]]
  l_regions <- length(regions)
  names(regions) <- paste0(set, "_", 1:l_regions)
  message("### ", set, " | ", l_regions, " regions")
  
  # DCIs_up
  annot_up <- annotatePeaks3D(promoter_regions = stdchr_promoters_A549, regulatory_regions = regions, GIObject = dcis_up_GI)
  anchorOne(annot_up)
  anchorTwo(annot_up)
  
  !is.na(anchorOne(annot_up)$enhancer.id)
  !is.na(anchorTwo(annot_up)$enhancer.id)
  
  
  
  
  annot_up_ep <- annot_up[isInteractionType(annot_up, "enhancer", "promoter")]
  annot_up_ee <- annot_up[isInteractionType(annot_up, "enhancer", "enhancer")]
  annot_up_ed <- annot_up[isInteractionType(annot_up, "enhancer", "distal")]
  annot_up_pp <- annot_up[isInteractionType(annot_up, "promoter", "promoter")]
  annot_up_pd <- annot_up[isInteractionType(annot_up, "promoter", "distal")]
  annot_up_dd <- annot_up[isInteractionType(annot_up, "distal", "distal")]
  
  length(annot_up_ep) + length(annot_up_ee) + length(annot_up_ed) + length(annot_up_pp) + length(annot_up_pd) + length(annot_up_dd)
  
  
  # DCIs_down
  
}


table(regions(annot_up)$node.class)
plotInteractionAnnotations(annot_up, legend = TRUE)
