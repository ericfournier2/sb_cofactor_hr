# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

library(tidyverse)
library(ComplexHeatmap)
source("scripts/ckn_utils.R")

# Load super-enhancers
TE_list <- readRDS(file = "output/analyses/ecosystem/TE_A549_DEX_SEdb_hg38.rds")
allTE <- c(TE_list[["TE_speCTRL"]], TE_list[["TE_speDEX"]], TE_list[["TE_common"]])
TE_list <- append(TE_list, allTE)
names(TE_list) <- c("TE_speCTRL", "TE_speDEX", "TE_common", "allTE")
sapply(TE_list, length)

# Load CDBS
CDBS <- load_diffbind_cofactors_peaks()

# Define MBC peak set
MBC_UP <- GenomicRanges::reduce(c(CDBS[["MED1_UP"]], CDBS[["BRD4_UP"]], CDBS[["CDK9_UP"]])) # 8253
MBC_DOWN <- GenomicRanges::reduce(c(CDBS[["MED1_DOWN"]], CDBS[["BRD4_DOWN"]], CDBS[["CDK9_DOWN"]])) # 7492
MBC_UNBIASED <- GenomicRanges::reduce(c(CDBS[["MED1_UNBIASED"]], CDBS[["BRD4_UNBIASED"]], CDBS[["CDK9_UNBIASED"]])) # 22749
MBC_list <- GRangesList("MBC_UP" = MBC_UP, "MBC_DOWN" = MBC_DOWN, "MBC_UNBIASED" = MBC_UNBIASED)

#
CDBS <- append(CDBS,
               GRangesList("MBC_UP" = MBC_UP, "MBC_DOWN" = MBC_DOWN, "MBC_UNBIASED" = MBC_UNBIASED))

# CDBS viewpoint
for (cdbs_name in names(CDBS)) {
  cdbs <- CDBS[[cdbs_name]]
  l_cdbs <- length(cdbs)
  message("##### ", cdbs_name, " | ", l_cdbs, " regions")
  for (te_name in names(TE_list)) {
    te <- TE_list[[te_name]]
    ov <- subsetByOverlaps(cdbs, te)
    message("    # ", te_name, " > ", length(ov), " / ", l_cdbs, " = ", round(length(ov)/l_cdbs*100, 2), " %")
  }
  ov_nobinding <- subsetByOverlaps(cdbs, allTE, invert = TRUE)
  message("    # not in TE > ", length(ov_nobinding), " / ", l_cdbs, " = ", round(length(ov_nobinding)/l_cdbs*100, 2), " %")
}

# SE viewpoint
all_cdbs <- unlist(GRangesList(CDBS))

for (te_name in names(TE_list)) {
  te <- TE_list[[te_name]]
  l_te <- length(te)
  message("##### ", te_name, " | ", l_te, " regions")
  for (cdbs_name in names(CDBS)) {
    cdbs <- CDBS[[cdbs_name]]
    ov <- subsetByOverlaps(te, cdbs)
    message("    # ", cdbs_name, " > ", length(ov), " / ", l_te, " = ", round(length(ov)/l_te*100, 2), " %")
  }
}
