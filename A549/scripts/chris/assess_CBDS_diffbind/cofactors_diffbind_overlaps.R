# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

library(ComplexHeatmap)
library(GenomicOperations)
source("scripts/ckn_utils.R")

#####
cofactors <- load_diffbind_cofactors_peaks()
# cofactors <- cofactors[grep("BRD4|MED1|CDK9|NIPBL|SMC1A", names(cofactors))]
cofactors_DOWN <- cofactors[grep("DOWN", names(cofactors))]
cofactors_UP <- cofactors[grep("UP", names(cofactors))]
cofactors_UNBIASED <- cofactors[grepl("UNBIASED", names(cofactors))]

sapply(cofactors, length)

# UpSet plot
inter_cofactors <- GenomicOperations::GenomicOverlaps(cofactors)
matrix_cofactors <- inter_cofactors@matrix
sum(matrix_cofactors > 1)
matrix_cofactors[matrix_cofactors > 1] <- 1
sum(matrix_cofactors > 1)
colnames(matrix_cofactors)

cofactors_names <- c("MED1", "BRD4", "CDK9", "NIPBL", "SMC1A")
cofactors_set_order <- c(paste0(cofactors_names, "_DOWN"))
m4 <- make_comb_mat(matrix_cofactors, remove_empty_comb_set = TRUE)

upset <- displayUpSet(combMat = m4, threshold = 10, customSetOrder = cofactors_set_order)
upset

# Venn diagramm
MBC_UP <- cofactors_UP[grep("MED1|BRD4|CDK9", names(cofactors_UP))]
plotVenn(MBC_UP)

MBC_DOWN <- cofactors_DOWN[grep("MED1|BRD4|CDK9", names(cofactors_DOWN))]
plotVenn(MBC_DOWN)

MBC_UNBIASED <- cofactors_UNBIASED[grep("MED1|BRD4|CDK9", names(cofactors_UNBIASED))]
plotVenn(MBC_UNBIASED)



