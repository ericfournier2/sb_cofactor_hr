# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

library(tidyverse)
library(ComplexHeatmap)
library(circlize)
source("scripts/ckn_utils.R")

######
saveHeatmap <- function(heatmap, output_dir, output_file, width_val = 25, height_val = 22, format = "pdf") {
  output_filepath <- file.path(output_dir, paste0(output_file, ".", format))
  pdf(file = output_filepath, width = width_val, height = height_val)
  print(heatmap)
  dev.off()
  message(" > Heatmap saved in ", output_filepath)
}

#####
today <- get_today()

### Call python script to generate framptongram matrix
python_script_path <- "/Users/chris/Desktop/sb_cofactor_hr/A549/scripts/chris/assess_CBDS_diffbind/clustergram/generate_framptongram_matrix.py"
input_path <- "/Users/chris/Desktop/sb_cofactor_hr/A549/scripts/chris/assess_CBDS_diffbind/clustergram/bedfile_CDBS_MBC.txt"
matrix_path <- "/Users/chris/Desktop/sb_cofactor_hr/A549/scripts/chris/assess_CBDS_diffbind/clustergram/framptongram_matrix_CDBS_MBC.txt"

call <- paste("python", python_script_path, input_path, matrix_path)
message(call)
system(call)

### Generate heatmap from framptongram matrix
data <- read.table(matrix_path, header = TRUE)

direction <- c("UP", "DOWN", "UNBIASED")
sample_names <- c(paste("MED1", direction, sep  = "_"),
                  paste("BRD4", direction, sep  = "_"),
                  paste("CDK9", direction, sep  = "_"))

mat <- as.matrix(data[, 4:ncol(data)])
colnames(mat) <- sample_names
rownames(mat) <- sample_names

customColors = colorRamp2(c(-1, 0, 1), c("#0f4259", "white", "#800020"))

hm <- Heatmap(mat, col = customColors, name = "Correlation",
              row_names_side = "left",
              column_names_side = "top", column_names_rot = 45,
              row_dend_width = unit(50, "mm"),
              show_column_dend = FALSE,
              rect_gp = gpar(col = "white", lwd = 1),
              cell_fun = function(j, i, x, y, width, height, fill) {
              grid.text(sprintf("%.2f", mat[i, j]), x, y, gp = gpar(fontsize = 10))
              })
hm

saveHeatmap(hm,
            output_dir = "output/analyses/ecosystem/heatmap_clustergram_CDBS",
            output_file = paste(today, "heatmap_clustergram_CBDS_MBC_3_2billion", sep = "_"),
            width_val = 15,
            height_val = 12)
