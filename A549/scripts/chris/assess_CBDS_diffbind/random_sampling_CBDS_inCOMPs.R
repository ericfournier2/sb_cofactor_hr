# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

# TODO adapt the code for COMPARTMENTS

library(tidyverse)
source("scripts/ckn_utils.R")
source("scripts/load_hic_data.R")

#####
countCDBSInComp <- function(comps, diffbind) {
  comp_id <- paste(seqnames(comps), start(comps), end(comps), sep = "_")
  df <- data.frame("comp_id" = comp_id)
  
  for (set in names(diffbind)) {
    regions <- diffbind[[set]]
    countInComp <- countOverlaps(comps, regions)
    df <- cbind(df, countInCad)
  }
  colnames(df) <- c("comp_id", names(diffbind))
  return(df)
}

#####
processShuffleBedOutput <- function(chr_vec) {
  splitted <- str_split(chr_vec, pattern = "\t")
  seqnames <- splitted %>% purrr::map(1) %>% unlist
  start <- splitted %>% purrr::map(2) %>% unlist
  end <- splitted %>% purrr::map(3) %>% unlist
  genomic_regions <- data.frame(seqnames, start, end) %>% GRanges
  return(genomic_regions)
}

######
computeRatio <- function(vec_up, vec_down) {
  res <- rep("TMP", length(vec_up))
  for (i in 1:length(vec_up)) {
    val_up <- vec_up[i]
    val_down <- vec_down[i]
    if (val_up == 0 & val_down == 0) {
      res[i] = -1
    } else {
      res[i] = val_up/(val_up+val_down)
    }
  }
  return(as.numeric(res))
}

######
pwd <- getwd()
chromSizehg38_path <- file.path(pwd, "input/hg38.chrom.sizes")

# Load compartments
allCOMPs <- readRDS(file = "output/hic/compartmentsAB/allCOMPs.rds")

###### Load MBC CDBS
# Load diffbind
diffbind <- load_diffbind_cofactors_peaks(c("MED1", "BRD4", "CDK9"))

# Build MED1_BRD4_CDK9
MBC_UP <- GenomicRanges::reduce(c(diffbind[["MED1_UP"]], diffbind[["BRD4_UP"]], diffbind[["CDK9_UP"]])) # 8253
MBC_DOWN <- GenomicRanges::reduce(c(diffbind[["MED1_DOWN"]], diffbind[["BRD4_DOWN"]], diffbind[["CDK9_DOWN"]])) # 7492
MBC_UNBIASED <- GenomicRanges::reduce(c(diffbind[["MED1_UNBIASED"]], diffbind[["BRD4_UNBIASED"]], diffbind[["CDK9_UNBIASED"]])) # 22749
MBC <- GRangesList("MBC_UP" = MBC_UP, "MBC_DOWN" = MBC_DOWN, "MBC_UNBIASED" = MBC_UNBIASED)

MBC_folder <- "output/chip-pipeline-GRCh38/binding_diff/A549_MBC"
MBC_UP_path <- file.path(pwd, MBC_folder, "A549_MBC_UP.bed")
MBC_UNBIASED_path <- file.path(pwd, MBC_folder, "A549_MBC_UNBIASED.bed")
MBC_DOWN_path <- file.path(pwd, MBC_folder, "A549_MBC_DOWN.bed")

###### Observed distribution of MBC_ratio
nTAD <- length(TADs_Reddy_1h)
countTad_obs <- countCDBSInTad(TADs_Reddy_1h, MBC)
countTad_ratio_obs <- countTad_obs %>% mutate(MBC_R = computeRatio(MBC_UP, MBC_DOWN),
                                              MBC_S = MBC_UP + MBC_DOWN,
                                              width_TADs = width(TADs_Reddy_1h))
MBC_R_obs <- countTad_ratio_obs %>% dplyr::filter(MBC_R != -1) %>% dplyr::pull(MBC_R)
TADs_with_binding_obs <- length(MBC_R_obs)

#####
nb_simulation <- 10
seed <- sample(10000, nb_simulation)
pval_list <- c()
TADs_with_binding_list <- c()
D_list <- c()

plot(ecdf(x = MBC_R_obs), asp = 1, xlim = c(0, 1), ylim = c(0, 1), main = "ECDF")
# plot(MBC_R_obs, col = 2)
# plot(sort(MBC_R_obs), col = 2)
# qqnorm(MBC_R_obs, col = "red")

# mypar()

for (i in 1:nb_simulation) {
  message("#### Simulation ", i, " / ", nb_simulation )
  
  # shuffle UP
  call_shuffle_UP <- paste("bedtools", "shuffle",
                           "-chrom",
                           "-seed", seed[i],
                           "-i", MBC_UP_path,
                           "-g", chromSizehg38_path)
  message(call_shuffle_UP)
  MBC_UP_exp <- system(call_shuffle_UP, intern = TRUE) %>% processShuffleBedOutput
  
  # shuffle down
  call_shuffle_DOWN <- paste("bedtools", "shuffle",
                           "-chrom",
                           "-seed", seed[i],
                           "-i", MBC_DOWN_path,
                           "-g", chromSizehg38_path)
  message(call_shuffle_DOWN)
  MBC_DOWN_exp <- system(call_shuffle_DOWN, intern = TRUE) %>% processShuffleBedOutput
  
  # shuffle unbiased
  call_shuffle_UNBIASED <- paste("bedtools", "shuffle",
                           "-chrom",
                           "-seed", seed[i],
                           "-i", MBC_UNBIASED_path,
                           "-g", chromSizehg38_path)
  message(call_shuffle_UNBIASED)
  MBC_UNBIASED_exp <- system(call_shuffle_UNBIASED, intern = TRUE) %>% processShuffleBedOutput
  
  # gather everything
  MBC_exp <- GRangesList("MBC_UP_exp" = MBC_UP_exp, "MBC_DOWN_exp" = MBC_DOWN_exp, "MBC_UNBIASED_exp" = MBC_UNBIASED_exp)
  
  #
  countTad_exp <- countCDBSInTad(TADs_Reddy_1h, MBC_exp)
  countTad_ratio_exp <- countTad_exp %>% mutate(MBC_R = computeRatio(MBC_UP_exp, MBC_DOWN_exp),
                                                MBC_S = MBC_UP_exp + MBC_DOWN_exp,
                                                width_TADs = width(TADs_Reddy_1h))
  MBC_R_exp <- countTad_ratio_exp %>% dplyr::filter(MBC_R != -1) %>% dplyr::pull(MBC_R)
  TADs_with_binding_exp <- length(MBC_R_exp)
  
  KS <- ks.test(MBC_R_obs, MBC_R_exp)
  KS_pval <- KS$p.value
  KS_D <- KS$statistic
  
  lines(ecdf(x = MBC_R_exp), col = i + 1)
  # lines(sort(MBC_R_exp), col = i + 1)
  
  pval_list <- c(pval_list, KS_pval)
  TADs_with_binding_list <- c(TADs_with_binding_list, TADs_with_binding_exp)
  D_list <- c(D_list, KS_D)
  
  message("")
}

summary(pval_list)
df <- data.frame(rep("pval", nb_simulation), value = pval_list) %>% set_names(c("group", "value"))
ggplot(df, aes(x=group, y=value)) +
  geom_boxplot(outlier.shape = NA) +
  geom_jitter(color="black", size=0.4, alpha=0.9, width = 0.25, height = 0) +
  theme_minimal() +
  theme(axis.title.x=element_blank()) +
  geom_hline(yintercept = 0.05, col = "red") +
  ggtitle("Distribution of p-values (KS test)")
sum(pval_list <= 0.05)
sum(pval_list <= 0.05) / nb_simulation * 100

df2 <- data.frame(rep("nb", nb_simulation), value = TADs_with_binding_list) %>% set_names(c("group", "TADs"))
ggplot(df2, aes(x=group, y=TADs)) +
  geom_boxplot(outlier.shape = NA) +
  geom_jitter(color="black", size=0.4, alpha=0.9, width = 0.25) +
  theme_minimal() +
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank()) +
  ylab("#TADs") +
  geom_hline(yintercept = 3907, col = "red") +
  ggtitle("Number of TADs with differential binding of MBC")
summary(TADs_with_binding_list)


boxplot(D_list)
summary(D_list)

sort(D_list)
sort(pval_list)
cor(sort(D_list), sort(pval_list))

par(mfrow = c(2, 2))
hist(MBC_R_obs, ylim = c(0, 2000), main = "Frequency of MBC_R_obs")
hist(MBC_R_exp, ylim = c(0, 2000), main = "Frequency of MBC_R_exp")
h1 <- hist(MBC_R_obs, plot = FALSE)
h1$density <- h1$counts/sum(h1$counts)*100
plot(h1, freq = FALSE, ylab = "Percentage", main = "Percentage of MBC_R_obs", ylim = c(0, 50))
h2 <- hist(MBC_R_exp, plot = FALSE)
h2$density <- h2$counts/sum(h2$counts)*100
plot(h2, freq = FALSE, ylab = "Percentage", main = "Percentage of MBC_R_exp", ylim = c(0, 50))

par(mfrow = c(1, 1))
plot(ecdf(x = MBC_R_obs))
lines(ecdf(x = MBC_R_exp), col = 2)


qqnorm(MBC_R_obs, pch = 1, frame = FALSE)
qqline(MBC_R_obs, col = "steelblue", lwd = 2)

library("car")
qqPlot(MBC_R_obs)

library(EnvStats)
EnvStats::qqPlot(x = MBC_R_exp,y = MBC_R_obs,
                 distribution = NULL,
                 plot.type = "Q-Q",
                 add.line = TRUE)
