# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

library(tidyverse)
library(ComplexHeatmap)
source("scripts/ckn_utils.R")

# Load super-enhancers
TE_list <- readRDS(file = "output/analyses/ecosystem/TE_A549_DEX_SEdb_hg38.rds")
allTE <- c(TE_list[["TE_speCTRL"]], TE_list[["TE_speDEX"]], TE_list[["TE_common"]])
TE_list <- append(TE_list, allTE)
names(TE_list) <- c("TE_speCTRL", "TE_speDEX", "TE_common", "allTE")
sapply(TE_list, length)

# Load GR binding
all_gr_regions <- load_reddy_gr_binding_consensus()
gr_regions <- GRangesList(c(all_gr_regions[grep("minutes", names(all_gr_regions))], "1 hour" = all_gr_regions[["1 hour"]]))
lapply(as.list(gr_regions), length)
# gr_regions_gathered <- unlist(gr_regions)
# length(gr_regions_gathered)

# GR viewpoint
for (gr_name in names(gr_regions)) {
  gr <- gr_regions[[gr_name]]
  l_gr <- length(gr)
  message("##### ", gr_name, " | ", l_gr, " regions")
  for (te_name in names(TE_list)) {
    te <- TE_list[[te_name]]
    ov <- subsetByOverlaps(gr, te)
    message("    # ", te_name, " > ", length(ov), " / ", l_gr, " = ", round(length(ov)/l_gr*100, 2), " %")
  }
  ov_nobinding <- subsetByOverlaps(gr, allTE, invert = TRUE)
  message("    # not in SE > ", length(ov_nobinding), " / ", l_gr, " = ", round(length(ov_nobinding)/l_gr*100, 2), " %")
}

# SE viewpoint
for (te_name in names(TE_list)) {
  te <- TE_list[[te_name]]
  l_te <- length(te)
  message("##### ", te_name, " | ", l_te, " regions")
  for (gr_name in names(gr_regions)) {
    gr <- gr_regions[[gr_name]]
    ov <- subsetByOverlaps(te, gr)
    message("    # ", gr_name, " > ", length(ov), " / ", l_te, " = ", round(length(ov)/l_te*100, 2), " %")
  }
}