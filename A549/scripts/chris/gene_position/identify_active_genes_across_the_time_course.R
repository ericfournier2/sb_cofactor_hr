# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

library(tidyverse)

raw <- read_tsv("results/a549_dex_time_points/raw_counts_with_colnames.txt")
# how many rows/genes? 60483

library_size <- colSums(raw[, -1])
rpm <- raw[, -1] / library_size * 1000000
rownames(rpm) <- raw$gene_id

keep_active_genes <- function(rpm_mat, threshold) {
  res <- list()
  TF_mat <- (rpm_mat >= threshold)
  ngenes <- nrow(rpm)
  active_mat <- apply(TF_mat, 1, any)
  gene_list <- names(active_mat[active_mat == TRUE])
  message("With RPM >= ", threshold, " >>> ", length(gene_list), " / ", ngenes, " genes")
  res$nb_all_genes <- ngenes
  res$threshold <- threshold
  res$gene_list <- gene_list
  res$nb_genes <- length(gene_list)
  return(res)
}

ths <- c()
nb_kept_genes <- c()
active_genes <- list()
for (th in c(seq(0, 1, 0.01), 2:10, seq(20, 100, 10), seq(200, 1000, 100), seq(2000, 20000, 1000))) {
  res <- keep_active_genes(rpm, threshold = th)
  
  # save results
  if (th >= 1) {
    active_genes[[paste0("th", th)]] <- res
  }
  
  # for visualisation
  ths <- c(ths, th)
  nb_kept_genes <- c(nb_kept_genes , res$nb_genes)
}

# save results
saveRDS(active_genes, file = "output/analyses/ecosystem/gene_position/active_genes.rds")

# Print results
df_res <- data.frame(threshold = ths, nb_active_genes = nb_kept_genes)
knitr::kable(df_res[1:50, ], row.names = FALSE, digits = 2)
knitr::kable(df_res[51:100, ], row.names = FALSE)
knitr::kable(df_res[101:150, ], row.names = FALSE)

# Visualisation
scaleX <- c(0, 0.01, 0.02, 0.03, 0.05,
            0.1, 0.2, 0.3, 0.5,
            1, 2, 3, 5, 
            10, 20, 30, 50,
            100, 200, 300, 500,
            1000, 2000, 3000, 5000,
            10000,
            20000)

ggplot(df_res, aes(x = threshold, y = nb_active_genes)) +
  geom_line(color="#22427C", size=1.5, alpha=0.9) +
  geom_point(shape=23, color="#C0C0C0", fill="#FFD700", size=1) +
  ylab("Number of active genes") +
  xlab("log(RPM threshold)") +
  scale_y_continuous(breaks = seq(0, 60000, 5000)) +
  scale_x_continuous(trans = 'log2', breaks = scaleX, labels = scaleX) +
  theme_minimal() +
  ggtitle("Number of active genes",
          "A gene is active if: 1) RPM >= threshold, 2) In at least one replicate across the time course")
