# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

library(ComplexHeatmap)
library(circlize)
library(knitr)
library(tidyverse)
library(grid)
source("scripts/ckn_utils.R")

######
saveHeatmap <- function(heatmap, output_dir, output_file, width_val = 25, height_val = 22, format = "pdf") {
        output_filepath <- file.path(output_dir, paste0(output_file, ".", format))
        pdf(file = output_filepath, width = width_val, height = height_val)
        print(heatmap)
        dev.off()
        message(" > Heatmap saved in ", output_filepath)
}

######
isGeneInTad <- function(gene_id, genesBelongTADs) {
        nb_tad <- length(genesBelongTADs$gene_names)
        # resTF <- vector(length = nb_tad)
        tads_index <- c()
        for (i in 1:nb_tad) {
                TF <- gene_id %in% genesBelongTADs$gene_names[[i]]
                if (sum(TF) > 0) {
                        tads_index <- c(tads_index, i)
                        # print(genesBelongTADs$gene_names[[i]])
                        # print(genesBelongTADs$width_TADs[i])
                        # print(genesBelongTADs[i, ])
                }
        }
        tads_df <- genesBelongTADs[tads_index, ]
        return(tads_df)
}

######
geneName_heatmap <- function(ensg, symbol, FCmat) {
        index_list <- c()
        symbol_list <- c()
        annot_genes <- list()
        for (i in 1:length(ensg)) {
                id <- ensg[i]
                n <- which(rownames(FCmat) == id)
                if (length(n) != 0) {
                        index_list <- c(index_list, n)
                        symbol_list <- c(symbol_list, symbol[i])
                }
        }
        annot_genes$index <- index_list
        annot_genes$symbol <- symbol_list
        return(annot_genes)
}

#####
set_TAD_direction2 <- function(ratio) {
        res <- rep(NA, length(ratio))
        res[ratio == 1] <- "TAD_onlyUP"
        res[ratio >= 0.6 & ratio < 1] <- "TAD_UP"
        res[ratio <= 0.4 & ratio > 0] <- "TAD_DOWN"
        res[ratio == 0] <- "TAD_onlyDOWN"
        res[ratio == -1] <- "TAD_NB"
        res[ratio > 0.4 & ratio < 0.6] <- "TAD_UNBIASED"
        return(res)
}

#####
today <- get_today()

##### Load FC values across time course
raw_fc <- read_delim("results/a549_dex_time_points/FC_mat.csv", delim = ",")

#### Set threshold RPM
ths <- c("th1", "th5", "th10", "th50", "th100", "th500",
         "th1000", "th2000", "th3000", "th4000", "th5000",
         "th10000")
        
##### Load "do genes belong to a TAD?"
genesBelongTADs <- readRDS(file = "output/analyses/ecosystem/gene_position/genesBelongTADs.rds")

##### TEST
for (th in rev(ths)[5]) {
        ##### Load actives genes
        # active_genes.rds have been generated in scripts/chris/gene_position/identify_active_genes_across_the_time_course.R
        active_genes_raw <- readRDS(file = "output/analyses/ecosystem/gene_position/active_genes.rds")
        # According to threshold = 1 > 15555 genes
        genelist_th <- active_genes_raw[[th]]$gene_list
        nb_genes <- length(genelist_th)
        
        ##### Are active genes in FC matrix?
        sum(genelist_th %in% raw_fc$gene_id) # 15551/15555 genes have a row in FC matrix
        ## What are the not present genes?
        genelist_th[!(genelist_th %in% raw_fc$gene_id)]
        
        #### 
        FC <- raw_fc %>% dplyr::filter(gene_id %in% genelist_th) # %>% dplyr::select(gene_id, "0.5", "1", "2")
        colnames(FC) <- c("gene_id", paste0(colnames(FC)[-1], "h"))
        nb_genes_inFC <- nrow(FC)
        # rownames(FC) <- FC$gene_id
        
        ##### Process binded genes for annotation
        # binded_genes.rds have been generated in script/chris/ecosystem/linear_and_3D_annotation.R
        binded_genes <- readRDS(file = "output/analyses/ecosystem/gene_position/binded_genes.rds")
        
        for (elt in names(binded_genes)[16:19]) {
                message("# ", elt)
                target <- binded_genes[[elt]]
                print(elementNROWS(target))
                
                genes_linear <- target$linear
                genes_via3D <- target$via3D
                
                linear_inTH <- FC$gene_id %in% genes_linear
                nb_linear <- sum(linear_inTH)
                message("\t\t\t# linear : ", nb_linear, " / ", length(genes_linear), "   ", round(nb_linear/length(genes_linear)*100, 2), " %")
                
                via3D_inTH <- FC$gene_id %in% genes_via3D
                nb_via3D <- sum(via3D_inTH)
                message("\t\t\t# via3D : ", nb_via3D, " / ", length(genes_via3D), "   ", round(nb_via3D/length(genes_via3D)*100, 2), " %")
                
                df_tmp <- data.frame(linear_inTH, via3D_inTH)
                colnames(df_tmp) <- paste(elt, c("linear", "via3D"), sep = "_")
                
                FC <- cbind(FC, df_tmp)
        }

        # Assign a TAD to each gene
        tbl_colnames <- c("gene_id", colnames(genesBelongTADs))
        TAD_infos <- as_tibble(data.frame(matrix(nrow=0,ncol=length(tbl_colnames))))
        # colnames(TAD_infos) <- tbl_colnames
        for (gene in FC$gene_id) {
                tads_df <- isGeneInTad(gene, genesBelongTADs)
                message(gene, " is present in ", nrow(tads_df), " TAD(s)")
                #sumtest_list <- c(sumtest_list, sum(test))
                
                if (nrow(tads_df) == 1) {
                        TAD_infos <- rbind(TAD_infos, cbind(gene_id = gene, tads_df))
                } else if (nrow(tads_df) == 0) {
                        TAD_infos <- rbind(TAD_infos, rep(NA, ncol(tads_df) + 1))
                        colnames(TAD_infos) <- tbl_colnames
                } else if (nrow(tads_df) > 1) {
                        # Let's assing the smallest TAD with that gene
                        # print(knitr::kable(tads_df[, c(1, 8, 10)] %>% arrange(width_TADs)))
                        minTAD <- min(tads_df$width_TADs)
                        # print(minTAD)
                        min_tad_df <- tads_df %>% dplyr::filter(width_TADs == minTAD)
                        # print(knitr::kable(min_tad_df[, c(1, 8, 10)]))
                        
                        TAD_infos <- rbind(TAD_infos, cbind(gene_id = gene, min_tad_df))
                }
        }
        
        # gather FC and TAD_infos in one data.frame
        FC_TAD <- left_join(FC, TAD_infos, by = "gene_id")
        
        ##########
        # With GR
        ##########
        
        FC_TAD_withGR <- FC_TAD %>% dplyr::filter(GR_linear == TRUE | GR_via3D == TRUE)
        nb_deg_withGR <- nrow(FC_TAD_withGR)
        
        ##### FCmat
        # FCmat <- FC %>% dplyr::select("0.5h", "1h", "2h", "3h", "4h", "5h", "6h", "7h", "8h", "10h", "12h") %>% as.matrix
        FCmat_withGR <- FC_TAD_withGR %>% dplyr::select("0.5h", "1h", "2h", "3h", "4h", "5h", "6h") %>% as.matrix
        rownames(FCmat_withGR) <- FC_TAD_withGR$gene_id
        # FCmat <- FC %>% dplyr::select("0.5h", "1h") %>% as.matrix
        # rownames(FCmat) <- raw_fc2$gene_id[raw_fc2$gene_id %in% genelist_deg]
        # max(FCmat) # 
        # min(FCmat) #
        
        cofactors <- c("MBC")
        order_column <- c(
                paste(rep(cofactors, each = 2), "UP", c("linear", "via3D"), sep = "_"),
                paste(rep(cofactors, each = 2), "DOWN", c("linear", "via3D"), sep = "_"),
                paste("GR", c("linear", "via3D"), sep = "_"),
                paste(rep(cofactors, each = 2), "UNBIASED", c("linear", "via3D"), sep = "_")
        )
        #order_column
        
        ##### TFmat
        TFmat_withGR <- FC_TAD_withGR %>% dplyr::select(order_column) %>% as.matrix
        
        ##### Gene name annotation # "MCL1", "CLK1", "PTGER4", "SNAI2"
        ensg <- c("ENSG00000167772", "ENSG00000166592","ENSG00000166589", "ENSG00000095752", "ENSG00000179094",
                  "ENSG00000108342", "ENSG00000185338", "ENSG00000156427", "ENSG00000157514", "ENSG00000128016",
                  "ENSG00000198517")
        symbol <- c("ANGPTL4", "RRAD", "CDH16", "IL11", "PER1",
                    "CSF3", "SOCS1", "FGF18", "TSC22D3", "ZFP36",
                    "MAFK")
        
        annot_genes_withGR <- geneName_heatmap(ensg, symbol, FCmat_withGR)
        
        ##### TADmat
        TADmat_raw_withGR <- FC_TAD_withGR %>% mutate(TADonly = ifelse(MBC_R == 1, "onlyUp", ifelse(MBC_R == 0, "onlyDown", NA))) %>% 
                dplyr::select(MBC_R, TADonly)
        TADmat_ratio_withGR <- TADmat_raw_withGR %>% dplyr::select(MBC_R) %>% as.matrix
        TADmat_status_withGR <-  TADmat_raw_withGR %>% dplyr::select(TADonly) %>% as.matrix
        TADColors <-  colorRamp2(c(-1, -0.1, 0, 0.5, 1), c("black", "black", "#1E8449", "white", "#800020"))
        
        ##### Annotation right
        if (length(annot_genes_withGR$index) != 0) {
                annot_right <- rowAnnotation(genes = anno_mark(at = annot_genes_withGR$index,
                                                               labels = annot_genes_withGR$symbol),
                                             binding = TFmat_withGR,
                                             TAD_ratio = TADmat_ratio_withGR,
                                             TAD_status = TADmat_status_withGR,
                                             col = list(binding = c("TRUE" = "#22427C", "FALSE" = "#EFEFEF"),
                                                        TAD_ratio = TADColors,
                                                        TAD_status = c("onlyUp" = "#800020", "onlyDown" = "#1E8449")),
                                             na_col = "#FFD700",
                                             annotation_name_side = "bottom",
                                             gap = unit(2, "mm"))
        } else {
                annot_right <- rowAnnotation(binding = TFmat_withGR,
                                             TAD_ratio = TADmat_ratio_withGR,
                                             TAD_status = TADmat_status_withGR,
                                             col = list(binding = c("TRUE" = "#22427C", "FALSE" = "#EFEFEF"),
                                                        TAD_ratio = TADColors,
                                                        TAD_status = c("onlyUp" = "#800020", "onlyDown" = "#1E8449")),
                                             na_col = "#FFD700",
                                             annotation_name_side = "bottom",
                                             gap = unit(2, "mm"))
        }
        
        # if (length(index_list) != 0) {
        #         annot_right <- rowAnnotation(genes = anno_mark(at = annot_genes$index,
        #                                                        labels = annot_genes$symbol))
        #         annot_left <- rowAnnotation("GR binding" = TF_mat_GR[, 3],
        #                                     annotation_width = unit(45, "cm"),
        #                                     col = list("GR binding" = c("TRUE" = "#22427C", "FALSE" = "#EFEFEF")),
        #                                     annotation_name_side = "top")
        #         
        # }
        
        # genes = anno_mark(at = annot_genes$index,
        #                   labels = annot_genes$symbol),
        
        ##### Set color scale
        # col_fun = colorRamp2(c(-2, -1, 0, 1, 2), c("#1E8449", "#1E8449", "black", "#800020", "#800020"))
        col_fun = colorRamp2(c(-4, -1.5, 0, 1.5, 4), c("#1E8449", "#1E8449", "black", "#800020", "#800020"))
        
        ##### Generate heatmap
        FC_heatmap_withGR <- Heatmap(FCmat_withGR, name = "log2(FC)",
                              right_annotation = annot_right,
                              # left_annotation = annot_left,
                              col = col_fun, na_col = "white",
                              show_row_names = FALSE,
                              column_names_side = "top", column_names_rot = 45,
                              row_dend_side = "left", row_dend_width = unit(4, "cm"),
                              column_order = colnames(FCmat_withGR),
                              # column_title = paste0("DEGs between ", timepoint, " (with GR binding) : ", nb_deg_withGR, " genes\n",
                              #                       # nb_upreg, " upregulated genes, ", nb_downreg, " downregulated genes\n",
                              #                       "log2(FC) = ", log, " | ", "FDR = ", fdr),
                              width = unit(7, "cm"),
                              show_row_dend = TRUE
                              
                              # rect_gp = gpar(col = "white", lwd = 0),
        )
        FC_heatmap_withGR
        
        # saveHeatmap(FC_heatmap_withGR, output_dir = "output/analyses/ecosystem/gene_position",
        #             output_file = paste(today, paste0(heatmap_output_name, "_withGR"), sep = "_"),
        #             width_val = 17, height_val = 9, format = "pdf")
        
        ##############################################################################################################
        # Without GR
        ##############################################################################################################
        
        FC_TAD_withoutGR <- FC_TAD %>% dplyr::filter(GR_linear == FALSE & GR_via3D == FALSE)
        nb_deg_withoutGR <- nrow(FC_TAD_withoutGR)
        
        ##### FCmat
        # FCmat <- FC %>% dplyr::select("0.5h", "1h", "2h", "3h", "4h", "5h", "6h", "7h", "8h", "10h", "12h") %>% as.matrix
        FCmat_withoutGR <- FC_TAD_withoutGR %>% dplyr::select("0.5h", "1h", "2h", "3h", "4h", "5h", "6h") %>% as.matrix
        rownames(FCmat_withoutGR) <- FC_TAD_withoutGR$gene_id
        # FCmat <- FC %>% dplyr::select("0.5h", "1h") %>% as.matrix
        # rownames(FCmat) <- raw_fc2$gene_id[raw_fc2$gene_id %in% genelist_deg]
        # max(FCmat) # 
        # min(FCmat) #
        
        cofactors <- c("MBC")
        order_column <- c(
                paste(rep(cofactors, each = 2), "UP", c("linear", "via3D"), sep = "_"),
                paste(rep(cofactors, each = 2), "DOWN", c("linear", "via3D"), sep = "_"),
                paste("GR", c("linear", "via3D"), sep = "_"),
                paste(rep(cofactors, each = 2), "UNBIASED", c("linear", "via3D"), sep = "_")
        )
        #order_column
        
        ##### TFmat
        TFmat_withoutGR <- FC_TAD_withoutGR %>% dplyr::select(order_column) %>% as.matrix
        
        ##### Gene name annotation # "MCL1", "CLK1", "PTGER4", "SNAI2"
        ensg <- c("ENSG00000167772", "ENSG00000166592","ENSG00000166589", "ENSG00000095752", "ENSG00000179094",
                  "ENSG00000108342", "ENSG00000185338", "ENSG00000156427", "ENSG00000157514", "ENSG00000128016",
                  "ENSG00000198517")
        symbol <- c("ANGPTL4", "RRAD", "CDH16", "IL11", "PER1",
                    "CSF3", "SOCS1", "FGF18", "TSC22D3", "ZFP36",
                    "MAFK")
        
        annot_genes_withoutGR <- geneName_heatmap(ensg, symbol, FCmat_withoutGR)
        
        ##### TADmat
        TADmat_raw_withoutGR <- FC_TAD_withoutGR %>% mutate(TADonly = ifelse(MBC_R == 1, "onlyUp", ifelse(MBC_R == 0, "onlyDown", NA))) %>% 
                dplyr::select(MBC_R, TADonly)
        TADmat_ratio_withoutGR <- TADmat_raw_withoutGR %>% dplyr::select(MBC_R) %>% as.matrix
        TADmat_status_withoutGR <-  TADmat_raw_withoutGR %>% dplyr::select(TADonly) %>% as.matrix
        TADColors <-  colorRamp2(c(-1, -0.1, 0, 0.5, 1), c("black", "black", "#1E8449", "white", "#800020"))
        
        ##### Annotation right
        if (length(annot_genes_withoutGR$index) != 0) {
                annot_right <- rowAnnotation(genes = anno_mark(at = annot_genes_withoutGR$index,
                                                               labels = annot_genes_withoutGR$symbol),
                                             binding = TFmat_withoutGR,
                                             TAD_ratio = TADmat_ratio_withoutGR,
                                             TAD_status = TADmat_status_withoutGR,
                                             col = list(binding = c("TRUE" = "#22427C", "FALSE" = "#EFEFEF"),
                                                        TAD_ratio = TADColors,
                                                        TAD_status = c("onlyUp" = "#800020", "onlyDown" = "#1E8449")),
                                             na_col = "#FFD700",
                                             annotation_name_side = "bottom",
                                             gap = unit(2, "mm"))
        } else {
                annot_right <- rowAnnotation(binding = TFmat_withoutGR,
                                             TAD_ratio = TADmat_ratio_withoutGR,
                                             TAD_status = TADmat_status_withoutGR,
                                             col = list(binding = c("TRUE" = "#22427C", "FALSE" = "#EFEFEF"),
                                                        TAD_ratio = TADColors,
                                                        TAD_status = c("onlyUp" = "#800020", "onlyDown" = "#1E8449")),
                                             na_col = "#FFD700",
                                             annotation_name_side = "bottom",
                                             gap = unit(2, "mm"))
        }
        
        # if (length(index_list) != 0) {
        #         annot_right <- rowAnnotation(genes = anno_mark(at = annot_genes$index,
        #                                                        labels = annot_genes$symbol))
        #         annot_left <- rowAnnotation("GR binding" = TF_mat_GR[, 3],
        #                                     annotation_width = unit(45, "cm"),
        #                                     col = list("GR binding" = c("TRUE" = "#22427C", "FALSE" = "#EFEFEF")),
        #                                     annotation_name_side = "top")
        #         
        # }
        
        # genes = anno_mark(at = annot_genes$index,
        #                   labels = annot_genes$symbol),
        
        ##### Set color scale
        # col_fun = colorRamp2(c(-2, -1, 0, 1, 2), c("#1E8449", "#1E8449", "black", "#800020", "#800020"))
        col_fun = colorRamp2(c(-4, -1.5, 0, 1.5, 4), c("#1E8449", "#1E8449", "black", "#800020", "#800020"))
        
        ##### Generate heatmap
        FC_heatmap_withoutGR <- Heatmap(FCmat_withoutGR, name = "log2(FC)",
                                     right_annotation = annot_right,
                                     # left_annotation = annot_left,
                                     col = col_fun, na_col = "white",
                                     show_row_names = FALSE,
                                     column_names_side = "top", column_names_rot = 45,
                                     row_dend_side = "left", row_dend_width = unit(4, "cm"),
                                     column_order = colnames(FCmat_withoutGR),
                                     # column_title = paste0("DEGs between ", timepoint, " (without GR binding) : ", nb_deg_withoutGR, " genes\n",
                                     #                       # nb_upreg, " upregulated genes, ", nb_downreg, " downregulated genes\n",
                                     #                       "log2(FC) = ", log, " | ", "FDR = ", fdr),
                                     width = unit(7, "cm"),
                                     show_row_dend = TRUE
                                     
                                     # rect_gp = gpar(col = "white", lwd = 0),
        )
        FC_heatmap_withoutGR
        
        # saveHeatmap(FC_heatmap_withoutGR, output_dir = "output/analyses/ecosystem/gene_position",
        #             output_file = paste(today, paste0(heatmap_output_name, "_withoutGR"), sep = "_"),
        #             width_val = 17, height_val = 9, format = "pdf")
        
        
        # ##### Boxplot of FC depending on time
        # 
        FC_TAD <- FC_TAD %>% mutate(direction = set_TAD_direction2(MBC_R),
                                    GR_binding = ifelse((GR_linear + GR_via3D) != 0, TRUE, FALSE))
        FC_TAD$GR_binding <- factor(FC_TAD$GR_binding, levels = c(TRUE, FALSE))
        FC_TAD$direction <- factor(FC_TAD$direction, levels = c("TAD_onlyDOWN", "TAD_DOWN",
                                                                "TAD_UNBIASED",
                                                                "TAD_UP", "TAD_onlyUP",
                                                                "TAD_NB"))
        
        justFC <- FC_TAD %>% dplyr::select(gene_id, "0.5h", "1h", "2h", "3h", "4h", "5h", "6h", "7h", "8h", "10h", "12h")
        colnames(justFC) <- c("gene_id", paste0("FC", colnames(justFC)[-1]))
        
        # gather(justFC, "FC1h", "FC2h", -gene_id)
        
        test <- gather(justFC, timepoint, FC, -gene_id)
        test2 <- left_join(test, FC_TAD %>% dplyr::select(-c(gene_names)), by = "gene_id")
        test2$timepoint <- gsub("FC", "", test2$timepoint)
        test2$timepoint <- factor(test2$timepoint, levels = c("0.5h", "1h", "2h", "3h", "4h", "5h", "6h",
                                                              "7h", "8h", "10h", "12h"))
        # test2$timepoint <- gsub("h", "", test2$timepoint)
        # test2$timepoint <- as.numeric(test2$timepoint)
        
        title <- paste("Threshold =", gsub("th", "", th), "RPM", paste0("\n", nb_genes_inFC), "genes")
        
        FCboxplot_perTADstatus <- test2 %>% ggplot(aes(x = timepoint, y = FC, fill = GR_binding)) +
                geom_boxplot(outlier.shape = NA) +
                geom_point(aes(x = timepoint, color = GR_binding), size = 0.4, alpha = 0.5,
                           position = position_jitterdodge(jitter.width=0.45)) +
                facet_grid(direction ~ .) +
                theme_minimal() +
                scale_fill_manual(values = c("#F5A623", "#4A90E2", "#008000")) +
                scale_color_manual(values = c("#4A90E2", "#F5A623")) +
                theme(axis.text.x = element_text(size = 11, face = "bold"),
                      strip.text.y = element_text(size = 11, face = "bold", angle = 0, hjust = 0),
                      strip.background = element_rect(colour = NA)) +
                ggtitle(title) + theme(plot.title = element_text(size = 13, face = "bold"))
        # FCboxplot_perTADstatus
        
        # saveHeatmap(FCboxplot_perTADstatus, output_dir = "output/analyses/ecosystem/gene_position",
        #             output_file = paste(today, "FCBoxplot", th, "perTAD_perTime", sep = "_"),
        #             width_val = 9, height_val = 11, format = "pdf")
}


# 97 DEG / 134 DEGs are in a TADs

# Gather linear and 3D
# TF_df_GR <- TF_df %>% dplyr::select("GR_linear", "GR_via3D", FC1h) %>%
#         dplyr::mutate(GR = ifelse((GR_linear + GR_via3D) != 0, TRUE, FALSE))
# rownames(TF_df_GR) <- TF_df$gene_id
# TF_mat_GR <- TF_df_GR %>% dplyr::select("GR_linear", "GR_via3D", "GR") %>% as.matrix

# Hom many genes are binded by GR: 143 / 228 = 63%
# sum(TF_mat_GR[, 3] == TRUE)
# length(TF_mat_GR[, 3])

#################
# Genes UP without GR binding
# genes_without_GR <- list()
# up_without_GR <- TF_df_GR %>% mutate(ensg = rownames(TF_df_GR)) %>%
#         dplyr::filter(GR == FALSE, FC1h > 0) %>% dplyr::select(ensg, GR, FC1h) %>% arrange(desc(FC1h))
# 
# down_without_GR <- TF_df_GR %>% mutate(ensg = rownames(TF_df_GR)) %>%
#         dplyr::filter(GR == FALSE, FC1h < 0) %>% dplyr::select(ensg, GR, FC1h) %>% arrange(desc(FC1h))
# 
# genes_without_GR$up <- up_without_GR
# genes_without_GR$down <- down_without_GR
# saveRDS(genes_without_GR, file = "output/analyses/ecosystem/gene_position/geneWithoutGR.rds")
# 
# # Genes Down with MBC_UP
# up_with_MBCup <- TF_df %>% dplyr::filter(gene_id %in% upreg) %>% 
#         dplyr::filter(MBC_UP_linear == TRUE | MBC_UP_via3D == TRUE)
# # 59/89 > 66.29%
# 
# up_with_MBCdown <- TF_df %>% dplyr::filter(gene_id %in% upreg) %>% 
#         dplyr::filter(MBC_DOWN_linear == TRUE | MBC_DOWN_via3D == TRUE)
# # 7/89 > 7.86%
# 
# down_with_MBCup <- TF_df %>% dplyr::filter(gene_id %in% downreg) %>% 
#         dplyr::filter(MBC_UP_linear == TRUE | MBC_UP_via3D == TRUE)
# # 5/45 > 11.11%
# 
# down_with_MBCdown <- TF_df %>% dplyr::filter(gene_id %in% downreg) %>% 
#         dplyr::filter(MBC_DOWN_linear == TRUE | MBC_DOWN_via3D == TRUE)
# # 27/45 > 60%

library(viridis)
##################
##### Boxplot of FC depending on GR bound/unbound and TAD status
##################

# FC_val <- FC_TAD %>% dplyr::select(-c(gene_names, "0h")) %>%
#         mutate(direction = set_TAD_direction2(MBC_R),
#                GR_binding = ifelse((GR_linear + GR_via3D) != 0, TRUE, FALSE))
# FC_val$FC1h <- FC_val$'1h'
# FC_val$GR_binding <- factor(FC_val$GR_binding, levels = c(TRUE, FALSE))
# FC_val$direction <- factor(FC_val$direction, levels = c("TAD_onlyDOWN", "TAD_DOWN",
#                                                         "TAD_UNBIASED",
#                                                         "TAD_UP", "TAD_onlyUP",
#                                                         "TAD_NB"))
# 
# FC_val %>% group_by(direction, GR_binding) %>% tally
# table(FC_val$direction)
# 
# FC_val %>% ggplot(aes(x = direction, y = FC1h, fill = GR_binding)) +
#         geom_boxplot(outlier.shape = NA) +
#         geom_point(aes(color = GR_binding), size = 0.4, alpha = 0.5,
#                    position = position_jitterdodge(jitter.width=0.45)) +
#         theme_minimal() +
#         scale_color_manual(values = c("blue", "red"))