# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

library(ComplexHeatmap)
library(circlize)
library(knitr)
library(tidyverse)
source("scripts/ckn_utils.R")

######
saveHeatmap <- function(heatmap, output_dir, output_file, width_val = 25, height_val = 22, format = "pdf") {
        output_filepath <- file.path(output_dir, paste0(output_file, ".", format))
        pdf(file = output_filepath, width = width_val, height = height_val)
        print(heatmap)
        dev.off()
        message(" > Heatmap saved in ", output_filepath)
}

#####
today <- get_today()

##### Load FC values across time course
raw_fc <- read_delim("results/a549_dex_time_points/FC_mat.csv", delim = ",")

##### Load DEG genes
deg <- readRDS(file = "output/analyses/deg0_6_fdr10.rds")
upreg <- deg$gene_list$FC0p5$upreg # 
# upreg <- "ENSG00000167772"
downreg <- deg$gene_list$FC0p5$downreg # 
# downreg <- "ENSG00000095752"
# Retrieve coordinates of all the transcripts of up and downreg
# upreg_ranges <- stdchr_promoters_A549[names(stdchr_promoters_A549) %in% upreg] 
# downreg_ranges <- stdchr_promoters_A549[names(stdchr_promoters_A549) %in% downreg] 

for (i in 1) {
        nb_upreg <- length(upreg)
        nb_downreg <- length(downreg)
        
        # Are those gene in FC matrix?
        sum(upreg %in% raw_fc$gene_id) # 
        sum(downreg %in% raw_fc$gene_id) #
        
        # gather upreg and downreg genes
        genelist_deg <- c(upreg, downreg)
        nb_deg <- length(genelist_deg)
        
        #
        FC <- raw_fc %>% dplyr::filter(gene_id %in% genelist_deg) # %>% dplyr::select(gene_id, "0.5", "1", "2")
        colnames(FC) <- c("gene_id", paste0(colnames(FC)[-1], "h"))
        rownames(FC) <- FC$gene_id
        
        ##### Process binded genes for annotation
        # binded_genes.rds have been generated in script/chris/ecosystem/linear_and_3D_annotation.R
        binded_genes <- readRDS(file = "output/analyses/ecosystem/gene_position/binded_genes.rds")
        
        for (elt in names(binded_genes)[16:19]) {
                message("# ", elt)
                target <- binded_genes[[elt]]
                print(elementNROWS(target))
                
                genes_linear <- target$linear
                genes_via3D <- target$via3D
                
                linear_inTH <- FC$gene_id %in% genes_linear
                nb_linear <- sum(linear_inTH)
                message("\t\t\t# linear : ", nb_linear, " / ", length(genes_linear), "   ", round(nb_linear/length(genes_linear)*100, 2), " %")
                
                via3D_inTH <- FC$gene_id %in% genes_via3D
                nb_via3D <- sum(via3D_inTH)
                message("\t\t\t# via3D : ", nb_via3D, " / ", length(genes_via3D), "   ", round(nb_via3D/length(genes_via3D)*100, 2), " %")
                
                df_tmp <- data.frame(linear_inTH, via3D_inTH)
                colnames(df_tmp) <- paste(elt, c("linear", "via3D"), sep = "_")
                
                FC <- cbind(FC, df_tmp)
        }
        
        cofactors <- c("MBC")
        order_column <- c(
                paste(rep(cofactors, each = 2), "UP", c("linear", "via3D"), sep = "_"),
                paste(rep(cofactors, each = 2), "DOWN", c("linear", "via3D"), sep = "_"),
                paste("GR", c("linear", "via3D"), sep = "_"),
                paste(rep(cofactors, each = 2), "UNBIASED", c("linear", "via3D"), sep = "_")
        )
        #order_column
        
        
        # FCmat <- FC %>% dplyr::select("0.5h", "1h", "2h", "3h", "4h", "5h", "6h", "7h", "8h", "10h", "12h") %>% as.matrix
        FCmat <- FC %>% dplyr::select("0.5h", "1h", "2h", "3h", "4h", "5h", "6h") %>% as.matrix 
        # FCmat <- FC %>% dplyr::select("0.5h", "1h") %>% as.matrix
        
        # rownames(FCmat) <- raw_fc2$gene_id[raw_fc2$gene_id %in% genelist_deg]
        # max(FCmat) # 
        # min(FCmat) #
        
        TFmat <- FC %>% dplyr::select(order_column) %>% as.matrix
        # TF_df_GR <- TF_df %>% dplyr::select("GR_linear", "GR_via3D", FC1h) %>%
        #         dplyr::mutate(GR = ifelse((GR_linear + GR_via3D) != 0, TRUE, FALSE))
        # rownames(TF_df_GR) <- TF_df$gene_id
        # TF_mat_GR <- TF_df_GR %>% dplyr::select("GR_linear", "GR_via3D", "GR") %>% as.matrix
        
        # Hom many genes are binded by GR: 143 / 228 = 63%
        # sum(TF_mat_GR[, 3] == TRUE)
        # length(TF_mat_GR[, 3])
        
        ##### Gene name annotation

        # ensg <- c("ENSG00000167772", "ENSG00000157514", "ENSG00000179094", "ENSG00000171522", "ENSG00000019549", "ENSG00000128016",
        #           "ENSG00000095752", "ENSG00000198517", "ENSG00000143384", "ENSG00000013441")
        # symbol <- c("ANGPTL4", "TSC22D3", "PER1", "PTGER4", "SNAI2", "ZFP36",
        #             "IL11", "MAFK", "MCL1", "CLK1")
        
        ensg <- c("ENSG00000167772", "ENSG00000166592","ENSG00000166589", "ENSG00000095752", "ENSG00000179094",
                  "ENSG00000108342", "ENSG00000185338", "ENSG00000156427")
        symbol <- c("ANGPTL4", "RRAD", "CDH16", "IL11", "PER1",
                    "CSF3", "SOCS1", "FGF18")
        
        index_list <- c()
        symbol_list <- c()
        annot_genes <- list()
        for (i in 1:length(ensg)) {
                id <- ensg[i]
                n <- which(rownames(FCmat) == id)
                if (length(n) != 0) {
                        index_list <- c(index_list, n)
                        symbol_list <- c(symbol_list, symbol[i])
                }
        }
        annot_genes$index <- index_list
        annot_genes$symbol <- symbol_list
        
        ##### Annotation right
        if (length(index_list) != 0) {
                annot_right <- rowAnnotation(genes = anno_mark(at = annot_genes$index,
                                                               labels = annot_genes$symbol),
                                             binding = TFmat, col = list(binding = c("TRUE" = "#22427C", "FALSE" = "#EFEFEF")),
                                             annotation_name_side = "bottom")
        } else {
                annot_right <- rowAnnotation(binding = TFmat, col = list(binding = c("TRUE" = "#22427C", "FALSE" = "#EFEFEF")),
                                             annotation_name_side = "bottom")
        }
        
        # if (length(index_list) != 0) {
        #         annot_right <- rowAnnotation(genes = anno_mark(at = annot_genes$index,
        #                                                        labels = annot_genes$symbol))
        #         annot_left <- rowAnnotation("GR binding" = TF_mat_GR[, 3],
        #                                     annotation_width = unit(45, "cm"),
        #                                     col = list("GR binding" = c("TRUE" = "#22427C", "FALSE" = "#EFEFEF")),
        #                                     annotation_name_side = "top")
        #         
        # }
        
        # genes = anno_mark(at = annot_genes$index,
        #                   labels = annot_genes$symbol),
        
        ##### Set color scale
        # col_fun = colorRamp2(c(-2, -1, 0, 1, 2), c("#1E8449", "#1E8449", "black", "#800020", "#800020"))
        col_fun = colorRamp2(c(-4, -1.5, 0, 1.5, 4), c("#1E8449", "#1E8449", "black", "#800020", "#800020"))
        
        ##### Generate heatmap
        FC_heatmap <- Heatmap(FCmat, name = "log2(FC)",
                              right_annotation = annot_right,
                              # left_annotation = annot_left,
                              col = col_fun, na_col = "white",
                              show_row_names = FALSE,
                              column_names_side = "top", column_names_rot = 45,
                              row_dend_side = "left", row_dend_width = unit(4, "cm"),
                              column_order = colnames(FCmat),
                              column_title = paste(nb_deg, "genes"),
                              width = unit(7, "cm"),
                              show_row_dend = TRUE,
                              
                              # rect_gp = gpar(col = "white", lwd = 0),
        )
        FC_heatmap
        
        saveHeatmap(FC_heatmap, output_dir = "output/analyses/ecosystem/gene_position",
                    output_file = paste(today, "FCheatmap_Annot_0_6h_fdr10", sep = "_"),
                    width_val = 17, height_val = 9, format = "pdf")
}



# Genes UP without GR binding
# genes_without_GR <- list()
# up_without_GR <- TF_df_GR %>% mutate(ensg = rownames(TF_df_GR)) %>%
#         dplyr::filter(GR == FALSE, FC1h > 0) %>% dplyr::select(ensg, GR, FC1h) %>% arrange(desc(FC1h))
# 
# down_without_GR <- TF_df_GR %>% mutate(ensg = rownames(TF_df_GR)) %>%
#         dplyr::filter(GR == FALSE, FC1h < 0) %>% dplyr::select(ensg, GR, FC1h) %>% arrange(desc(FC1h))
# 
# genes_without_GR$up <- up_without_GR
# genes_without_GR$down <- down_without_GR
# saveRDS(genes_without_GR, file = "output/analyses/ecosystem/gene_position/geneWithoutGR.rds")
# 
# # Genes Down with MBC_UP
# up_with_MBCup <- TF_df %>% dplyr::filter(gene_id %in% upreg) %>% 
#         dplyr::filter(MBC_UP_linear == TRUE | MBC_UP_via3D == TRUE)
# # 59/89 > 66.29%
# 
# up_with_MBCdown <- TF_df %>% dplyr::filter(gene_id %in% upreg) %>% 
#         dplyr::filter(MBC_DOWN_linear == TRUE | MBC_DOWN_via3D == TRUE)
# # 7/89 > 7.86%
# 
# down_with_MBCup <- TF_df %>% dplyr::filter(gene_id %in% downreg) %>% 
#         dplyr::filter(MBC_UP_linear == TRUE | MBC_UP_via3D == TRUE)
# # 5/45 > 11.11%
# 
# down_with_MBCdown <- TF_df %>% dplyr::filter(gene_id %in% downreg) %>% 
#         dplyr::filter(MBC_DOWN_linear == TRUE | MBC_DOWN_via3D == TRUE)
# # 27/45 > 60%
