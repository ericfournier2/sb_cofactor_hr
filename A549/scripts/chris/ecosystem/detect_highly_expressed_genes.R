# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

library(tidyverse)
library(knitr)
library(EnsDb.Hsapiens.v86)
library(metagene2)
source("scripts/ckn_utils.R")

# Get today
today <- get_today()

##### 
get_bam_cofactors_filepath <- function(cofactors = c("MED1", "BRD4", "CDK9", "NIPBL", "SMC1A")) {
  bam_filepath_list <- c()
  chip_bam_dir <- "output/chip-pipeline-GRCh38/alignment"
  for (cofactor in cofactors) {
    for (condition in c("CTRL", "DEX")) {
      basename <- paste("A549", condition, cofactor, "rep1", sep = "_")
      bamfilename <- paste0(basename, ".sorted.dup.bam")
      bam_filepath <- file.path(chip_bam_dir, basename, bamfilename)
      bam_filepath_list <- c(bam_filepath_list, bam_filepath)
    }
  }
  return(bam_filepath_list)
}

##### 
get_bam_reddy_filepath <- function(targets = c("JUN", "BCL3", "CEBPB", "CTCF", "FOSL2", "HES2", "JUNB", "RAD21", "EP300", "SMC3", "H3K4me1", "H3K4me2", "H3K4me3", "H3K27ac"),
                                   reps = "123") {
  bam_folder <- "input/ENCODE/A549/GRCh38/chip-seq/bam"
  targets <- gsub("GR", "NR3C1", targets)
  
  bam_filepath_list <- c()
  for (target in targets) {
    bam_pattern <- paste0("^", target, "_([0-9]+.*)_rep([", reps, "])_(.*\\.bam$)")
    bam_pattern <- paste0("^", target, "_([0-1]{1}hour)_rep([", reps, "])_(.*\\.bam$)")
    bam_files <- list.files(path = bam_folder, pattern = bam_pattern, full.names = TRUE)
    bam_filepath_list <- c(bam_filepath_list, bam_files)
  }
  return(bam_filepath_list)
}

##### Make metadata from bam cofactors filepath list
make_metadata_from_bam_cofactors_filepath_list <- function(bam_cofactors_filepath_list) {
  bam_names <- basename(bam_cofactors_filepath_list)
  basename <- gsub(".bam", "", bam_names)
  
  splitted <- strsplit(bam_names, split = "_")
  target <- splitted %>% purrr:::map(3) %>% unlist
  condition <- splitted %>% purrr:::map(2) %>% unlist
  metadata <- data.frame(design = basename, target, condition, stringsAsFactors = FALSE)
  
  return(metadata)
}

##### Make metadata from bam reddy filepath list
make_metadata_from_bam_reddy_filepath_list <- function(bam_reddy_filepath_list) {
  bam_names <- basename(bam_reddy_filepath_list)
  basename <- gsub(".bam", "", bam_names)
  
  splitted <- strsplit(bam_names, split = "_")
  target <- splitted %>% purrr:::map(1) %>% unlist
  target <- gsub("NR3C1", "GR", target)
  
  condition <- splitted %>% purrr:::map(2) %>% unlist
  condition <- gsub("0hour", "CTRL", condition)
  condition <- gsub("1hour", "DEX", condition)
  
  metadata <- data.frame(design = basename, target, condition, stringsAsFactors = FALSE)
  
  return(metadata)
}

#####
plot_metagene_cofactor <- function(df_metagene, customColors = c("#F5A623", "#4A90E2", "#008000", "#8C001A"),
                                   title = "", scale_val = "fixed") {
  metagene_plot <- ggplot(df_metagene, aes(x=bin, y=value, ymin=qinf, ymax=qsup, group=condition)) +
    geom_ribbon(aes(fill = condition), alpha = 0.3) +
    scale_fill_manual(values = customColors) +
    geom_line(aes(color = condition)) + # , size = condition, alpha = condition)) +
    scale_color_manual(values = customColors) +
    theme_classic() +
    theme(strip.text.x = element_text(size = 9, face = "bold"),
          strip.text.y = element_text(size = 9, face = "bold", angle = 0, hjust = 0),
          axis.text = element_text(size = 7),
          strip.background = element_rect(colour = NA)) +
    #scale_x_continuous(breaks = seq(0, 100, 25),
    #                   labels = c("-1.5", "-0.75", "0", "0.75", "1.5")) +
    facet_grid(target ~ region, scales = scale_val) + 
    ggtitle(title) + theme(plot.title = element_text(size = 9, face = "bold")) +
    # xlab("Distance in kb") +
    ylab("RPM")
  return(metagene_plot)
}

#####
saveMetagene <- function(metagene_plot, output_dir, output_file, width_val = 25, height_val = 22) {
  output_filepath <- file.path(output_dir, paste0(output_file, ".pdf"))
  pdf(file = output_filepath, width = width_val, height = height_val)
  print(metagene_plot)
  dev.off()
  message(" > Metagene saved in ", output_filepath)
}

# Load db
genes <- genes(most_expressed_TxDb)

# get SYMBOL
edb <- EnsDb.Hsapiens.v86
gene2symbol <- genes(edb, columns = "gene_name", return.type = "DataFrame") %>% as.data.frame
gene2symbol <- gene2symbol[,2:1]
colnames(gene2symbol) <- c("gene_id", "symbol")

# detect highly expressed genes
rawcounts <- read_tsv("results/a549_dex_time_points/raw_counts_with_colnames.txt")
h0_rawcounts <- rawcounts %>% dplyr::select(starts_with("0h"))
library_size <- h0_rawcounts %>% colSums
rpm <- h0_rawcounts / (library_size / 1000000)
mean_rpm <- data.frame("gene_id" = rawcounts$gene_id, "mean_rpm" = rowSums(rpm) / 4) %>%
  left_join(gene2symbol, by = "gene_id")

highly_expressed_genes_raw <- mean_rpm %>% dplyr::select(gene_id, symbol, mean_rpm) %>%
  arrange(desc(mean_rpm)) %>% dplyr::filter(mean_rpm > 500)
kable(head(highly_expressed_genes_raw, 50))
kable(highly_expressed_genes_raw)

  # remove mitochondrial genes
highly_expressed_genes <- highly_expressed_genes_raw[!(grepl("MT-", highly_expressed_genes_raw$symbol)), ]
  # Which genes are not in most_expressed_db?
highly_expressed_genes[!(highly_expressed_genes$gene_id %in% names(genes)), ]
  # remove genes not present in most_expressed_db: MALAT1, NCL, JAK1, AP2B1, NEAT1, CD24, USP9X, EEF2 (Cofactors binding occurs at these genes, need to be include later)
  # Redo TxDB based on genes detected in RNA-Seq?
highly_expressed_genes <- highly_expressed_genes %>% dplyr::filter(!(symbol %in% c("MALAT1", "NCL", "JAK1", "AP2B1", "NEAT1", "CD24", "USP9X", "EEF2")))
kable(highly_expressed_genes)

##### Load induced and repressed gene categories
deg <- readRDS(file = "output/analyses/deg.rds")
upreg_genelist <- deg$gene_list$FC1$upreg
downreg_genelist <- deg$gene_list$FC1$downreg

#### Are those genes significantly induced or repressed?
highly_expressed_genes[highly_expressed_genes$gene_id %in% upreg_genelist, ]
highly_expressed_genes[highly_expressed_genes$gene_id %in% downreg_genelist, ]

# Get GRanges for those genes
highly_expressed_genes_coord <- genes[highly_expressed_genes$gene_id]
# Save highly_expressed_genes_coord for later use
saveRDS(highly_expressed_genes_coord, file = "output/analyses/ecosystem/highly_expressed_genes.rds")

# Load diffbind
diffbind <- load_diffbind_cofactors_peaks()

# How many cofactors per highly transcribed genes
res <- data.frame("rank" = 1:length(highly_expressed_genes_coord), "gene_id" = highly_expressed_genes_coord$gene_id)
for (set in names(diffbind)) {
  count_diffbind <- countOverlaps(highly_expressed_genes_coord + 4000, diffbind[[set]])
  res <- cbind(res, count_diffbind)
}
colnames(res) <- c("rank", "gene_id", names(diffbind))

res_df <- res %>% left_join(gene2symbol, by = "gene_id") %>% dplyr::select(rank, gene_id, symbol, contains("DOWN"))
kable(res_df)

# Ranking according to number of loss
sum_down <- res_df %>% dplyr::select(-rank, -gene_id, -symbol) %>% rowSums
res_df2 <- data.frame(res_df, "sumDown" = sum_down) %>% arrange(desc(sumDown)) %>% dplyr::filter(sumDown != 0)
kable(head(res_df2, 50))


## Are those genes in induced/repressed groups?
deg <- readRDS("output/analyses/deg.rds")
upreg <- deg$gene_list$FC1$upreg
downreg <- deg$gene_list$FC1$downreg

inUpreg <- highly_expressed_genes$gene_id %in% upreg
sum(inUpreg)
inDownreg <- highly_expressed_genes$gene_id %in% downreg
sum(inDownreg)
res <- data.frame(highly_expressed_genes, inUpreg, inDownreg) %>% dplyr::filter(inUpreg == TRUE | inDownreg == TRUE)

###### Make metagene
##### Define what to draw on metagene, using which bam
# bam_filepath and metadata for cofactors
bam_cofactors_filepath_list <- get_bam_cofactors_filepath(c("MED1", "BRD4", "CDK9", "NIPBL", "SMC1A"))
metadata_cofactors <- make_metadata_from_bam_cofactors_filepath_list(bam_cofactors_filepath_list)
# bam_filepath and metadata for reddy
# bam_filepath_reddy_list <- get_bam_reddy_filepath(c("JUN", "BCL3", "CEBPB", "CTCF", "FOSL2", "HES2", "JUNB", "RAD21", "EP300", "SMC3", "H3K4me1", "H3K4me2", "H3K4me3", "H3K27ac"))
bam_reddy_filepath_list_rep2 <- get_bam_reddy_filepath(targets = c("GR", "JUN"), reps = "2")
bam_reddy_filepath_list_rep1 <- get_bam_reddy_filepath(targets = c("EP300", "FOSL2", "H3K27ac", "POLR2A"), reps = "1")
metadata_reddy_rep2 <- make_metadata_from_bam_reddy_filepath_list(bam_reddy_filepath_list_rep2)
metadata_reddy_rep1 <- make_metadata_from_bam_reddy_filepath_list(bam_reddy_filepath_list_rep1)  
# gather cofactors and reddy
bam_filepath_list <- c(bam_cofactors_filepath_list, bam_reddy_filepath_list_rep2, bam_reddy_filepath_list_rep1)
metadata <- rbind(metadata_cofactors, metadata_reddy_rep2, metadata_reddy_rep1)


for (i in 1:15) {
  gene_id <- res_df2$gene_id[i]
  symbol <- res_df2$symbol[i]
  coord <- highly_expressed_genes_coord[gene_id]
  
  mg <- metagene2$new(regions = coord + 4000,
                      bam_files = bam_filepath_list,
                      normalization = "RPM",
                      force_seqlevels = TRUE,
                      assay = 'chipseq',
                      cores = 4,
                      bin_count = 500)
  mg$add_metadata(design_metadata = metadata)
  df_cofactor <- mg$get_data_frame()
  
  title_df <- paste(paste0("Highly transcribed genes #", res_df2$rank[i]), " | ", symbol, " | rank", i)
  
  df_cofactor$target <- factor(df_cofactor$target, levels = c("MED1", "BRD4", "CDK9", "NIPBL", "SMC1A", "EP300",
                                                              "GR", "POLR2A", "JUN", "FOSL2", "H3K27ac"))
  metagene_plot <- plot_metagene_cofactor(df_cofactor, title = title_df,
                                          customColors = c("#008000", "#8C001A"), scale_val = "free_y")
  
  saveMetagene(metagene_plot,
               output_dir = "output/analyses/ecosystem/highly_transcribed_genes",
               output_file = paste(today, "highly_transcribed", paste0("rank", i), symbol, sep = "_"),
               width_val = 13, height_val = 13)
}



