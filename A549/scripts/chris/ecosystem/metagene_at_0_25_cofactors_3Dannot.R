# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

source("scripts/chris/metagene2_Reddy.utils.R")
source("scripts/ckn_utils.R")

#####
today <- get_today()

##### That second version incluse facet_grid induced vs repressed genes
plot_metagene_Reddy2 <- function(df_metagene, customColors = c("#F5A623", "#4A90E2", "#008000", "#8C001A"),
                                title = "", scale_val = "fixed") {
  metagene_plot <- ggplot(df_metagene, aes(x=bin, y=value, ymin=qinf, ymax=qsup, group=replicate)) +
    geom_ribbon(aes(fill = replicate), alpha = 0.3) +
    scale_fill_manual(values = customColors) +
    geom_line(aes(color = replicate, size = replicate, alpha = replicate)) + 
    scale_color_manual(values = customColors) +
    scale_size_manual(values = c(0.3, 0.3, 1)) +
    scale_alpha_manual(values = c(0.6, 0.6, 1)) +
    theme_classic() +
    theme(axis.title.x = element_blank(),
          axis.text.x = element_blank(),
          axis.ticks.x = element_blank()) +
    theme(strip.text.x = element_text(size = 15, face = "bold"),
          strip.text.y = element_text(size = 15, face = "bold", angle = 0, hjust = 0),
          strip.background = element_rect(colour = NA)) +
    ggtitle(title) + theme(plot.title = element_text(size = 20, face = "bold")) +
    xlab("Distance in bins") +
    ylab("RPM") +
    facet_grid(target*region_name ~ timepoint, scales = scale_val)
  return(metagene_plot)
}

##### Load cofactors 3D
enhRDS <- readRDS("output/analyses/ecosystem/metagene_cofactors_3DAnnotation_without_unbiased/regions_cofactors_3D_annotations.rds")

##### Load Reddy ChIP
# targets <- c("EP300", "JUN", "FOSL2", "H3K27ac",
#              "BCL3", "CEBPB", "CTCF", "HES2", "JUNB", "RAD21",
#              "SMC3", "H3K4me1", "H3K4me2", "H3K4me3")
# targets <- c("EP300", "JUN", "FOSL2", "H3K27ac")
targets <- c("EP300")

chip_Reddy_raw <- list()
for (target in targets) {
  message("##### ", target)
  all_chip_raw <- load_reddy_binding_consensus(target)
  all_chip <- all_chip_raw[grep("^[01]\\shour", names(all_chip_raw))]
  names(all_chip) <- gsub("0 hour", paste(target, "CTRL", sep = "_"), names(all_chip))
  names(all_chip) <- gsub("1 hour", paste(target, "DEX", sep = "_"), names(all_chip))
  chip_Reddy_raw <- c(chip_Reddy_raw, all_chip)
}

chip_Reddy_stchr <- sapply(chip_Reddy_raw, keepStdChr) 
chip_Reddy <- GRangesList(chip_Reddy_stchr)
names(chip_Reddy)

##### Load Meyer POLR2A ChIP
chip_pol2_raw <- load_POLR2A_peaks_Myers()
chip_pol2_stdchr <- sapply(chip_pol2_raw, keepStdChr) 
chip_pol2 <- GRangesList(chip_pol2_stdchr)
sapply(chip_pol2, length)

##### Gather all peaks
stdchr <- append(stdchr, c(chip_Reddy, chip_pol2))
sapply(stdchr, length)

##### Load GR binding sites
all_gr_regions <- load_reddy_gr_binding_consensus()
gr_regions_list <- GRangesList(c(all_gr_regions[grep("minutes", names(all_gr_regions))], "1 hour" = all_gr_regions[["1 hour"]]))
gr_regions <- GenomicRanges::reduce(unlist(gr_regions_list))

##### Load induced and repressed gene categories
deg <- readRDS(file = "output/analyses/deg.rds")
upreg <- deg$gene_list$FC1$upreg
downreg <- deg$gene_list$FC1$downreg
# responsive_genes <- c(upreg, downreg) %>% unique

##### Define cofactors (on the metagene, which regions you want to evaluate)
# cofactors <- c("MED1", "BRD4", "CDK9", "NIPBL", "SMC1A", targets)
cofactors <- c("BRD4")
# cofactors <- c("GR", "POLR2A", "MED1", "BRD4", "EP300", "CDK9",
#                "NIPBL", "SMC1A",
#                "JUN", "FOSL2", "H3K27ac")

###### Main analysis
regions <- list()
for (cofactor in cofactors) {
  message("##### ", cofactor)
  
  if (cofactor == "GR") {
    all_peaks <- gr_regions %>% keepStdChr
    message("    # Total number of non-overlapping GR : ", length(all_peaks))
  } else {
    regionSets <- enhRDS[[cofactor]]
    print(sapply(regionSets, length))
    
    regionSets_withGR <- sapply(regionSets, subsetByOverlaps, gr_regions)
    print(sapply(regionSets_withGR, length))
  }
  
  coordinate_regions_raw <- GRangesList("Gain" = GRanges(regionSets_withGR[["up"]]),
                                        "Loss" = GRanges(regionSets_withGR[["down"]]))
  coordinate_regions <- lapply(coordinate_regions_raw, resize, width = 3000, fix = "center")
  
  # metagene2
  message("   Process metagene...")
  df_metagene <- make_df_metagene_Reddy(chip_target = c("GR"), peaks = coordinate_regions, merge_replicates = TRUE, reps = "12")
  title_df <- paste0("Centered at ", cofactor, " differential binding sites\n",
                     "Gain : ", length(coordinate_regions_raw[["Gain"]]), " | Loss : ", length(coordinate_regions_raw[["Loss"]]))
  
  print(title_df)
  metagene_plot <- plot_metagene_Reddy2(df_metagene, customColors = c("#F5A623", "#4A90E2", "#8C001A"), title = title_df, scale_val = "fixed")
  saveMetagene(metagene_plot,
               output_dir = "output/analyses/ecosystem/metagene_cofactors_0-25m_cofactors_3Dannot",
               output_file = paste(today, "metagene_3Dannot", cofactor, "0-25m", sep = "_"),
               width = 23, height = 9)
}
