# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

library(GenomicInteractions)
source("scripts/ckn_utils.R")

# NB: percentage are FALSE

##### What's the day today?
today <- get_today()

##### Function to parse Hits object obtain from findOverlaps
get_overlapping_features <- function(anchor, feature, prefix) {
  if (prefix != FALSE) {
    names(feature) <- paste(prefix, 1:length(feature), sep = "_")
  }
  ov <- findOverlaps(anchor, feature)
  query <- queryHits(ov)
  subject <- subjectHits(ov)
  res <- vector("list", length(anchor))
  
  for (i in 1:length(ov)) {
    res[[query[i]]] <- c(res[[query[i]]], names(feature)[subject[i]])
  }
  return(res)
}

### Load GR binding sites
all_gr_regions <- load_reddy_gr_binding_consensus()
gr_regions <- GRangesList(c(all_gr_regions[grep("minutes", names(all_gr_regions))], "1 hour" = all_gr_regions[["1 hour"]]))
gr_regions <- unlist(gr_regions)

##### Load diffbind cofactors
diffbind <- load_diffbind_cofactors_peaks()
brd4_sets <- diffbind[grep("BRD4", names(diffbind))]
sapply(brd4_sets, length)
med1_sets <- diffbind[grep("MED1", names(diffbind))]
sapply(med1_sets, length)

##### Overlaps with GR
brd4_sets_GR <- sapply(brd4_sets, subsetByOverlaps, gr_regions)
sapply(brd4_sets_GR, length)
med1_sets_GR <- sapply(med1_sets, subsetByOverlaps, gr_regions)
sapply(med1_sets_GR, length)

### Load reference genome
raw_promoters_A549 <- promoters(most_expressed_TxDb, columns = "gene_id", upstream = 5000, downstream = 5000)
stdchr_promoters_A549 <- keepStdChr(raw_promoters_A549)
names(stdchr_promoters_A549) <- stdchr_promoters_A549$gene_id

##### Load induced and repressed gene categories
deg <- readRDS(file = "output/analyses/deg.rds")
upreg_genelist <- deg$gene_list$FC1$upreg
downreg_genelist <- deg$gene_list$FC1$downreg

##### Load enhancers and super_enhancers
enh_se <- readRDS(file = "output/analyses/ecosystem/enhancers/enh_se.rds")
enh <- enh_se[["ENH"]]
se <- enh_se[["SE"]]

### Regions sets
brd4_up <- brd4_sets_GR[["BRD4_UP"]]
brd4_unbiased <- brd4_sets_GR[["BRD4_UNBIASED"]]
brd4_down <- brd4_sets_GR[["BRD4_DOWN"]]
med1_up <- med1_sets_GR[["MED1_UP"]]
med1_unbiased <- med1_sets_GR[["MED1_UNBIASED"]]
med1_down <- med1_sets_GR[["MED1_DOWN"]]
upreg <- stdchr_promoters_A549[names(stdchr_promoters_A549) %in% upreg_genelist]
downreg <- stdchr_promoters_A549[names(stdchr_promoters_A549) %in% downreg_genelist]

##### Load highly expressed genes
highly_expressed_genes <- readRDS("output/analyses/ecosystem/highly_expressed_genes.rds")

## Load Hi-C data 1 hour
hic_1h <- readRDS("output/analyses/annotate_peaks_with_hic/hic_1h_GIObject.rds")
hic_1h_df1_raw <- as.data.frame(hic_1h)[, c(1:3, 6:8)]
hic_1h_df2_raw <- as.data.frame(hic_1h)[, c(6:8, 1:3)]
# colnames(hic_1h_df2_raw) <- c("seqnames2", "start2", "end2", "seqnames1", "start1", "end1")
hic_1h_df <- rbind(hic_1h_df1_raw, hic_1h_df2_raw) # df to put all the results

##### annotate
annotateCI_with_multipleFeatures <- function(hic_df, source, targets, name_source, names_targets) {
  # Transform to tibble
  hic_df <- as_tibble(hic_df)
  
  # Determine anchors
  hic_df_tmp <- hic_df
  colnames(hic_df_tmp) <- c("seqnames", "start", "end", "seqnames", "start", "end")
  anchor1 <- GRanges(hic_df_tmp[, 1:3])
  anchor2 <- GRanges(hic_df_tmp[, 4:6])
  
  # Source
  have_source_df <- countOverlaps(anchor1, source) != 0
  source_df <- get_overlapping_features(anchor1, source, prefix = name_source)
  hic_df <- cbind(hic_df, have_source_df, tibble(source_df))
  colnames(hic_df) <- c("seqnames1", "start1", "end1", "seqnames2", "start2", "end2", paste("have", name_source, 1, sep = "."), paste(name_source, 1, sep = "."))
  
  # Target
  for (i in 1:length(targets)) {
    target_regions <- targets[[i]]
    name_target <- names_targets[i]
    if (name_target %in% c("UPREG", "DOWNREG")) {
      name_target = FALSE
    }
    target_df <- get_overlapping_features(anchor2, target_regions, prefix = name_target)
    hic_df <- cbind(hic_df, tibble(target_df))
  }
  
  colnames(hic_df) <- c(colnames(hic_df)[1:(ncol(hic_df)-length(targets))], paste(names_targets, 2, sep = "."))
  
  # Results
  return(hic_df)
}

############ Associate chromatin interaction and strengh (as counts number)
strength_CI <- function(feature, counts_CI) {
  res <- list()
  message(sum(lengths(feature) != 0))
  individual_strength <- lengths(feature) * counts_CI
  res[["distrib"]] <- individual_strength[individual_strength != 0]
  res[["sum"]] <- sum(individual_strength[individual_strength != 0])
  res[["mean_strength"]] <- mean(individual_strength[individual_strength != 0])
  return(res)
}

################# analyse
analyse_CI <- function(CI_df, counts_CI) {
  column_names <- colnames(CI_df)
  true_source <- CI_df[CI_df[, 7] == TRUE, ]
  true_counts <- counts_CI[CI_df[, 7] == TRUE]
  message(nrow(true_source))
  message(true_source[[column_names[8]]] %>% unlist %>% unique %>% length)
  
  vals_vecs <- c()
  strength_vecs <- c()
  distrib_vects <- list()
  for (n in column_names[9:16]) {
    all_vals <- true_source[[n]] %>% unlist %>% unique %>% length
    nb_all_vals <- true_source[[n]] %>% unlist %>% length
    vals_vecs <- c(vals_vecs, all_vals)
    
    strength <- strength_CI(true_source[[n]], true_counts)
    strength_vecs <- c(strength_vecs, strength[["sum"]])
    
    message("##### ", n, " : ", " | ", all_vals, " | ", nb_all_vals, " | ", strength[["sum"]], " | ", round(strength[["mean_strength"]], 2))
    distrib_vects[[n]] <- strength[["distrib"]]
  }
  
  # tot_val <- sum(vals_vecs)
  # percent_val <- round(vals_vecs/tot_val*100, 2)
  # # print(percent_val)
  # 
  # tot_strength <- sum(strength_vecs)
  # percent_strength <- round(strength_vecs/tot_strength*100, 2)
  # # print(percent_strength)
  return(distrib_vects)
}

####### Interactions strength
counts_CI_1h <- c(hic_1h$counts, hic_1h$counts)

##########
CI_brd4_up_1h <- annotateCI_with_multipleFeatures(hic_1h_df, source = brd4_up,
                                         targets = list(brd4_up, brd4_down, brd4_unbiased, upreg, downreg, highly_expressed_genes, enh, se),
                                         name_source = "BRD4_UP",
                                         names_target = c("BRD4_UP", "BRD4_DOWN", "BRD4_UNBIASED", "UPREG", "DOWNREG", "HIGHLY_EXPRESSED_GENES", "ENHANCERS", "SUPER-ENHANCERS"))

CI_brd4_down_1h <- annotateCI_with_multipleFeatures(hic_1h_df, source = brd4_down,
                                               targets = list(brd4_down, brd4_up, brd4_unbiased, upreg, downreg, highly_expressed_genes, enh, se),
                                               name_source = "BRD4_DOWN",
                                               names_target = c("BRD4_UP", "BRD4_DOWN", "BRD4_UNBIASED", "UPREG", "DOWNREG", "HIGHLY_EXPRESSED_GENES", "ENHANCERS", "SUPER-ENHANCERS"))

CI_brd4_unbiased_1h <- annotateCI_with_multipleFeatures(hic_1h_df, source = brd4_unbiased,
                                                 targets = list(brd4_unbiased, brd4_up, brd4_down, upreg, downreg, highly_expressed_genes, enh, se),
                                                 name_source = "BRD4_UNBIASED",
                                                 names_target = c("BRD4_UP", "BRD4_DOWN", "BRD4_UNBIASED", "UPREG", "DOWNREG", "HIGHLY_EXPRESSED_GENES", "ENHANCERS", "SUPER-ENHANCERS"))

CI_med1_up_1h <- annotateCI_with_multipleFeatures(hic_1h_df, source = med1_up,
                                               targets = list(med1_up, med1_down, med1_unbiased, upreg, downreg, highly_expressed_genes, enh, se),
                                               name_source = "MED1_UP",
                                               names_target = c("MED1_UP", "MED1_DOWN", "MED1_UNBIASED", "UPREG", "DOWNREG", "HIGHLY_EXPRESSED_GENES", "ENHANCERS", "SUPER-ENHANCERS"))

CI_med1_down_1h <- annotateCI_with_multipleFeatures(hic_1h_df, source = med1_down,
                                                 targets = list(med1_up, med1_down, med1_unbiased, upreg, downreg, highly_expressed_genes, enh, se),
                                                 name_source = "MED1_DOWN",
                                                 names_target = c("MED1_UP", "MED1_DOWN", "MED1_UNBIASED", "UPREG", "DOWNREG", "HIGHLY_EXPRESSED_GENES", "ENHANCERS", "SUPER-ENHANCERS"))

CI_med1_unbiased_1h <- annotateCI_with_multipleFeatures(hic_1h_df, source = med1_unbiased,
                                                     targets = list(med1_up, med1_down, med1_unbiased, upreg, downreg, highly_expressed_genes, enh, se),
                                                     name_source = "MED1_UNBIASED",
                                                     names_target = c("MED1_UP", "MED1_DOWN", "MED1_UNBIASED", "UPREG", "DOWNREG", "HIGHLY_EXPRESSED_GENES", "ENHANCERS", "SUPER-ENHANCERS"))

distrib_brd4_up_1h <- analyse_CI(CI_brd4_up_1h, counts_CI_1h)
distrib_brd4_down_1h <- analyse_CI(CI_brd4_down_1h, counts_CI_1h)
distrib_brd4_unbiased_1h <- analyse_CI(CI_brd4_unbiased_1h, counts_CI_1h)
distrib_med1_up_1h <- analyse_CI(CI_med1_up_1h, counts_CI_1h)
distrib_med1_down_up_1h <- analyse_CI(CI_med1_down_1h, counts_CI_1h)
distrib_med1_unbiased_up_1h <- analyse_CI(CI_med1_unbiased_1h, counts_CI_1h)

# ###### At 0h
# ### Load Hi-C data 0 hour
hic_0h <- readRDS(file = "output/analyses/annotate_peaks_with_hic/hic_0h_GIObject.rds")
hic_0h_df1_raw <- as.data.frame(hic_0h)[, c(1:3, 6:8)]
hic_0h_df2_raw <- as.data.frame(hic_0h)[, c(6:8, 1:3)]
# colnames(hic_0h_df2_raw) <- c("seqnames2", "start2", "end2", "seqnames1", "start1", "end1")
hic_0h_df <- rbind(hic_0h_df1_raw, hic_0h_df2_raw) # df to put all the results
counts_CI_0h <- c(hic_0h$counts, hic_0h$counts)

##########
CI_brd4_up_0h <- annotateCI_with_multipleFeatures(hic_0h_df, source = brd4_up,
                                               targets = list(brd4_up, brd4_down, brd4_unbiased, upreg, downreg, highly_expressed_genes, enh, se),
                                               name_source = "BRD4_UP",
                                               names_target = c("BRD4_UP", "BRD4_DOWN", "BRD4_UNBIASED", "UPREG", "DOWNREG", "HIGHLY_EXPRESSED_GENES", "ENHANCERS", "SUPER-ENHANCERS"))

CI_brd4_down_0h <- annotateCI_with_multipleFeatures(hic_0h_df, source = brd4_down,
                                                 targets = list(brd4_up, brd4_down, brd4_unbiased, upreg, downreg, highly_expressed_genes, enh, se),
                                                 name_source = "BRD4_DOWN",
                                                 names_target = c("BRD4_UP", "BRD4_DOWN", "BRD4_UNBIASED", "UPREG", "DOWNREG", "HIGHLY_EXPRESSED_GENES", "ENHANCERS", "SUPER-ENHANCERS"))

CI_brd4_unbiased_0h <- annotateCI_with_multipleFeatures(hic_0h_df, source = brd4_unbiased,
                                                     targets = list(brd4_up, brd4_down, brd4_unbiased, upreg, downreg, highly_expressed_genes, enh, se),
                                                     name_source = "BRD4_UNBIASED",
                                                     names_target = c("BRD4_UP", "BRD4_DOWN", "BRD4_UNBIASED", "UPREG", "DOWNREG", "HIGHLY_EXPRESSED_GENES", "ENHANCERS", "SUPER-ENHANCERS"))

CI_med1_up_0h <- annotateCI_with_multipleFeatures(hic_0h_df, source = med1_up,
                                               targets = list(med1_up, med1_down, med1_unbiased, upreg, downreg, highly_expressed_genes, enh, se),
                                               name_source = "MED1_UP",
                                               names_target = c("MED1_UP", "MED1_DOWN", "MED1_UNBIASED", "UPREG", "DOWNREG", "HIGHLY_EXPRESSED_GENES", "ENHANCERS", "SUPER-ENHANCERS"))

CI_med1_down_0h <- annotateCI_with_multipleFeatures(hic_0h_df, source = med1_down,
                                                 targets = list(med1_up, med1_down, med1_unbiased, upreg, downreg, highly_expressed_genes, enh, se),
                                                 name_source = "MED1_DOWN",
                                                 names_target = c("MED1_UP", "MED1_DOWN", "MED1_UNBIASED", "UPREG", "DOWNREG", "HIGHLY_EXPRESSED_GENES", "ENHANCERS", "SUPER-ENHANCERS"))

CI_med1_unbiased_0h <- annotateCI_with_multipleFeatures(hic_0h_df, source = med1_unbiased,
                                                     targets = list(med1_up, med1_down, med1_unbiased, upreg, downreg, highly_expressed_genes, enh, se),
                                                     name_source = "MED1_UNBIASED",
                                                     names_target = c("MED1_UP", "MED1_DOWN", "MED1_UNBIASED", "UPREG", "DOWNREG", "HIGHLY_EXPRESSED_GENES", "ENHANCERS", "SUPER-ENHANCERS"))

distrib_brd4_up_0h <- analyse_CI(CI_brd4_up_0h, counts_CI_0h)
distrib_brd4_down_0h <- analyse_CI(CI_brd4_down_0h, counts_CI_0h)
distrib_brd4_unbiased_0h <- analyse_CI(CI_brd4_unbiased_0h, counts_CI_0h)
distrib_med1_up_0h <- analyse_CI(CI_med1_up_0h, counts_CI_0h)
distrib_med1_down_up_0h <- analyse_CI(CI_med1_down_0h, counts_CI_0h)
distrib_med1_unbiased_up_0h <- analyse_CI(CI_med1_unbiased_0h, counts_CI_0h)

###############
# Comparing distribution of strength of chromatin interactions

t.test(distrib_brd4_down_0h$`SUPER-ENHANCERS.2`, distrib_brd4_down_1h$`SUPER-ENHANCERS.2`)

df <- data.frame(
  name = c(rep("brd4_down_0h", length(distrib_brd4_down_0h$`SUPER-ENHANCERS.2`)),
           rep("brd4_down_1h", length(distrib_brd4_down_1h$`SUPER-ENHANCERS.2`))),
  value = c(distrib_brd4_down_0h$`SUPER-ENHANCERS.2`, distrib_brd4_down_1h$`SUPER-ENHANCERS.2`))

bar <- data.frame(acol = c(1, 1, 2, 2), bcol = c(699, 700, 700, 699))

pp <- ggplot(df, aes(x = name, y = value)) +
  geom_boxplot() +
  # geom_jitter(color="black", size=0.4, alpha=0.9) +
  ggtitle("Interactions of BRD4_DOWN with super-enhancers\nbetween 0 and 1 hour") +
  xlab("")

pp + geom_line(data = bar, aes(x = acol, y = bcol)) +
  annotate("text", x = 1.5, y = 740, label = "p < 0.001", size = 3)
