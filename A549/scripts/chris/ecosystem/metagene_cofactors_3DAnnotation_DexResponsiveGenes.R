# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

library(GenomicInteractions)
library(metagene2)
library(GenomicRanges)
source("scripts/ckn_utils.R")

##### What's the day today?
today <- get_today()

##### 
get_bam_cofactors_filepath <- function(cofactors = c("MED1", "BRD4", "CDK9", "NIPBL", "SMC1A")) {
  bam_filepath_list <- c()
  chip_bam_dir <- "output/chip-pipeline-GRCh38/alignment"
  for (cofactor in cofactors) {
    for (condition in c("CTRL", "DEX")) {
      basename <- paste("A549", condition, cofactor, "rep1", sep = "_")
      bamfilename <- paste0(basename, ".sorted.dup.bam")
      bam_filepath <- file.path(chip_bam_dir, basename, bamfilename)
      bam_filepath_list <- c(bam_filepath_list, bam_filepath)
    }
  }
  return(bam_filepath_list)
}

##### 
get_bam_reddy_filepath <- function(targets = c("JUN", "BCL3", "CEBPB", "CTCF", "FOSL2", "HES2", "JUNB", "RAD21", "EP300", "SMC3", "H3K4me1", "H3K4me2", "H3K4me3", "H3K27ac"),
                                   reps = "123") {
  bam_folder <- "input/ENCODE/A549/GRCh38/chip-seq/bam"
  targets <- gsub("GR", "NR3C1", targets)
  
  bam_filepath_list <- c()
  for (target in targets) {
    bam_pattern <- paste0("^", target, "_([0-9]+.*)_rep([", reps, "])_(.*\\.bam$)")
    bam_pattern <- paste0("^", target, "_([0-1]{1}hour)_rep([", reps, "])_(.*\\.bam$)")
    bam_files <- list.files(path = bam_folder, pattern = bam_pattern, full.names = TRUE)
    bam_filepath_list <- c(bam_filepath_list, bam_files)
  }
  return(bam_filepath_list)
}

##### Make metadata from bam cofactors filepath list
make_metadata_from_bam_cofactors_filepath_list <- function(bam_cofactors_filepath_list) {
  bam_names <- basename(bam_cofactors_filepath_list)
  basename <- gsub(".bam", "", bam_names)
  
  splitted <- strsplit(bam_names, split = "_")
  target <- splitted %>% purrr:::map(3) %>% unlist
  condition <- splitted %>% purrr:::map(2) %>% unlist
  metadata <- data.frame(design = basename, target, condition, stringsAsFactors = FALSE)
  
  return(metadata)
}

##### Make metadata from bam reddy filepath list
make_metadata_from_bam_reddy_filepath_list <- function(bam_reddy_filepath_list) {
  bam_names <- basename(bam_reddy_filepath_list)
  basename <- gsub(".bam", "", bam_names)
  
  splitted <- strsplit(bam_names, split = "_")
  target <- splitted %>% purrr:::map(1) %>% unlist
  target <- gsub("NR3C1", "GR", target)
  
  condition <- splitted %>% purrr:::map(2) %>% unlist
  condition <- gsub("0hour", "CTRL", condition)
  condition <- gsub("1hour", "DEX", condition)
  
  metadata <- data.frame(design = basename, target, condition, stringsAsFactors = FALSE)
  
  return(metadata)
}

#####
plot_metagene_cofactor <- function(df_metagene, customColors = c("#F5A623", "#4A90E2", "#008000", "#8C001A"),
                                   title = "", scale_val = "fixed") {
  metagene_plot <- ggplot(df_metagene, aes(x=bin, y=value, ymin=qinf, ymax=qsup, group=condition)) +
    geom_ribbon(aes(fill = condition), alpha = 0.3) +
    scale_fill_manual(values = customColors) +
    geom_line(aes(color = condition)) + # , size = condition, alpha = condition)) +
    scale_color_manual(values = customColors) +
    theme_classic() +
    theme(strip.text.x = element_text(size = 9, face = "bold"),
          strip.text.y = element_text(size = 9, face = "bold", angle = 0, hjust = 0),
          axis.text = element_text(size = 7),
          strip.background = element_rect(colour = NA)) +
    scale_x_continuous(breaks = seq(0, 100, 25),
                       labels = c("-1.5", "-0.75", "0", "0.75", "1.5")) +
    facet_grid(target ~ region, scales = scale_val) + 
    ggtitle(title) + theme(plot.title = element_text(size = 9, face = "bold")) +
    xlab("Distance in kb") +
    ylab("RPM")
  return(metagene_plot)
}

#####
saveMetagene <- function(metagene_plot, output_dir, output_file, width_val = 25, height_val = 22) {
  output_filepath <- file.path(output_dir, paste0(output_file, ".pdf"))
  pdf(file = output_filepath, width = width_val, height = height_val)
  print(metagene_plot)
  dev.off()
  message(" > Metagene saved in ", output_filepath)
}

##### Load cofactors peaks
stdchr <- load_cofactor_stdchr_peaks()
sapply(stdchr, length)

# #### Load Reddy ChIP
# # targets <- c("EP300", "JUN", "FOSL2", "H3K27ac",
# #              "BCL3", "CEBPB", "CTCF", "HES2", "JUNB", "RAD21",
# #              "SMC3", "H3K4me1", "H3K4me2", "H3K4me3")
# targets <- c("EP300", "JUN", "FOSL2", "H3K27ac")
# 
# chip_Reddy_raw <- list()
# for (target in targets) {
#   message("##### ", target)
#   all_chip_raw <- load_reddy_binding_consensus(target)
#   all_chip <- all_chip_raw[grep("^[01]\\shour", names(all_chip_raw))]
#   names(all_chip) <- gsub("0 hour", paste(target, "CTRL", sep = "_"), names(all_chip))
#   names(all_chip) <- gsub("1 hour", paste(target, "DEX", sep = "_"), names(all_chip))
#   chip_Reddy_raw <- c(chip_Reddy_raw, all_chip)
# }
# 
# chip_Reddy_stchr <- sapply(chip_Reddy_raw, keepStdChr) 
# chip_Reddy <- GRangesList(chip_Reddy_stchr)
# names(chip_Reddy)
# 
# #### Load Meyer POLR2A ChIP
# chip_pol2_raw <- load_POLR2A_peaks_Myers()
# chip_pol2_stdchr <- sapply(chip_pol2_raw, keepStdChr) 
# chip_pol2 <- GRangesList(chip_pol2_stdchr)
# sapply(chip_pol2, length)
# 
# #### Gather all peaks
# stdchr <- append(stdchr, c(chip_Reddy, chip_pol2))
# sapply(stdchr, length)

### Load GR binding sites
all_gr_regions <- load_reddy_gr_binding_consensus()
gr_regions <- GRangesList(c(all_gr_regions[grep("minutes", names(all_gr_regions))], "1 hour" = all_gr_regions[["1 hour"]]))
gr_regions <- unlist(gr_regions)

### Load reference genome
raw_promoters_A549 <- promoters(most_expressed_TxDb, columns = "gene_id", upstream = 3000, downstream = 3000)
stdchr_promoters_A549 <- keepStdChr(raw_promoters_A549)
names(stdchr_promoters_A549) <- stdchr_promoters_A549$gene_id

### Load Hi-C data 1 hour
hic_1h <- readRDS("output/analyses/annotate_peaks_with_hic/hic_1h_GIObject.rds")

##### Load induced and repressed gene categories
deg <- readRDS(file = "output/analyses/deg.rds")
upreg <- deg$gene_list$FC1$upreg
downreg <- deg$gene_list$FC1$downreg

##### Load diffbind regions
diffbind <- load_diffbind_cofactors_peaks()

##### Define what to draw on metagene, using which bam
# bam_filepath and metadata for cofactors
bam_cofactors_filepath_list <- get_bam_cofactors_filepath(c("MED1", "BRD4", "CDK9", "NIPBL", "SMC1A"))
metadata_cofactors <- make_metadata_from_bam_cofactors_filepath_list(bam_cofactors_filepath_list)
# bam_filepath and metadata for reddy
# bam_filepath_reddy_list <- get_bam_reddy_filepath(c("JUN", "BCL3", "CEBPB", "CTCF", "FOSL2", "HES2", "JUNB", "RAD21", "EP300", "SMC3", "H3K4me1", "H3K4me2", "H3K4me3", "H3K27ac"))
bam_reddy_filepath_list_rep2 <- get_bam_reddy_filepath(targets = c("GR", "JUN"), reps = "2")
bam_reddy_filepath_list_rep1 <- get_bam_reddy_filepath(targets = c("EP300", "FOSL2", "H3K27ac", "POLR2A"), reps = "1")
metadata_reddy_rep2 <- make_metadata_from_bam_reddy_filepath_list(bam_reddy_filepath_list_rep2)
metadata_reddy_rep1 <- make_metadata_from_bam_reddy_filepath_list(bam_reddy_filepath_list_rep1)
# gather cofactors and reddy
bam_filepath_list <- c(bam_cofactors_filepath_list, bam_reddy_filepath_list_rep2, bam_reddy_filepath_list_rep1)
metadata <- rbind(metadata_cofactors, metadata_reddy_rep2, metadata_reddy_rep1)

#### Define cofactors
# cofactors <- c("GR", "POLR2A", "MED1", "BRD4", "EP300", "CDK9",
#                "NIPBL", "SMC1A",
#                "JUN", "FOSL2", "H3K27ac")

cofactors <- c("MED1", "BRD4")

enhRDS <- list()
for (cofactor in cofactors) {
  message("##### ", cofactor)
  
  if (cofactor == "GR") {
    all_peaks <- gr_regions %>% keepStdChr
    message("    # Total number of non-overlapping GR : ", length(all_peaks))
  } else {
    regionSets <- stdchr[grep(cofactor, names(stdchr))]
    print(sapply(regionSets, length))
    
    #### With overlaps of GR
    ctrl_gr <- subsetByOverlaps(regionSets[[1]], gr_regions)
    message("    ", names(regionSets)[1], "\tNumber of regions with GR : ", length(ctrl_gr))
    dex_gr <- subsetByOverlaps(regionSets[[2]], gr_regions)
    message("    ", names(regionSets)[2], "\tNumber of regions with GR : ", length(dex_gr))

    all_peaks_raw <- c(ctrl_gr, dex_gr)
    message("    # Total number of regions with GR : ", length(all_peaks_raw))
    all_peaks <- GenomicRanges::reduce(all_peaks_raw)
    message("    # Total number of non-overlapping regions with GR : ", length(all_peaks))
    
    # #### Without overlaps with GR
    # cofactor_ctrl <- regionSets[[1]]
    # cofactor_dex <- regionSets[[2]]
    # all_peaks <- GenomicRanges::reduce(c(cofactor_ctrl, cofactor_dex))
    
    names(all_peaks) <- paste(cofactor, 1:length(all_peaks), sep = "_")
  }
  
  # 3D annotation: Annotate with Hi-C
  annotation.features <- list(promoter = stdchr_promoters_A549, enhancer = all_peaks)
  annotateInteractions(hic_1h, annotation.features)
  
  ### Explore interactions
  message("Number of annotations : ")
  print(table(regions(hic_1h)$node.class))
  
  # Interactions types
  plotInteractionAnnotations(hic_1h, legend = TRUE, viewpoints = "enhancer")
  nb_ep <- length(hic_1h[isInteractionType(hic_1h, "enhancer", "promoter")])
  nb_ed <- length(hic_1h[isInteractionType(hic_1h, "enhancer", "distal")])
  nb_ee <- length(hic_1h[isInteractionType(hic_1h, "enhancer", "enhancer")])
  message("  Number of enhancer-promoter interactions : ", nb_ep)
  message("  Number of enhancer-distal interactions : ", nb_ed)
  message("  Number of enhancer-enhancer interactions : ", nb_ee)
  message("")
  
  # Focus on enhancer-promoter
  ep <- hic_1h[isInteractionType(hic_1h, "enhancer", "promoter")]
  
  # Retrieve gene name from promoters
  prom1 <- anchorOne(ep)$promoter.id %>% unlist %>% na.omit %>% unique
  prom2 <- anchorTwo(ep)$promoter.id %>% unlist %>% na.omit %>% unique
  prom12 <- c(prom1, prom2) %>% unique %>% as.data.frame
  colnames(prom12) <- c("gene")
  message("Number of unique genes : ", nrow(prom12))
  
  # activated/repressed over 12h
  res <- cbind(prom12, "upreg" = prom12$gene %in% upreg, "downreg" = prom12$gene %in% downreg)
  nb_upreg <- sum(res$upreg)
  nb_downreg <- sum(res$downreg)
  message("     Associated with ",  nb_upreg, " induced genes > ", round(nb_upreg/nrow(res)*100, 2), " %")
  message("     Associated with ",  nb_downreg, " repressed genes > ", round(nb_downreg/nrow(res)*100, 2), " %")
  message(" ")
  
  # Retrieve coordinates of cofactors regions
  anchorOne <- anchorOne(ep) %>% as.data.frame
  colnames(anchorOne) <- paste(colnames(anchorOne), 1, sep = "_")
  anchorTwo <- anchorTwo(ep) %>% as.data.frame
  colnames(anchorTwo) <- paste(colnames(anchorTwo), 2, sep = "_")
  
  anchors <- cbind(anchorOne, anchorTwo) %>% dplyr::select(node.class_1, promoter.id_1, enhancer.id_1,
                                                           node.class_2, promoter.id_2, enhancer.id_2)
  
  enh_upreg <- c()
  enh_downreg <- c()
  prom_upreg <- c()
  prom_downreg <- c()
  
  for (i in 1:nrow(anchors)) {
    ep <- anchors$node.class_1[i]
    if (ep == "enhancer") {
      message("#### ", ep, i)
      enh <- anchors$enhancer.id_1[[i]]
      prom <- anchors$promoter.id_2[[i]]
      
      tf_upreg <- prom %in% upreg
      if (any(tf_upreg)) {
        enh_upreg <- c(enh_upreg, enh)
        prom_upreg <- c(prom_upreg, prom)
      }
      
      tf_downreg <- prom %in% downreg
      if (any(tf_downreg)) {
        enh_downreg <- c(enh_downreg, enh)
        prom_downreg <- c(prom_downreg, prom)
      }
    }
  }
  
  for (i in 1:nrow(anchors)) {
    ep <- anchors$node.class_2[i]
    if (ep == "enhancer") {
      message("#### ", ep, i)
      enh <- anchors$enhancer.id_2[[i]]
      prom <- anchors$promoter.id_1[[i]]
      
      tf_upreg <- prom %in% upreg
      if (any(tf_upreg)) {
        enh_upreg <- c(enh_upreg, enh)
        prom_upreg <- c(prom_upreg, prom)
      }
      
      tf_downreg <- prom %in% downreg
      if (any(tf_downreg)) {
        enh_downreg <- c(enh_downreg, enh)
        prom_downreg <- c(prom_downreg, prom)
      }
    }
  }
  
  # # Retrieve regions
  # coordinate_regions_raw <- GRangesList("Upregulated genes" = all_peaks[enh_upreg],
  #                                       "Downregulated genes" = all_peaks[enh_downreg])
  # coordinate_regions <- lapply(coordinate_regions_raw, resize, width = 3000, fix = "center")
  
  # How many overlaps with our diffbind regions
  enh_upreg_regions <- all_peaks[enh_upreg %>% unique]
  enh_downreg_regions <- all_peaks[enh_downreg %>% unique]
  
  message("#####     ", cofactor)
  message("Number of regions associated with induced genes : ", length(enh_upreg_regions))
  message("Number of upsignal regions : ", countOverlaps(enh_upreg_regions, diffbind[[paste(cofactor, "UP", sep = "_")]]) %>% sum)
  message("Number of downsignal regions : ", countOverlaps(enh_upreg_regions, diffbind[[paste(cofactor, "DOWN", sep = "_")]]) %>% sum)
  message("Number of unbiasedsignal regions : ", countOverlaps(enh_upreg_regions, diffbind[[paste(cofactor, "UNBIASED", sep = "_")]]) %>% sum)
  message("#####")
  message("Number of regions associated with repressed genes : ", length(enh_downreg_regions))
  message("Number of upsignal regions : ", countOverlaps(enh_downreg_regions, diffbind[[paste(cofactor, "UP", sep = "_")]]) %>% sum)
  message("Number of downsignal regions : ", countOverlaps(enh_downreg_regions, diffbind[[paste(cofactor, "DOWN", sep = "_")]]) %>% sum)
  message("Number of unbiasedsignal regions : ", countOverlaps(enh_downreg_regions, diffbind[[paste(cofactor, "UNBIASED", sep = "_")]]) %>% sum)
  
  # Remove unbiased signals and retrieve regions
  enh_upreg_regions_without_unbiased <- subsetByOverlaps(enh_upreg_regions, diffbind[[paste(cofactor, "UNBIASED", sep = "_")]], invert = TRUE)
  enh_downreg_regions_without_unbiased <- subsetByOverlaps(enh_downreg_regions, diffbind[[paste(cofactor, "UNBIASED", sep = "_")]], invert = TRUE)
  enh_downreg_regions_without_unbiased <- subsetByOverlaps(enh_downreg_regions_without_unbiased, diffbind[[paste(cofactor, "UP", sep = "_")]], invert = TRUE)
  message("#####")
  message("enh_upreg_regions_without_unbiased : ", length(enh_upreg_regions_without_unbiased))
  message("enh_downreg_regions_without_unbiased : ", length(enh_downreg_regions_without_unbiased))
  
  coordinate_regions_raw <- GRangesList("Upregulated genes" = enh_upreg_regions_without_unbiased,
                                        "Downregulated genes" = enh_downreg_regions_without_unbiased)
  # With GR:
  # 252
  # 92
  
  # Without Gr:
  # 311
  # 155
  
  coordinate_regions <- lapply(coordinate_regions_raw, resize, width = 3000, fix = "center")
  
  # metagene_df
  message("Metagene...")
  mg <- metagene2$new(regions = coordinate_regions,
                      bam_files = bam_filepath_list,
                      normalization = "RPM",
                      force_seqlevels = TRUE,
                      assay = 'chipseq',
                      cores = 4,
                      bin_count = 100)
  mg$add_metadata(design_metadata = metadata)
  df_cofactor <- mg$get_data_frame()

  # # metagene_plot
  # title_df <- paste0("Centered at ", cofactor, " binding sites linked via 3D to promoters\nof dex-responsive genes\n",
  #                    "(up : ", length(enh_upreg %>% unique) , " regions, ", prom_upreg %>% unique %>% length, " genes | ",
  #                    "down : ", length(enh_downreg %>% unique), " regions, ", prom_downreg %>% unique %>% length, " genes)")
  
  title_df <- paste0("Centered at ", cofactor, " binding sites linked via 3D to promoters\nof dex-responsive genes\n",
                     "(up : ", length(enh_upreg_regions_without_unbiased) , " regions, ", "...", " genes | ",
                     "down : ", length(enh_downreg_regions_without_unbiased), " regions, ", "...", " genes)")
  
  print(title_df)
  df_cofactor$target <- factor(df_cofactor$target, levels = c("GR", "POLR2A", "MED1", "BRD4", "EP300", "CDK9",
                                                              "NIPBL", "SMC1A",
                                                              "JUN", "FOSL2", "H3K27ac"))

  metagene_plot <- plot_metagene_cofactor(df_cofactor, title = title_df,
                                          customColors = c("#008000", "#8C001A"), scale_val = "fixed")
  # saveMetagene(metagene_plot,
  #              output_dir = "output/analyses/ecosystem/metagene_cofactors_3DAnnotation_without_unbiased",
  #              output_file = paste(today, "metagene", cofactor, "at_enhancers_linked_to_promoters_of_dex_responsive_genes_TEST", sep = "_"),
  #              width_val = 5, height_val = 13)
  
  enhRDS[[cofactor]]$up <- enh_upreg_regions_without_unbiased
  enhRDS[[cofactor]]$down <- enh_downreg_regions_without_unbiased
}

saveRDS(enhRDS, file = "output/analyses/ecosystem/metagene_cofactors_3DAnnotation_without_unbiased/regions_cofactors_3D_annotations.rds")
