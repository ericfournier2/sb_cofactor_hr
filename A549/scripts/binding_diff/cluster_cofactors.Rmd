


```{r load_cofactors, include=FALSE}
    # rmarkdown::render("scripts/binding_diff/cluster_cofactors.Rmd", knit_root_dir=getwd())
    library(rmarkdown)
    library(knitr)
    library(ggplot2)
    library(dplyr)
    
    source("scripts/binding_diff/load_diffbind_cofactors.R")
    CLUSTER_FLANK_SIZE=5000
    
    diff_regions = load_diffbind_cofactors_peaks()
    mbc_regions = combine_MBC_diff_peaks(diff_regions)
    diff_regions = c(diff_regions, mbc_regions)
    
    clusters_list = load_diffbind_clusters(CLUSTER_FLANK_SIZE)
```

# Summary of clusters

```{r cluster_summary, echo=FALSE}
    # Build a data-frame summarizing results.
    summary_df = data.frame(Region=names(clusters_list),
                            Cofactor=mcols(clusters_list)$Cofactor,
                            Direction=mcols(clusters_list)$Direction,
                            NRegions=unlist(lapply(diff_regions, length)),
                            stringsAsFactors=FALSE)
    nclusters = unlist(lapply(clusters_list, length))
    summary_df$NClusters = nclusters[summary_df$Region]
    
    # Calculate overlap percentages
    summary_df$UPOverlap=NA
    summary_df$DOWNOverlap=NA
    summary_df$UNBIASEDOverlap=NA
    for(i in 1:nrow(summary_df)) {
        region_name = summary_df$Region[i]
        for(direction in unique(mcols(clusters_list)$Direction)) {
            other_cluster = clusters_list[[paste0(summary_df$Cofactor[i], "_", direction)]]
            
            #small_cluster = clusters_list[[region_name]]
            #start(small_cluster) = start(small_cluster) + 4500
            #end(small_cluster) = end(small_cluster) - 4500
            #
            #n_overlaps = countOverlaps(small_cluster, other_cluster)
            
            n_overlaps = countOverlaps(clusters_list[[region_name]], other_cluster)
            n_overlapping = sum(n_overlaps > 0) / length(clusters_list[[region_name]])
            summary_df[[paste0(direction, "Overlap")]][i] = n_overlapping
        }
    }
    
    write.table(summary_df, file="output/Diff cofactor clusters summary.tsv", sep="\t", col.names=TRUE, row.names=FALSE)
    kable(summary_df)
```

# Plot NCluster size vs NPeaks
```{r cluster_vs_peaks, echo=FALSE}
    plot_df = reshape2::melt(summary_df, id.vars=c("Cofactor", "Direction"), 
                                         measure.vars=c("NRegions", "NClusters"), 
                                         variable.name="N")
                                         
    gg_obj = ggplot(plot_df, aes(x=N, y=value, fill=Direction)) + 
       geom_bar(stat="identity", color="black") + 
       facet_grid(Cofactor~Direction)
    
    print(gg_obj)
    pdf("output/Comparison of NPeaks vs NClusters.pdf")   
    print(gg_obj)
    invisible(dev.off())
```

# Plot cluster widths

```{r cluster_widths, echo=FALSE}
cluster_width_list = lapply(names(clusters_list), function(x) {
    data.frame(Size=width(clusters_list[[x]]),
               Cofactor=gsub("_.*", "", x),
               Direction=gsub(".*_", "", x))
})
cluster_width_df = do.call(rbind, cluster_width_list)

pdf("output/Density plot for the width of clusters.pdf")
gg_obj = ggplot(cluster_width_df, aes(x=log2(Size), fill=Direction)) +
    geom_density() +
    facet_grid(Cofactor~Direction) +
    xlim(c(13,14.5)) +
    labs(title="Density plot for the width of clusters")
    print(gg_obj)
invisible(dev.off())
print(gg_obj)
```    
    
# Plot the proportion of clusters with N binding events 
```{r cluster_psites, echo=FALSE}   
    cluster_psites_list = lapply(names(clusters_list), function(x) {
        clust_df = as.data.frame(clusters_list[[x]])
        res_df = clust_df %>%
            group_by(NSites) %>%
            summarize(N=n(), P=N/nrow(clust_df))
            
        res_df$Cofactor=gsub("_.*", "", x)
        res_df$Direction=gsub(".*_", "", x)
        
        res_df
    })
    
    cluster_psite_df = do.call(rbind, cluster_psites_list)
    cluster_psite_df$NSitesF = factor(cluster_psite_df$NSites)
    
    pdf("output/Proportion of clusters with N binding events.pdf")
    gg_obj = ggplot(cluster_psite_df[cluster_psite_df$NSites <= 7,], aes(x=NSitesF, y=P, fill=Direction)) +
        geom_bar(stat="identity") +
        facet_grid(Cofactor~Direction) +
        labs(title="Proportion of clusters with N binding events")
    print(gg_obj)
    invisible(dev.off())
    
    print(gg_obj)
```    
    