---
title: "HiC summary"
author: "Eric Fournier"
date: "`r Sys.Date()`"
output: html_document
---

```{r load_data, echo=FALSE}
library(RColorBrewer)
hic_timepoint="Ctrl"
source("scripts/load_hic_data.R")
t_obj = load_connection_data(hic_timepoint=hic_timepoint)
```
# Summary of parameters

| metric | value |
|---:|:-----|
| Spacing around promoters | `r 1000` |
| HiC-contacts used | `r hic_timepoint` |
| TADs used | `r hic_timepoint` |

# Summary of analyzed promoters

Number of assessed genes: `r length(query_regions(t_obj)$Promoters)`

## Differential expression across time:
Genes are considered DE if they have an FDR < 0.05 and a abs(logFC) > log(1.5)
```{r 2h_DE_summary, echo=FALSE}
time_order = c("DE-5m", "DE-10m", "DE-15m", "DE-20m", "DE-25m", "DE-0.5h", 
               "DE-1h", "DE-2h", "DE-3h", "DE-4h", "DE-5h", "DE-6h", "DE-7h",
               "DE-8h", "DE-10h", "DE-12h")
               
metrics_fun = function(time_point) {
    data.frame(Genes=c(sum(mcols(query_regions(t_obj)$Promoters)[[time_point]] == "Up"),
                       sum(mcols(query_regions(t_obj)$Promoters)[[time_point]] == "Down"),
                       sum(mcols(query_regions(t_obj)$Promoters)[[time_point]] == "Stable")),
               Direction=c("Up", "Down", "Stable"),
               Time=time_point)
}

de_df = do.call(rbind, lapply(time_order, metrics_fun))
de_df$Time = factor(de_df$Time, levels=time_order)
ggplot(de_df[de_df$Direction != "Stable",], aes(x=Time, y=Genes, fill=Direction)) +
    geom_bar(stat="identity", color="black") +
    scale_fill_manual(values=c(Up="#0ecc57", Down="red")) +
    theme(axis.text.x = element_text(angle = 90, hjust = 1))
```

## GR-binding status of DE genes

```{r GR_binding_regulation_status, echo=FALSE, results='asis'}
prom = query_regions(t_obj)$Promoters
total_DE_genes = sum(prom$RegulationStatus != "Stable", na.rm=TRUE)
gr_bound = prom$ConsensusGR > 0 | prom$ConsensusGR_Window | prom$ConsensusGR_Contact
activated = prom$RegulationStatus %in% c("ActivatedFast", "ActivatedSlow")
repressed = prom$RegulationStatus %in% c("RepressedFast", "RepressedSlow")
direct_gr_activated = sum(activated & gr_bound)
indirect_gr_activated = sum(activated & !gr_bound)
direct_gr_repressed = sum(repressed & gr_bound)
indirect_gr_repressed = sum(repressed & !gr_bound)
```

| metric | value |
|---:|:-----|
| Number of DE genes | `r total_DE_genes` |
| Number of activatated genes with promoter or HiC GR-binding | `r direct_gr_activated` |
| Number of activatated genes without promoter or HiC GR-binding | `r indirect_gr_activated` |
| Number of repressed genes with promoter or HiC GR-binding | `r direct_gr_repressed` |
| Number of repressed genes without promoter or HiC GR-binding | `r indirect_gr_repressed` |

: `r total_DE_genes`


## Detailed binding status of DE genes

```{r binding_vs_regulation_status, echo=FALSE, results='asis'}
print_factor_summary <- function(factor_column) {
    contact_column = paste0(factor_column, "_Contact")
    window_column = paste0(factor_column, "_Window")
    tad_column = paste0(factor_column, "_TAD")
    summary_df = as.data.frame(query_regions(t_obj)$Promoters) %>% 
        group_by(RegulationStatus) %>% 
        summarize(N=n(), 
                  DirectBindingN=sum(!!as.symbol(factor_column)>0), 
                  DirectBindingP=sprintf("%2.1f%%", DirectBindingN/N * 100), 
                  ContactN=sum(!!as.symbol(contact_column)), 
                  ContactP=sprintf("%2.1f%%", ContactN/N * 100),
                  DirectOrContactN=sum(!!as.symbol(factor_column)>0 | !!as.symbol(contact_column) | !!as.symbol(window_column)),
                  DirectOrContactP=sprintf("%2.1f%%", DirectOrContactN/N * 100),
                  TADN=sum(!!as.symbol(tad_column)), 
                  TADP=sprintf("%2.1f%%", TADN/N * 100),
                  AnyN=sum(!!as.symbol(factor_column)>0 | !!as.symbol(contact_column) | !!as.symbol(window_column) | !!as.symbol(tad_column)),
                  AnyP=sprintf("%2.1f%%", AnyN/N * 100))
    
    kable(summary_df, align="r")
}

up_down_names = names(annotations(t_obj))[grepl("UP|DOWN", names(annotations(t_obj)))]
for(i in c("ConsensusGR", up_down_names)) {
    cat(i, ", ", length(annotations(t_obj)[[i]]), " total regions:\n")
    print(print_factor_summary(i))
    cat("\n\n")
}
```

```{r define_venn_plot_function, echo=FALSE}
    library(VennDiagram)

    venn_plot <- function(de_value, main) {
        is_de = query_regions(t_obj)$Promoters$"DE-2h" %in% de_value
        
        direct_indices = is_de & query_regions(t_obj)$Promoters$ConsensusGR>0
        window_indices = is_de & query_regions(t_obj)$Promoters$ConsensusGR_Window
        contact_indices = is_de & query_regions(t_obj)$Promoters$ConsensusGR_Contact
        
        gene_list = list(Direct=query_regions(t_obj)$Promoters$ensembl_gene_id[direct_indices],
                         Window=query_regions(t_obj)$Promoters$ensembl_gene_id[window_indices],
                         Contact=query_regions(t_obj)$Promoters$ensembl_gene_id[contact_indices])
        
        unrepresented = sum(is_de & !direct_indices & !window_indices & !contact_indices)
        precent_unrepresented = sprintf(" (%2.0f%%) ", (unrepresented / sum(is_de)) * 100)
        main_title = paste0("Venn for genes in (", paste0(de_value, collapse=", "), ")")
        sub_title = paste0(unrepresented, precent_unrepresented, "have no binding at all.")
        
        grid.draw(venn.diagram(gene_list, filename=NULL,
                       fill=c("red", "yellow", "green"),
                       main=main_title,
                       sub=sub_title))    
    }
```

## GR-binding statuses
### GR-binding status for all genes:
```{r gr_binding_summary_all, echo=FALSE}
venn_plot(c("Stable", "Up", "Down"), "all genes")
```

### GR-binding status for DE genes:
```{r gr_binding_summary_DE, echo=FALSE}
venn_plot(c("Up", "Down"), "DE genes")
```

### GR-binding status for Up genes:
```{r gr_binding_summary_Up, echo=FALSE}
venn_plot(c("Up"), "upregulated genes")
```

### GR-binding status for Down genes:
```{r gr_binding_summary_Down, echo=FALSE}
venn_plot(c("Down"), "downregulated genes")
```

## Proportion of GR-bound DE genes across time
```{r decrease_in_gr_binding_over_time, echo=FALSE}
time_order = c("DE-5m", "DE-10m", "DE-15m", "DE-20m", "DE-25m", "DE-0.5h", 
               "DE-1h", "DE-2h", "DE-3h", "DE-4h", "DE-5h", "DE-6h", "DE-7h",
               "DE-8h", "DE-10h", "DE-12h")
direction_list = list(DE=c("Up", "Down"), Up="Up", Down="Down")

metrics_fun = function(y, time_point) {
    de_genes = mcols(query_regions(t_obj)$Promoters)[[time_point]] %in% direction_list[[y]]
    direct_binding = query_regions(t_obj)$Promoters$ConsensusGR > 0
    indirect_binding = query_regions(t_obj)$Promoters$ConsensusGR_Window |
                       query_regions(t_obj)$Promoters$ConsensusGR_Contact 
    
    data.frame(DE=rep(sum(de_genes), 3),
               Bound=c(sum(de_genes & direct_binding),
                       sum(de_genes & !direct_binding & indirect_binding),
                       sum(de_genes & (direct_binding | indirect_binding))),
               BindingType=c("Direct", "Indirect", "Any"),
               Direction=y)
}

time_metrics = lapply(time_order, function(x) {
    metrics = lapply(names(direction_list), metrics_fun, time_point=x)
               
    res = do.call(rbind, metrics)
    res$Time = x
    
    res
})

data_df = do.call(rbind, time_metrics)
data_df$Time = factor(data_df$Time, levels=time_order)

ggplot(data_df, aes(x=Time, y=Bound/DE, color=Direction, group=Direction)) +
    geom_line() +
    theme(axis.text.x = element_text(angle = 90, hjust = 1)) +
    facet_wrap(BindingType~.)
```

# Summary of HiC data

```{r TAD_intra_vs_inter, echo=FALSE}
# Assess intra-TAD vs inter-TAD contacts.
f_a = anchors(contacts(t_obj), type="first")
s_a = anchors(contacts(t_obj), type="second")

f_tad = findOverlaps(f_a, structures(t_obj)$TAD)
s_tad = findOverlaps(s_a, structures(t_obj)$TAD)

tad_df = data.frame(f=rep(NA, length(f_a)), s=rep(NA, length(s_a)))
tad_df$f[queryHits(f_tad)] = subjectHits(f_tad)
tad_df$s[queryHits(s_tad)] = subjectHits(s_tad)

n_interactions = length(contacts(t_obj))
n_regions = length(regions(contacts(t_obj)))
prop_intra_tad = sprintf("%2.1f%%", sum(tad_df$f==tad_df$s, na.rm=TRUE) / nrow(tad_df) * 100)
```

| metric | value |
|---:|:-----|
| Number of interactions | `r n_interactions` |
| Number of interacting regions | `r n_regions` |
| Intra-TAD interactions | `r prop_intra_tad` |

## TAD histogram of cofactor binding

```{r histograms_diff_cofactors_TAD}
tad_df = as.data.frame(structures(t_obj)$TAD)
for(i in up_down_names) {
    fill_color = ifelse(grepl("UP", i), "#0ecc57", "red")
    plot_title = paste0("Histogram of the number of ", i, " sites per TAD")
    #pdf(paste0(i, " per TAD.pdf"))
    print(ggplot(tad_df, aes_string(x=i)) + 
        geom_bar(fill=fill_color, color="black") + 
        labs(y="Number of TADs", x="Number of sites", title=plot_title))
    #dev.off()
}
```


## TAD heatmaps of cofactor differential binding.

```{r TAD_cofactor_summary, echo=FALSE}

# Define a function to calculate UP proportion and
# associate a status with each TAD.
tad_factor_up = function(x, factor_name) {
    new_col_p = paste0(factor_name, "_UP_Percent")
    new_col_s = paste0(factor_name, "_Status")
    up_col = paste0(factor_name, "_UP")
    down_col = paste0(factor_name, "_DOWN")
    
    up_val = mcols(x)[[up_col]]
    down_val = mcols(x)[[down_col]]
    new_val = up_val / (up_val + down_val)
    
    new_val[is.nan(new_val)] = NA
    
    status = ifelse(is.na(new_val), "Unknown",
             ifelse(new_val >= 0.8, "Up",
             ifelse(new_val <= 0.2, "Down",
                    "Ambiguous")))
    
    mcols(x)[[new_col_p]] = new_val
    mcols(x)[[new_col_s]] = status
        
    return(x)
}

# Apply the function to all cofactors.
tad_gr = structures(t_obj)$TAD
for(i in c("NIPBL", "MED1", "BRD4", "SMC1A", "CDK9")) {
    tad_gr = tad_factor_up(tad_gr, i)
}

# Determine which tADs have no binding information, and filter them out.
all_unknown = tad_gr$NIPBL_Status=="Unknown" & 
              tad_gr$CDK9_Status=="Unknown" &  
              tad_gr$BRD4_Status=="Unknown" & 
              tad_gr$MED1_Status=="Unknown" 

# gather all UP binding percent in a single matrix.
bind_mat = cbind(tad_gr$NIPBL_UP_Percent[!all_unknown], 
                 tad_gr$CDK9_UP_Percent[!all_unknown], 
                 tad_gr$BRD4_UP_Percent[!all_unknown], 
                 tad_gr$MED1_UP_Percent[!all_unknown])
colnames(bind_mat) = c("NIPBL", "CDK9", "BRD4", "MED1")

# NA can't be used in clustering, so replace NA with 0.5.
bind_mat_2 = bind_mat
bind_mat_2[is.na(bind_mat_2)] = 0.5

# Plot it.
new_pal = colorRampPalette(brewer.pal(8, "RdYlGn"))(25)
heatmap(bind_mat_2, scale="none", col = new_pal)
```

## Promoter heatmaps of cofactor differential binding.

```{r gene_cofactor_summary, echo=FALSE}
promoter_factor_up = function(x, factor_name) {
    new_col_s = paste0(factor_name, "_Status")
    up_col = paste0(factor_name, "_UP")
    up_col_w = paste0(factor_name, "_UP_Window")
    up_col_c = paste0(factor_name, "_UP_Contact")
    down_col = paste0(factor_name, "_DOWN")
    down_col_w = paste0(factor_name, "_DOWN_Window")
    down_col_c = paste0(factor_name, "_DOWN_Contact")
    
    up_val = mcols(x)[[up_col]] > 0 | mcols(x)[[up_col_w]] | mcols(x)[[up_col_c]]
    down_val = mcols(x)[[down_col]] > 0 | mcols(x)[[down_col_w]] | mcols(x)[[down_col_c]]
   
    status = ifelse(!up_val & !down_val, "Unknown",
             ifelse(up_val & down_val, "Ambiguous",
             ifelse(up_val, "Up",
                    "Down")))
    
    mcols(x)[[new_col_s]] = status
        
    return(x)
}

promoter_gr = query_regions(t_obj)$Promoters
for(i in c("NIPBL", "MED1", "BRD4", "SMC1A", "CDK9")) {
    promoter_gr = promoter_factor_up(promoter_gr, i)
}

all_unknown = promoter_gr$NIPBL_Status=="Unknown" & 
              promoter_gr$CDK9_Status=="Unknown" &  
              promoter_gr$BRD4_Status=="Unknown" & 
              promoter_gr$MED1_Status=="Unknown" 

promoter_gr = promoter_gr[!all_unknown]

promoter_mat = cbind(promoter_gr$MED1_Status, 
                     promoter_gr$BRD4_Status, 
                     promoter_gr$CDK9_Status, 
                     promoter_gr$NIPBL_Status)
                     
colnames(promoter_mat) = c("MED1", "BRD4", "CDK9", "NIPBL")                     
promoter_mat = ifelse(promoter_mat=="Up", 1,
               ifelse(promoter_mat=="Down", 0,
                      0.5))
new_pal = colorRampPalette(brewer.pal(8, "RdYlGn"))(25)

for(gene_status in unique(promoter_gr$RegulationStatus)) {
    partial_mat = promoter_mat[promoter_gr$RegulationStatus==gene_status,]
    #pdf(paste0(gene_status, " cofactor binding.pdf"))
    heatmap(partial_mat, scale="none", col = new_pal, main=paste(gene_status, "promoters"))
    #dev.off()
}
```

## TADs by regulation status

```{r gene_tad_status, echo=FALSE}
proms = query_regions(t_obj)$Promoters
tads = structures(t_obj)$TAD
for(regulation_status in unique(proms$RegulationStatus)) {
    proms_subset = proms[proms$RegulationStatus == regulation_status]
    mcols(tads)[[regulation_status]] = countOverlaps(tads, proms_subset)
}
status_mat = as.data.frame(tads)[,unique(proms$RegulationStatus)]
with_genes = apply(status_mat > 0, 1, any)

status_de_mat = as.data.frame(tads)[,setdiff(unique(proms$RegulationStatus), "Stable")]
with_de_genes = apply(status_de_mat > 0, 1, any)

with_multiple_de_genes = apply(status_de_mat, 1, sum) > 1
proportion_de_mat = apply(status_de_mat, 2, "/", apply(status_de_mat, 1, sum))

#pdf("TADS by regulation status of genes.pdf", height=14, width=7)
heatmap(proportion_de_mat[with_multiple_de_genes,], scale="none", Colv=NA, labRow=NULL)
#dev.off()

```

## GR motif enrichments

### GR motif in GR-bound sites of DE genes

```{r gr_motif_gr_sites_de_genes, echo=FALSE}
motif_by_de = function(promoter_regions, gr_by_gene, de_time, de_class) {
    de_genes = names(promoter_regions)[mcols(promoter_regions)[[de_time]] %in% de_class]
    de_gr = gr_by_gene[names(gr_by_gene) %in% de_genes]
    de_gr_gr = unlist(GRangesList(unlist(de_gr)))

    res = motif_enrichment(de_gr_gr, select_annotations("hg38"))

    return(res)
}

promoter_regions = query_regions(t_obj)$Promoters
gr_by_gene = get_all_sites(t_obj, "Promoters", "ConsensusGR")

promoter_up_motif = motif_by_de(promoter_regions, gr_by_gene, "DE-2h", "Up")
promoter_down_motif = motif_by_de(promoter_regions, gr_by_gene, "DE-2h", "Down")

print_enrichment <- function(enrichment_results) {
    as.data.frame(enrichment_results$Report) %>% 
        mutate(adjusted.p.value=p.adjust(p.value)) %>% 
        filter(adjusted.p.value <= 0.05) %>% 
        arrange(desc(top.motif.prop)) %>%
        head(n=50) %>%
        kable
}
```

#### Top 50 motifs for upregulated genes
```{r gr_motif_gr_sites_de_genes_up, echo=FALSE}
    print_enrichment(promoter_up_motif)
```

#### Top 50 motifs for downregulated genes
```{r gr_motif_gr_sites_de_genes_down, echo=FALSE}
    print_enrichment(promoter_down_motif)
```

### GR motif in cofactor binding-sites

```{r gr_motif_cofactor_diff, echo=FALSE}
cofactor_up_motif = motif_enrichment(annotations(t_obj)$CofactorsUp, select_annotations("hg38"))
cofactor_down_motif = motif_enrichment(annotations(t_obj)$CofactorsDown, select_annotations("hg38"))
```

#### Top 50 motifs for Cofactor UP
```{r gr_motif_cofactor_diff_up, echo=FALSE}
print_enrichment(cofactor_up_motif)
```

#### Top 50 motifs for Cofactor DOWN
```{r gr_motif_cofactor_diff_down, echo=FALSE}
print_enrichment(cofactor_down_motif)
```